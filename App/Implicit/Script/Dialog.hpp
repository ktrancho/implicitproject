// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <unordered_map>
#include <string>
#include <istream>
#include <ostream>
#include <algorithm>

#include <Implicit/Script/DialogData.hpp>

namespace Implicit {
namespace Script {

class Dialog {
public:
    Dialog(std::istream& a_in, std::ostream& a_out) :
        parent(nullptr),
        root(nullptr),
        in(a_in),
        out(a_out),
        actions() {
        initCommands();
    }

    Dialog(Dialog* a_parent) :
        parent(a_parent),
        root(parent->root),
        in(parent->in),
        out(parent->out),
        actions() {
        initCommands();
    }

    bool use(Dialog*& nextDialog) {
        greeting();
        std::string command;
        in >> command;
        return exec(command, nextDialog);
    }

    virtual void greeting() {
        out << ">>> ";
    }

    virtual void help() {
        out << "Commands : " << std::endl;
        std::for_each(actions.cbegin(), actions.cend(), [&](const std::pair<std::string, DialogData>& command) {
            out << " - " << command.second.lineCommand << std::endl;
        });
    }

    virtual void helpCommand(const std::string& command) {

        std::unordered_map<std::string, DialogData>::iterator it;
        if((it = actions.find(command)) == actions.cend()) {
            out << "No command named \"" << command << "\" at this stage" << std::endl;
            return;
        }
        out << "For given command : " << (it->second.lineCommand) << std::endl;
        it->second.help(out);
    }

    void registerCommand(const std::string& command, const DialogData& action) {
        actions[command] = action;
    }

    bool exec(const std::string& command, Dialog*& nextDialog) {
        std::unordered_map<std::string, DialogData>::iterator it;
        if((it = actions.find(command)) == actions.cend()) {
            out << "No command found for given command : \"" << command << "\"" << std::endl;
            return true;
        }
        Dialog* nextTarget = nullptr;
        bool check = it->second.action(out, nextTarget);
        if(! nextTarget) {
            nextDialog = this;
        } else {
            nextDialog = nextTarget;
        }
        return check;
    }

private:
    void initCommands() {
        registerCommand("exit", DialogData("exit : exit the console.", [&](std::ostream& outStream) {
            outStream << "Stop talking with the console." << std::endl;
            outStream << "Implicits are too sad about this." << std::endl;
        }, [&](std::ostream& outStream, Dialog*& nextDialog) -> bool {

            nextDialog = nullptr;
            return false;
        }));

        registerCommand("help", DialogData("help : ask for this page.", [&](std::ostream& outStream) {
            outStream << "Display list of proposed commands at the current stage." << std::endl;
        }, [&](std::ostream& outStream, Dialog*& nextDialog) -> bool {

            help();
            nextDialog = nullptr;
            return true;
        }));

        registerCommand("back", DialogData("back : go to previous stage.", [&](std::ostream& outStream) {
            outStream << "Quit current stage to go to previous stage." << std::endl;
        }, [&](std::ostream& outStream, Dialog*& nextDialog) -> bool {

            nextDialog = parent;
            return true;
        }));

        registerCommand("?", DialogData("? [COMMAND] : ask for the help of a given command.",
                [&](std::ostream& outStream) {
            outStream << "Give more details about current command, parameters and actions." << std::endl;
        }, [&](std::ostream& outStream, Dialog*& nextDialog) -> bool {

            std::string command;
            in >> command;
            helpCommand(command);
            nextDialog = nullptr;
            return true;
        }));
    }

protected:
    Dialog* parent;
    Dialog* root;
    std::istream& in;
    std::ostream& out;
private:
    std::unordered_map<std::string, DialogData> actions;
};

}
}
