// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <Implicit/Script/Dialog.hpp>
#include <Implicit/Engine/Tree/Scene2D/Scene.hpp>
#include <Implicit/Engine/Tree/Utils.hpp>

namespace Implicit {
namespace Script {
namespace Dialogs {

class View : public Implicit::Script::Dialog {
public:
    View(const std::shared_ptr<Implicit::Engine::Tree::Vector2f::Scene>& a_scene, Dialog* a_parent) :
            Implicit::Script::Dialog(a_parent),
            scene(a_scene) {
        initCommands();
    }

    virtual void greeting() override {
        out << "view >>> ";
    }

private:
    void initCommands() {
        registerCommand("state", DialogData("state : give the current state of visualization settings.", [&](std::ostream& outStream) {
            outStream << "Print field visualization related informations." << std::endl;
        }, [&](std::ostream& outStream, Dialog*& nextDialog) -> bool {

            out << "numberIso : " << scene->window->numberIso << std::endl;
            out << "drawField : " << ((scene->window->drawField) ? "true" : "false") << std::endl;
            out << "drawTarget : " << ((scene->window->drawTarget) ? "true" : "false") << std::endl;
            out << "drawIso : " << ((scene->window->drawIso) ? "true" : "false") << std::endl;
            out << "drawExternal : " << ((scene->window->drawExternal) ? "true" : "false") << std::endl;
            out << "drawFarField : " << ((scene->window->drawFarField) ? "true" : "false") << std::endl;
            nextDialog = nullptr;
            return true;
        }));

        registerCommand("numberIso", DialogData("numberIso : number of isosurfaces lines to display.", [&](std::ostream& outStream) {
            outStream << "Draw the lines corresponding to given fixed values." << std::endl;
            outStream << "numberIso control the number of isosurfaces to display outside and inside the surface." << std::endl;
        }, [&](std::ostream& outStream, Dialog*& nextDialog) -> bool {

            setNumberIso();
            nextDialog = nullptr;
            return true;
        }));

        registerCommand("drawField", DialogData("drawField : display the colors corresponding to field value.", [&](std::ostream& outStream) {
            outStream << "Display field in the background of the isosurfaces." << std::endl;
            outStream << "Gives an idea about how the field acts everywhere on the space." << std::endl;
        }, [&](std::ostream& outStream, Dialog*& nextDialog) -> bool {

            setDrawField();
            nextDialog = nullptr;
            return true;
        }));

        registerCommand("drawTarget", DialogData("drawTarget : display the target implicit surface.", [&](std::ostream& outStream) {
            outStream << "Display the isosurface corresponding to the target implicit surface." << std::endl;
            outStream << "This show the shape we get and we are interested about after operations on field." << std::endl;
        }, [&](std::ostream& outStream, Dialog*& nextDialog) -> bool {

            setDrawTarget();
            nextDialog = nullptr;
            return true;
        }));

        registerCommand("drawIso", DialogData("drawIso : display isosurfaces outside and inside the surface.", [&](std::ostream& outStream) {
            outStream << "Draw the lines corresponding to given fixed values." << std::endl;
        }, [&](std::ostream& outStream, Dialog*& nextDialog) -> bool {

            setDrawIso();
            nextDialog = nullptr;
            return true;
        }));

        registerCommand("drawExternal", DialogData("drawExternal : display isosurfaces further than standard definition region", [&](std::ostream& outStream) {
            outStream << "Draw the lines corresponding to given fixed values." << std::endl;
            outStream << "Often implicit fields can be bounded, this option allow to display unbounded isosurfaces." << std::endl;
        }, [&](std::ostream& outStream, Dialog*& nextDialog) -> bool {

            setDrawExternal();
            nextDialog = nullptr;
            return true;
        }));

        registerCommand("drawFarField", DialogData("drawFarField : display the colors corresponding to field value one range further.", [&](std::ostream& outStream) {
            outStream << "Display field in the background of the isosurfaces." << std::endl;
            outStream << "Gives an idea about how the field acts everywhere on the space." << std::endl;
            outStream << "Allow visualization on one more range than standard bounded definition." << std::endl;
        }, [&](std::ostream& outStream, Dialog*& nextDialog) -> bool {

            setDrawFarField();
            nextDialog = nullptr;
            return true;
        }));


    }

    void setNumberIso() {
        int val = 0;
        if(!(in >> val)){
            out << "Please, give a integer value !" << std::endl;
            in.clear();
            in.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            return;
        }
        if(val <= 0) {
            out << "numberIso has to be positive, can't be " << val << std::endl;
        }
        scene->window->numberIso = val;
        out << "numberIso = " << scene->window->numberIso << std::endl;
    }

    bool getUserInputBool(bool& val) {
        std::string userInput;
        in >> userInput;
        if(userInput == "true") {
            val = true;
            return true;
        } else if(userInput == "false") {
            val = false;
            return true;
        }
        out << "Please, you value has to be \"true\" or \"false\", \"" << userInput << "\" will be ignored" << std::endl;
        return false;
    }

    void setDrawField() {
        if(! getUserInputBool(scene->window->drawField)) {
            return;
        }
        out << "drawField = " << ((scene->window->drawField) ? "true" : "false") << std::endl;
    }

    void setDrawTarget() {
        if(! getUserInputBool(scene->window->drawTarget)) {
            return;
        }
        out << "drawTarget = " << ((scene->window->drawTarget) ? "true" : "false") << std::endl;
    }

    void setDrawIso() {
        if(! getUserInputBool(scene->window->drawIso)) {
            return;
        }
        out << "drawIso = " << ((scene->window->drawIso) ? "true" : "false") << std::endl;
    }

    void setDrawExternal() {
        if(! getUserInputBool(scene->window->drawExternal)) {
            return;
        }
        out << "drawExternal = " << ((scene->window->drawExternal) ? "true" : "false") << std::endl;
    }

    void setDrawFarField() {
        if(! getUserInputBool(scene->window->drawFarField)) {
            return;
        }
        out << "drawFarField = " << ((scene->window->drawFarField) ? "true" : "false") << std::endl;
    }

private:
    std::shared_ptr<Implicit::Engine::Tree::Vector2f::Scene> scene;
};

}
}
}
