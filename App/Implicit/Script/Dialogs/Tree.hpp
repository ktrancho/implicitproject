// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <Implicit/Script/Dialog.hpp>
#include <Implicit/Engine/Tree/Scene2D/Scene.hpp>
#include <Implicit/Engine/Tree/Utils.hpp>
#include <Implicit/Script/Dialogs/Components/Selector.hpp>
#include <Implicit/Script/Dialogs/Components/Factory.hpp>

namespace Implicit {
namespace Script {
namespace Dialogs {

class Tree : public Implicit::Script::Dialog {
public:
    Tree(const std::shared_ptr<Implicit::Engine::Tree::Vector2f::Scene>& a_scene, Dialog* a_parent) :
        Implicit::Script::Dialog(a_parent),
        scene(a_scene),
        componentSelector(nullptr) {
        initCommands();
    }

    virtual void greeting() override {
        out << "tree >>> ";
    }

private:
    void initCommands() {
        registerCommand("display", DialogData("display : display current implicit tree.", [&](std::ostream& outStream) {
            outStream << "Show the implicit tree in the console, show nodes, #id, type and name." << std::endl;
        }, [&](std::ostream& outStream, Dialog*& nextDialog) -> bool {

            for(auto implicit : scene->implicits) {
                Implicit::Engine::Tree::Utils::treeDisplay(implicit, out);
            }

            nextDialog = nullptr;
            return true;
        }));

        registerCommand("view", DialogData("view : display first implicit found and operator.", [&](std::ostream& outStream) {
            outStream << "Default view from the top of the tree." << std::endl;
        }, [&](std::ostream& outStream, Dialog*& nextDialog) -> bool {

            if(! scene->implicits.empty()) {
                scene->currentImplicit = scene->implicits[0];
                scene->firstOperatorFoundLookAt();
            }

            nextDialog = nullptr;
            return true;
        }));

        registerCommand("select", DialogData("select [COMPONENT ID] : select a component.", [&](std::ostream& outStream) {
            outStream << "Select a component to handle it." << std::endl;
        }, [&](std::ostream& outStream, Dialog*& nextDialog) -> bool {

            selectComponent();
            nextDialog = componentSelector.get();
            return true;
        }));
    }

    void selectComponent() {
        int id = -1;
        if(!(in >> id)){
            componentSelector = nullptr;
            out << "Please, give a integer value !" << std::endl;
            in.clear();
            in.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            return;
        }
        if(id < 0) {
            componentSelector = nullptr;
            out << "Id has to be a positive value can't be " << id << std::endl;
            return;
        }

        std::shared_ptr<Implicit::Engine::Tree::Component> res;
        bool found = false;
        for(auto implicit : scene->implicits) {
            if(Implicit::Engine::Tree::Utils::findComponentById(implicit, id, res)) {
                found = true;
                break;
            }
        }
        if(! found) {
            componentSelector = nullptr;
            out << "Component #" << id << " can't be found in current configuration." << std::endl;
            return;
        }

        componentSelector = Implicit::Script::Dialogs::Components::Factory::createByType(scene, res, this);
        return;
    }

private:
    std::shared_ptr<Implicit::Engine::Tree::Vector2f::Scene> scene;
    std::shared_ptr<Implicit::Script::Dialogs::Components::Selector> componentSelector;
};

}
}
}
