// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <memory>
#include <iostream>
#include <istream>
#include <ostream>

#include <Implicit/Script/Dialog.hpp>
#include <Implicit/Engine/Tree/Scene2D/Scene.hpp>

namespace Implicit {
namespace Script {
namespace Dialogs {

class Factory {
public:
    static std::shared_ptr<Implicit::Script::Dialog> createDefault(std::istream& a_in = std::cin,
            std::ostream& a_out = std::cout);

    static std::shared_ptr<Implicit::Script::Dialog> createScene(
            const std::shared_ptr<Implicit::Engine::Tree::Vector2f::Scene>& a_scene, std::istream& a_in = std::cin,
            std::ostream& a_out = std::cout);
};

}
}
}
