// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <memory>

#include <Implicit/Script/Dialog.hpp>
#include <Implicit/Engine/Tree/Scene2D/Scene.hpp>
#include <Implicit/Script/Dialogs/Tree.hpp>
#include <Implicit/Script/Dialogs/View.hpp>

namespace Implicit {
namespace Script {
namespace Dialogs {

class SceneDialog : public Implicit::Script::Dialog {
public:
    SceneDialog(const std::shared_ptr<Implicit::Engine::Tree::Vector2f::Scene>& a_scene,
            std::istream& a_in, std::ostream& a_out) :
        Implicit::Script::Dialog(a_in, a_out),
        scene(a_scene),
        treeDialog(new Implicit::Script::Dialogs::Tree(scene, this)),
        viewDialog(new Implicit::Script::Dialogs::View(scene, this)){
        initCommands();
    }

private:
    void initCommands() {
        registerCommand("tree", DialogData("tree : manage implicit tree.", [&](std::ostream& outStream) {
            outStream << "Allow operations on the current implicit tree." << std::endl;
        }, [&](std::ostream& outStream, Dialog*& nextDialog) -> bool {

            nextDialog = treeDialog.get();
            return true;
        }));

        registerCommand("view", DialogData("view : manage implicit display on screen.", [&](std::ostream& outStream) {
            outStream << "Change display settings of the field and the implicits." << std::endl;
        }, [&](std::ostream& outStream, Dialog*& nextDialog) -> bool {

            nextDialog = viewDialog.get();
            return true;
        }));
    }

private:
    std::shared_ptr<Implicit::Engine::Tree::Vector2f::Scene> scene;

    std::unique_ptr<Implicit::Script::Dialogs::Tree> treeDialog;
    std::unique_ptr<Implicit::Script::Dialogs::View> viewDialog;
};

}
}
}
