// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <Implicit/Script/Dialogs/Components/Selector.hpp>
#include <Implicit/Engine/Transforms2D/Translation.hpp>

namespace Implicit {
namespace Script {
namespace Dialogs {
namespace Components {

class Translation : public Implicit::Script::Dialogs::Components::Selector {
public:
    Translation(const std::shared_ptr<Implicit::Engine::Tree::Vector2f::Scene>& a_scene,
            const std::shared_ptr<Implicit::Engine::Tree::Component>& a_component, Dialog* a_parent) :
        Implicit::Script::Dialogs::Components::Selector(a_scene, a_component, a_parent),
        translation((const std::shared_ptr<Implicit::Engine::Vector2f::Transforms::Translation>&)a_component) {
        initCommands();
    }

protected:
    virtual void viewInScene() override {
        scene->currentImplicit = translation;
    }

private:
    void initCommands() {
        registerCommand("state", DialogData("state : give current state of the translation node.", [&](std::ostream& outStream) {
            outStream << "Print current translation vector." << std::endl;
        }, [&](std::ostream& outStream, Dialog*& nextDialog) -> bool {

            out << "Translation : (" << translation->translation.transpose() << ")" << std::endl;
            nextDialog = nullptr;
            return true;
        }));

        registerCommand("mouse", DialogData("mouse : edit translation using mouse grab.", [&](std::ostream& outStream) {
            outStream << "Edit the translation vector using mouse grab on screen." << std::endl;
        }, [&](std::ostream& outStream, Dialog*& nextDialog) -> bool {

            mouse();
            nextDialog = nullptr;
            return true;
        }));

        registerCommand("move", DialogData("move [X] [Y] : add a translation to current node.", [&](std::ostream& outStream) {
            outStream << "Relative translation vector modification." << std::endl;
        }, [&](std::ostream& outStream, Dialog*& nextDialog) -> bool {

            move();
            nextDialog = nullptr;
            return true;
        }));
    }

    bool getUserXY(float& x, float& y) {
        if(!(in >> x)){
            out << "Please, give a float value !" << std::endl;
            in.clear();
            in.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            return false;
        }
        if(!(in >> y)){
            out << "Please, give a float value !" << std::endl;
            in.clear();
            in.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            return false;
        }
        return true;
    }

    void move() {
        float x, y;
        if(! getUserXY(x, y)) {
            return;
        }
        out << "translation (" << translation->translation.transpose() << ") -> (";
        translation->translation += Eigen::Vector2f(x, y);
        out << translation->translation.transpose() << ")" << std::endl;
    }

    void mouse() {
        out << " - Edit translation with mouse grab on screen." << std::endl;
        out << " - Type key Esc. when finished." << std::endl;
        out << " ...(window manipulation) " << std::endl;
        scene->window->mouseGrab([&](const Eigen::Vector2f& offset) {
            Eigen::Vector2f sceneOffset;
            scene->vectorFromScreenToScene(offset, sceneOffset);
            translation->translation += sceneOffset;
        }, [&]() {
            scene->render(1);
        });
    }

private:
    std::shared_ptr<Implicit::Engine::Vector2f::Transforms::Translation> translation;
};

}
}
}
}
