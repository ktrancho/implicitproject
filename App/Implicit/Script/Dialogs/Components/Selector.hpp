// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <memory>

#include <Implicit/Script/Dialog.hpp>
#include <Implicit/Engine/Tree/Component.hpp>
#include <Implicit/Engine/Tree/Utils.hpp>
#include <Implicit/Engine/Tree/Scene2D/Scene.hpp>

namespace Implicit {
namespace Script {
namespace Dialogs {
namespace Components {

class Selector : public Implicit::Script::Dialog {
public:
    Selector(const std::shared_ptr<Implicit::Engine::Tree::Vector2f::Scene>& a_scene,
            const std::shared_ptr<Implicit::Engine::Tree::Component>& a_component, Dialog* a_parent) :
            Implicit::Script::Dialog(a_parent),
            scene(a_scene),
            component(a_component) {
        initCommands();
    }

    virtual void greeting() override {
        out << "#" << component->getId() << " | " << component->getType() << " >>> ";
    }

protected:
    /// Redefine this to allow view of component in scene.
    virtual void viewInScene() {
        // do nothing.
    }

private:
    void initCommands() {
        registerCommand("display", DialogData("display : display current implicit tree.", [&](std::ostream& outStream) {
            outStream << "Show the implicit tree in the console, show nodes, #id, type and name." << std::endl;
        }, [&](std::ostream& outStream, Dialog*& nextDialog) -> bool {

            Implicit::Engine::Tree::Utils::treeDisplay(component, out);
            nextDialog = nullptr;
            return true;
        }));

        registerCommand("view", DialogData("view : display current component on the scene.", [&](std::ostream& outStream) {
            outStream << "Show the implicit field, operator or other in the scene." << std::endl;
        }, [&](std::ostream& outStream, Dialog*& nextDialog) -> bool {

            viewInScene();
            nextDialog = nullptr;
            return true;
        }));

        registerCommand("select", DialogData("select [COMPONENT ID] : select a component.", [&](std::ostream& outStream) {
            outStream << "Select a component to handle it." << std::endl;
        }, [&](std::ostream& outStream, Dialog*& nextDialog) -> bool {

            return parent->exec("select", nextDialog);
        }));
    }

protected:
    std::shared_ptr<Implicit::Engine::Tree::Vector2f::Scene> scene;
private:
    std::shared_ptr<Implicit::Engine::Tree::Component> component;
};

}
}
}
}
