// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <memory>

#include <Implicit/Script/Dialogs/Components/Selector.hpp>

namespace Implicit {
namespace Script {
namespace Dialogs {
namespace Components {

class Factory {
public:
    static std::shared_ptr<Implicit::Script::Dialogs::Components::Selector> createDefault(
            const std::shared_ptr<Implicit::Engine::Tree::Vector2f::Scene>& a_scene,
            const std::shared_ptr<Implicit::Engine::Tree::Component>& a_component, Dialog* a_parent);

    static std::shared_ptr<Implicit::Script::Dialogs::Components::Selector> createByType(
            const std::shared_ptr<Implicit::Engine::Tree::Vector2f::Scene>& a_scene,
            const std::shared_ptr<Implicit::Engine::Tree::Component>& a_component, Dialog* a_parent);
};

}
}
}
}
