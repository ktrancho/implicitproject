// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <string>

#include <Implicit/Script/Dialogs/Components/Selector.hpp>
#include <Implicit/Engine/Composite2D/BinaryComposite.hpp>
#include <Implicit/Composition/Operators/Binary/Factory.hpp>

namespace Implicit {
namespace Script {
namespace Dialogs {
namespace Components {

class BinaryComposite : public Implicit::Script::Dialogs::Components::Selector {
public:
    BinaryComposite(const std::shared_ptr<Implicit::Engine::Tree::Vector2f::Scene>& a_scene,
                  const std::shared_ptr<Implicit::Engine::Tree::Component>& a_component, Dialog* a_parent) :
            Implicit::Script::Dialogs::Components::Selector(a_scene, a_component, a_parent),
            composite((const std::shared_ptr<Implicit::Engine::Vector2f::Composition::BinaryComposite>&)a_component) {
        initCommands();
    }

protected:
    virtual void viewInScene() override {
        scene->currentImplicit = composite;
        scene->lookAtOperator(composite->op);
    }

private:
    void initCommands() {
        registerCommand("operator-change", DialogData("operator-change [OP] : change the operator of the current composite.", [&](std::ostream& outStream) {
            outStream << "Available operators are the following :" << std::endl;
            for(std::string name : Implicit::Operator::Binary::Factory::getNames()) {
                outStream << "- " << name << std::endl;
            }
        }, [&](std::ostream& outStream, Dialog*& nextDialog) -> bool {

            changeOperator();
            nextDialog = nullptr;
            return true;
        }));
    }

    void changeOperator() {
        std::string name;
        in >> name;

        std::shared_ptr<Implicit::Operator::BinaryFloat> op;
        if(! Implicit::Operator::Binary::Factory::byName(name, op)) {
            out << "No binary operator found with the name \"" << name << "\"" << std::endl;
            return;
        }
        composite->changeOperator(op);

        viewInScene();
    }

private:
    std::shared_ptr<Implicit::Engine::Vector2f::Composition::BinaryComposite> composite;
};

}
}
}
}
