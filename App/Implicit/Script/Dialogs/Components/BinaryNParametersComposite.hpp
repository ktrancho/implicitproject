// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <string>

#include <Implicit/Script/Dialogs/Components/Selector.hpp>
#include <Implicit/Engine/Composite2D/BinaryNParametersComposite.hpp>
#include <Implicit/Composition/Operators/BinaryNParameters/Factory.hpp>
#include <Implicit/Engine/Display/BinaryNParameters.hpp>

namespace Implicit {
namespace Script {
namespace Dialogs {
namespace Components {

class BinaryNParametersComposite : public Implicit::Script::Dialogs::Components::Selector {
public:
    BinaryNParametersComposite(const std::shared_ptr<Implicit::Engine::Tree::Vector2f::Scene>& a_scene,
                    const std::shared_ptr<Implicit::Engine::Tree::Component>& a_component, Dialog* a_parent) :
            Implicit::Script::Dialogs::Components::Selector(a_scene, a_component, a_parent),
            composite((const std::shared_ptr<Implicit::Engine::Vector2f::Composition::NParameters::BinaryComposite>&)a_component),
            displayData(new Implicit::Engine::Display::BinaryNParameters(composite->op, composite->size())){
        initCommands();
    }

protected:
    virtual void viewInScene() override {
        scene->currentImplicit = composite;
        scene->visualizeOperator(displayData);
    }

private:
    void initCommands() {
        registerCommand("operator-change", DialogData("operator-change [OP] : change the operator of the current composite.", [&](std::ostream& outStream) {
            outStream << "Available operators are the following :" << std::endl;
            for(std::string name : Implicit::Operator::Binary::NParameters::Factory::getNames()) {
                outStream << "- " << name << std::endl;
            }
        }, [&](std::ostream& outStream, Dialog*& nextDialog) -> bool {

            changeOperator();
            nextDialog = nullptr;
            return true;
        }));

        registerCommand("set-value", DialogData("set-value [ID] [VALUE] : change a parameter in the binary operator.", [&](std::ostream& outStream) {
            outStream << "parameters is of size " << displayData->parameters.size() << std::endl;
        }, [&](std::ostream& outStream, Dialog*& nextDialog) -> bool {

            int id;
            float value;

            in >> id;
            in >> value;

            if(id < 0 || id >= displayData->parameters.size()) {
                outStream << "Please give a correct value " << id << " is not between 0 and " << displayData->parameters.size() << std::endl;
                nextDialog = nullptr;
                return true;
            }

            displayData->parameters[id] = value;
            viewInScene();
            nextDialog = nullptr;
            return true;
        }));

        registerCommand("display-info", DialogData("display-info : list parameters for the binary operator's display.", [&](std::ostream& outStream) {
        }, [&](std::ostream& outStream, Dialog*& nextDialog) -> bool {

            outStream << "Display of size " << displayData->parameters.size() << std::endl;

            outStream << "Values are : [";
            for(int i = 0; i < displayData->parameters.size(); ++i) {
                if(i) {
                    outStream << ", ";
                }
                outStream << displayData->parameters[i];
            }
            outStream << "]" << std::endl;

            nextDialog = nullptr;
            return true;
        }));
    }

    void changeOperator() {
        std::string name;
        in >> name;

        std::shared_ptr<Implicit::Operator::NParameters::BinaryFloat> op;
        if(! Implicit::Operator::Binary::NParameters::Factory::byName(name, op)) {
            out << "No binary operator found with the name \"" << name << "\"" << std::endl;
            return;
        }
        composite->changeOperator(op);

        viewInScene();
    }

private:
    std::shared_ptr<Implicit::Engine::Vector2f::Composition::NParameters::BinaryComposite> composite;
    std::shared_ptr<Implicit::Engine::Display::BinaryNParameters> displayData;
};

}
}
}
}
