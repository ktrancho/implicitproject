// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <string>

#include <Implicit/Script/Dialogs/Components/Selector.hpp>
#include <Implicit/Engine/Composite2D/NaryOneParameterComposite.hpp>
#include <Implicit/Composition/Operators/NaryOneParameter/Factory.hpp>
#include <Implicit/Engine/Display/NaryFloatOneParameter.hpp>

namespace Implicit {
namespace Script {
namespace Dialogs {
namespace Components {

class NaryOneParameterComposite : public Implicit::Script::Dialogs::Components::Selector {
public:
    NaryOneParameterComposite(const std::shared_ptr<Implicit::Engine::Tree::Vector2f::Scene>& a_scene,
                    const std::shared_ptr<Implicit::Engine::Tree::Component>& a_component, Dialog* a_parent) :
            Implicit::Script::Dialogs::Components::Selector(a_scene, a_component, a_parent),
            composite((const std::shared_ptr<Implicit::Engine::Vector2f::Composition::OneParameter::NaryComposite>&)a_component),
            displayData(new Implicit::Engine::Display::NaryFloatOneParameter(composite->op, composite->size(), 0.f)){
        initCommands();
    }

protected:
    virtual void viewInScene() override {
        scene->currentImplicit = composite;
        scene->visualizeOperator(displayData);
    }

private:

    virtual void viewInScene3D() {
        scene->currentImplicit = composite;
        scene->visualizeOperator3D(displayData);
    }

    void initCommands() {

        registerCommand("view-3D", DialogData("view-3D : display current component in 3D on the scene.", [&](std::ostream& outStream) {
            outStream << "Show the implicit field, operator or other as 3D in the scene." << std::endl;
        }, [&](std::ostream& outStream, Dialog*& nextDialog) -> bool {

            viewInScene3D();
            nextDialog = nullptr;
            return true;
        }));

        registerCommand("operator-change", DialogData("operator-change [OP] : change the operator of the current composite.", [&](std::ostream& outStream) {
            outStream << "Available operators are the following :" << std::endl;
            for(std::string name : Implicit::Operator::Nary::OneParameter::Factory::getNames()) {
                outStream << "- " << name << std::endl;
            }
        }, [&](std::ostream& outStream, Dialog*& nextDialog) -> bool {

            changeOperator();
            nextDialog = nullptr;
            return true;
        }));

        registerCommand("set-value", DialogData("set-value [ID] [VALUE] : change a value in the N-ary operator.", [&](std::ostream& outStream) {
            outStream << "operator is of size " << displayData->values.size() << std::endl;
        }, [&](std::ostream& outStream, Dialog*& nextDialog) -> bool {

            int id;
            float value;

            in >> id;
            in >> value;

            if(id < 0 || id >= displayData->values.size()) {
                outStream << "Please give a correct value " << id << " is not between 0 and " << displayData->values.size() << std::endl;
                nextDialog = nullptr;
                return true;
            }

            displayData->values[id] = value;
            viewInScene();
            nextDialog = nullptr;
            return true;
        }));

        registerCommand("set-first-id", DialogData("set-first-id [ID] : change the first id channel in the N-ary operator.", [&](std::ostream& outStream) {
            outStream << "operator is of size " << displayData->values.size() << std::endl;
        }, [&](std::ostream& outStream, Dialog*& nextDialog) -> bool {

            int id;

            in >> id;

            if(id < 0 || id >= displayData->values.size()) {
                outStream << "Please give a correct value " << id << " is not between 0 and " << displayData->values.size() << std::endl;
                nextDialog = nullptr;
                return true;
            }

            displayData->firstId = id;
            viewInScene();
            nextDialog = nullptr;
            return true;
        }));

        registerCommand("set-second-id", DialogData("set-second-id [ID] : change the second id channel in the N-ary operator.", [&](std::ostream& outStream) {
            outStream << "operator is of size " << displayData->values.size() << std::endl;
        }, [&](std::ostream& outStream, Dialog*& nextDialog) -> bool {

            int id;

            in >> id;

            if(id < 0 || id >= displayData->values.size()) {
                outStream << "Please give a correct value " << id << " is not between 0 and " << displayData->values.size() << std::endl;
                nextDialog = nullptr;
                return true;
            }

            displayData->secondId = id;
            viewInScene();
            nextDialog = nullptr;
            return true;
        }));

        registerCommand("set-param", DialogData("set-param [VALUE] : change the parameter's value in the N-ary operator.", [&](std::ostream& outStream) {
        }, [&](std::ostream& outStream, Dialog*& nextDialog) -> bool {

            float value;

            in >> value;

            displayData->parameter = value;
            viewInScene();
            nextDialog = nullptr;
            return true;
        }));

        registerCommand("display-info", DialogData("display-info : list parameters for the N-ary operator's display.", [&](std::ostream& outStream) {
        }, [&](std::ostream& outStream, Dialog*& nextDialog) -> bool {

            outStream << "Display of size " << displayData->values.size() << std::endl;

            outStream << "Values are : [";
            for(int i = 0; i < displayData->values.size(); ++i) {
                if(i) {
                    outStream << ", ";
                }
                outStream << displayData->values[i];
            }
            outStream << "]" << std::endl;

            outStream << "First Id : " << displayData->firstId << std::endl;
            outStream << "Second Id : " << displayData->secondId << std::endl;
            outStream << "Parameter : " << displayData->parameter << std::endl;

            nextDialog = nullptr;
            return true;
        }));
    }

    void changeOperator() {
        std::string name;
        in >> name;

        std::shared_ptr<Implicit::Operator::OneParameter::NaryFloat> op;
        if(! Implicit::Operator::Nary::OneParameter::Factory::byName(name, op)) {
            out << "No binary operator found with the name \"" << name << "\"" << std::endl;
            return;
        }
        composite->changeOperator(op);

        viewInScene();
    }

private:
    std::shared_ptr<Implicit::Engine::Vector2f::Composition::OneParameter::NaryComposite> composite;
    std::shared_ptr<Implicit::Engine::Display::NaryFloatOneParameter> displayData;
};

}
}
}
}
