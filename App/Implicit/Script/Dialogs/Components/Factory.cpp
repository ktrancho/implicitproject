// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#include "Factory.hpp"

#include <Implicit/Script/Dialogs/Components/Translation.hpp>
#include <Implicit/Script/Dialogs/Components/ImplicitField.hpp>
#include <Implicit/Script/Dialogs/Components/BinaryComposite.hpp>
#include <Implicit/Script/Dialogs/Components/NaryOneParameterComposite.hpp>
#include <Implicit/Script/Dialogs/Components/BinaryNParametersComposite.hpp>

namespace Implicit {
namespace Script {
namespace Dialogs {
namespace Components {

std::shared_ptr<Implicit::Script::Dialogs::Components::Selector> Factory::createDefault(
        const std::shared_ptr<Implicit::Engine::Tree::Vector2f::Scene>& a_scene,
        const std::shared_ptr<Implicit::Engine::Tree::Component>& a_component, Dialog* a_parent) {
    std::shared_ptr<Implicit::Script::Dialogs::Components::Selector> selector(
            new Implicit::Script::Dialogs::Components::Selector(a_scene, a_component, a_parent));
    return selector;
}

std::shared_ptr<Implicit::Script::Dialogs::Components::Selector> Factory::createByType(
        const std::shared_ptr<Implicit::Engine::Tree::Vector2f::Scene>& a_scene,
        const std::shared_ptr<Implicit::Engine::Tree::Component>& a_component, Dialog* a_parent) {

    if(a_component->getType() == "Translation") {
        std::shared_ptr<Implicit::Script::Dialogs::Components::Translation> selector(
                new Implicit::Script::Dialogs::Components::Translation(a_scene, a_component, a_parent));
        return selector;
    } else if(a_component->getType() == "ImplicitField") {
        std::shared_ptr<Implicit::Script::Dialogs::Components::ImplicitField> selector(
                new Implicit::Script::Dialogs::Components::ImplicitField(a_scene, a_component, a_parent));
        return selector;
    } else if(a_component->getType() == "BinaryComposite") {
        std::shared_ptr<Implicit::Script::Dialogs::Components::BinaryComposite> selector(
                new Implicit::Script::Dialogs::Components::BinaryComposite(a_scene, a_component, a_parent));
        return selector;
    } else if(a_component->getType() == "NaryOneParameterComposite") {
        std::shared_ptr<Implicit::Script::Dialogs::Components::NaryOneParameterComposite> selector(
                new Implicit::Script::Dialogs::Components::NaryOneParameterComposite(a_scene, a_component, a_parent));
        return selector;
    } else if(a_component->getType() == "BinaryNParametersComposite") {
        std::shared_ptr<Implicit::Script::Dialogs::Components::BinaryNParametersComposite> selector(
                new Implicit::Script::Dialogs::Components::BinaryNParametersComposite(a_scene, a_component, a_parent));
        return selector;
    }

    std::shared_ptr<Implicit::Script::Dialogs::Components::Selector> selector(
            new Implicit::Script::Dialogs::Components::Selector(a_scene, a_component, a_parent));
    return selector;
}

}
}
}
}