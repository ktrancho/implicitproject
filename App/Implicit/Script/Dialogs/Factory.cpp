// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#include "Factory.hpp"

#include <Implicit/Script/Dialogs/SceneDialog.hpp>

namespace Implicit {
namespace Script {
namespace Dialogs {

std::shared_ptr<Implicit::Script::Dialog> Factory::createDefault(std::istream& a_in, std::ostream& a_out) {
    std::shared_ptr<Implicit::Script::Dialog> dialog(new Implicit::Script::Dialog(a_in, a_out));
    return dialog;
}

std::shared_ptr<Implicit::Script::Dialog> Factory::createScene(
        const std::shared_ptr<Implicit::Engine::Tree::Vector2f::Scene>& a_scene, std::istream& a_in,
        std::ostream& a_out) {
    std::shared_ptr<Implicit::Script::Dialogs::SceneDialog> dialog(new Implicit::Script::Dialogs::SceneDialog(a_scene, 
            a_in, a_out));
    return dialog;
}

}
}
}