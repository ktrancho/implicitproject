// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <memory>
#include <functional>
#include <string>
#include <ostream>

namespace Implicit {
namespace Script {
class Dialog;
}
}

namespace Implicit {
namespace Script {

struct DialogData {
    DialogData() : DialogData("NONE", [](std::ostream&) {},
            [](std::ostream&, Dialog*&) {return false;}) {}
    DialogData(const std::string& a_lineCommand, const std::function<void (std::ostream&)>& a_help,
                const std::function<bool (std::ostream&, Dialog*& nextDialog)>& a_action) :
        lineCommand(a_lineCommand), help(a_help), action(a_action) {}

    std::string lineCommand;
    std::function<void (std::ostream&)> help;
    std::function<bool (std::ostream&, Dialog*& nextDialog)> action;
};

}
}