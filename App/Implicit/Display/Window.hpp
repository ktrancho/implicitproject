// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <string>
#include <memory>
#include <functional>

#include <Eigen/Dense>

#include <Implicit/Core/Types/Types2D/ImplicitField.hpp>
#include <Implicit/Engine/Display/BinaryOperator.hpp>
#include <Implicit/Engine/Display/NaryFloatOneParameter.hpp>


namespace Implicit {
namespace Display {

class Window {
protected:
    Window(int a_width, int a_height, const std::string& a_caption) :
        width(a_width),
        height(a_height),
        caption(a_caption),

        numberIso(4),
        drawField(true),
        drawTarget(true),
        drawIso(true),
        drawExternal(false),
        drawFarField(true) {

    }

public:
    virtual ~Window() {}

    /// Interface methods to implement : {

    virtual void displayVector2fImplicitField(const std::shared_ptr<Implicit::Core::Vector2f::ImplicitField>& field,
                                              const Eigen::AlignedBox2f& screenRegion,
                                              const Eigen::AlignedBox2f& spaceRegion,
                                              int accuracy = 1) = 0;

    /// }

    /// Interface methods with default implementation : {

    virtual void displayVector2fWorkingField(const std::shared_ptr<Implicit::Core::Vector2f::ImplicitField>& field,
                                             const Eigen::AlignedBox2f& spaceRegion, int accuracy = 1) {
        if(! field.get()) {
            return;
        }

        Eigen::AlignedBox2f region = spaceRegion;

        Eigen::AlignedBox2f view = getScreenRegion();
        view = Eigen::AlignedBox2f(view.min() + height * Eigen::Vector2f::UnitX(),
                view.max() + height * Eigen::Vector2f::UnitX());

        displayVector2fImplicitField(field, view, region, accuracy);
    }

    virtual void displayOperatorWorkingField(const std::shared_ptr<Implicit::Engine::Display::BinaryOperator>& op,
                                             int accuracy = 1) {
        if(! op.get()) {
            return;
        }

        Eigen::AlignedBox2f region = Eigen::AlignedBox2f(-Eigen::Vector2f::Ones(), Eigen::Vector2f::Ones());

        Eigen::AlignedBox2f view = Eigen::AlignedBox2f(Eigen::Vector2f::Zero(), height * Eigen::Vector2f::Ones());

        displayVector2fImplicitField(op, view, region, accuracy);
    }

    virtual void displayOperatorNaryWorkingField(const std::shared_ptr<Implicit::Engine::Display::NaryFloatOneParameter>& op,
            int accuracy = 1) {
    }

    virtual bool init() {
        // do nothing.
        return true;
    }

    virtual bool quit() {
        // do nothing.
        return true;
    }

    // return if we have to continue waiting more.
    virtual bool wait() {
        // do nothing
        return false;
    }

    virtual void clear() {

    }

    virtual void refresh() {

    }

    virtual void mouseGrab(std::function<void (const Eigen::Vector2f& offset)> mouseOffsetHandler,
            std::function<void (void)> render) {

    }

    /// }

    virtual Eigen::AlignedBox2f getScreenRegion() const {
        return Eigen::AlignedBox2f(Eigen::Vector2f::Zero(), width * Eigen::Vector2f::UnitX() + height * Eigen::Vector2f::UnitY());
    }

protected:
    int width;
    int height;
    std::string caption;

public:
    int numberIso;
    bool drawField;
    bool drawTarget;
    bool drawIso;
    bool drawExternal;
    bool drawFarField;
};

}
}
