// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <memory>
#include <Implicit/Display/Window.hpp>

namespace Implicit {
namespace Display {

class Core {
protected:
    Core() {}

public:
    virtual ~Core() {}

    /// Interface methods to redefine : {

    virtual std::shared_ptr<Window> createWindow(int width, int height,
        const std::string& caption = "Implicit display caption") = 0;

    /// }

};

}
}