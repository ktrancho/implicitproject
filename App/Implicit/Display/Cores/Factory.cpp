// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#include "Factory.hpp"

#include <Implicit/Display/Cores/SDL1/Core.hpp>

namespace Implicit {
namespace Display {
namespace Cores {

std::shared_ptr<Implicit::Display::Core> Factory::createSDL1() {
    std::shared_ptr<Implicit::Display::Cores::SDL1::Core> core(new Implicit::Display::Cores::SDL1::Core());
    return core;
}

}
}
}

