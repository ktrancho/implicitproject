// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <Implicit/Display/Core.hpp>

namespace Implicit {
namespace Display {
namespace Cores {
namespace SDL1 {

class Core : public Implicit::Display::Core {
public:

    /// Interface methods : {

    virtual std::shared_ptr<Implicit::Display::Window> createWindow(int width, int height,
        const std::string& caption = "SDL1 Implicit display caption") override;

    /// }
};

}
}
}
}




