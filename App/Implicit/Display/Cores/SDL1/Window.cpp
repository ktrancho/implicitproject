// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#include "Window.hpp"

#include <iostream>
#include <cmath>

#include <Eigen/Dense>
#include <SDL/SDL_gfxPrimitives.h>

namespace Implicit {
namespace Display {
namespace Cores {
namespace SDL1 {

bool Window::init() {
    if(SDL_Init(SDL_INIT_EVERYTHING) != 0) {
        std::cerr << "SDL1 initialization failed" << std::endl;
        return false;
    }

    if(!(screen = SDL_SetVideoMode(width + height, height, 32, SDL_HWSURFACE))) {
        std::cerr << "SDL1 screen initialization failed" << std::endl;
        return false;
    }

    SDL_WM_SetCaption(caption.data(), NULL);

    return true;
}

bool Window::quit() {
    SDL_Quit();

    return true;
}

void Window::clear() {
    SDL_FillRect(screen, NULL, SDL_MapRGB(screen->format, 0, 0, 0));
}

void Window::refresh() {
    SDL_Flip(screen);
}

bool Window::wait() {
    SDL_Event event;

    SDL_WaitEvent(&event);

    switch(event.type) {
        case SDL_QUIT:
            return false;

        case SDL_KEYDOWN: {
            switch(event.key.keysym.sym) {
                case SDLK_ESCAPE:
                    return false;

                default:
                    break;
            }
        } break;

        default:
            break;
    }

    return true;
}

void Window::mouseGrab(std::function<void (const Eigen::Vector2f& offset)> mouseOffsetHandler,
               std::function<void (void)> render) {
    SDL_Event event;
    Eigen::Vector2f offsetMouse = Eigen::Vector2f::Zero();

    bool running = true;
    bool grab = false;

    int FPS = 60;
    int FPS_delay = 1000 / FPS;

    while(running) {

        offsetMouse = Eigen::Vector2f::Zero();

        while(SDL_PollEvent(&event)) {

            switch (event.type) {
                case SDL_KEYDOWN: {
                    switch (event.key.keysym.sym) {
                        case SDLK_ESCAPE: {
                            running = false;
                        } break;

                        default: break;
                    }
                } break;

                case SDL_MOUSEBUTTONDOWN: {
                    grab = true;
                } break;

                case SDL_MOUSEBUTTONUP: {
                    grab = false;
                } break;

                case SDL_MOUSEMOTION: {
                    offsetMouse += Eigen::Vector2f(event.motion.xrel, event.motion.yrel);
                } break;

                default: break;
            }

        }

        if(grab) {
            mouseOffsetHandler(offsetMouse);

            render();
        }

        SDL_Delay(FPS_delay);
    }

}

Eigen::Vector3f Window::computeColor(float targetIso, float fieldValue) {

    float ligneWidth = 0.5 * 1e-1;
    float distanceToIso = fieldValue - targetIso;

    Eigen::Vector3f outsideColor(5, 80, 255);
    Eigen::Vector3f insideColor(182, 18, 18);
    Eigen::Vector3f farOutsideColor(123, 207, 205);
    //Eigen::Vector3f farOutsideColor(255, 255, 255);
    Eigen::Vector3f deepInsideColor(252, 210, 0);

    Eigen::Vector3f color;
    Eigen::Vector3f addColor;
    float ligneDist;
    float ligneFactor;
    float factor;

    if(drawField) {
        color = (distanceToIso > 0.f) ? (insideColor) : (outsideColor);
        color *= std::min(2.f * std::abs(distanceToIso), 1.f);
        if(drawFarField && (std::abs(distanceToIso) > 0.5f)) {
            addColor = (distanceToIso > 0.f) ? (deepInsideColor) : (farOutsideColor);
            factor = std::min((std::abs(distanceToIso) - 0.5f) * 2.f, 1.f);
            color = (1.f - factor) * color + factor * addColor;
        }
    } else {
        color = Eigen::Vector3f::Zero();
    }

    ligneDist = std::abs(1 - std::cos((2 * distanceToIso) * (2 * M_PI) * numberIso));

    if(drawExternal || ((fieldValue > -0.25f / numberIso) && (fieldValue < 1.f + (0.25f / numberIso)))) {
        if(ligneDist < ligneWidth) {
            if (std::abs(distanceToIso) > 0.25f / numberIso) {
                if(drawIso) {
                    if(std::abs(fieldValue - 1.f) < 0.25f / numberIso || std::abs(fieldValue - 0.f) < 0.25f / numberIso) {
                        addColor = 255 * Eigen::Vector3f::Ones();
                    } else {
                        addColor = 255 * 0.6f * Eigen::Vector3f::Ones();
                    }
                } else {
                    addColor = Eigen::Vector3f::Zero();
                }
            } else {
                if(drawTarget) {
                    addColor = 255 * Eigen::Vector3f::UnitY();
                } else if(drawIso) {
                    addColor = 255 * Eigen::Vector3f::Ones();
                } else {
                    addColor = Eigen::Vector3f::Zero();
                }
            }
            ligneFactor = (ligneWidth - ligneDist) / ligneWidth;
            addColor *= ligneFactor;
            for (int d = 0; d < 3; ++d) {
                color[d] = std::max(color[d], addColor[d]);
            }
        }
    }

    return color;
}

void Window::displayVector2fImplicitField(const std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> &field,
                                          const Eigen::AlignedBox2f& screenRegion,
                                          const Eigen::AlignedBox2f& spaceRegion,
                                          int accuracy) {

    int screenWidth = screenRegion.sizes()[0];
    int screenHeight = screenRegion.sizes()[1];

    Eigen::Vector2f spaceRegionSize = spaceRegion.sizes();

    float targetIso = 0.5f;

    field->hasSurfaceIso(targetIso);

    Eigen::Vector2f position;
    float fieldValue;

    Eigen::Vector3f color;
    SDL_Rect pixel;
    pixel.h = accuracy;
    pixel.w = accuracy;

    for(int x = 0; x < screenWidth; x += accuracy) {
        for(int y = 0; y < screenHeight; y += accuracy) {
            position = spaceRegion.min() + ((x * spaceRegionSize[0]) / screenWidth) * Eigen::Vector2f::UnitX() +
                    ((y * spaceRegionSize[1]) / screenHeight) * Eigen::Vector2f::UnitY();
            field->getFloatField(position, fieldValue);

            color = computeColor(targetIso, fieldValue);

            pixel.x = screenRegion.min()[0] + x;
            pixel.y = screenRegion.min()[1] + y;
            SDL_FillRect(screen, &pixel, SDL_MapRGB(screen->format, color[0], color[1], color[2]));
        }
    }
}

void Window::displayOperatorWorkingField(const std::shared_ptr<Implicit::Engine::Display::BinaryOperator>& op,
                                 int accuracy) {
    Implicit::Display::Window::displayOperatorWorkingField(op, accuracy);
    SDL_Rect region;

    int size = 1;

    region.x = (height / 4);
    region.y = (height / 4);
    region.w = size;
    region.h = (height / 2) + size;
    SDL_FillRect(screen, &region, SDL_MapRGB(screen->format, 0, 0, 51));

    region.x = (height / 4);
    region.y = (height / 4);
    region.w = (height / 2) + size;
    region.h = size;
    SDL_FillRect(screen, &region, SDL_MapRGB(screen->format, 51, 0, 0));

    region.x = ((3 * height) / 4);
    region.y = (height / 4);
    region.w = size;
    region.h = (height / 2) + size;
    SDL_FillRect(screen, &region, SDL_MapRGB(screen->format, 51, 0, 0));

    region.x = (height / 4);
    region.y = ((3 * height) / 4);
    region.w = (height / 2) + size;
    region.h = size;
    SDL_FillRect(screen, &region, SDL_MapRGB(screen->format, 0, 0, 51));
}

void Window::displayOperatorNaryWorkingField(
        const std::shared_ptr<Implicit::Engine::Display::NaryFloatOneParameter> &op,
        int accuracy) {

    if(! op.get()) {
        return;
    }

    Eigen::Vector3f outsideColor(5, 80, 255);
    Eigen::Vector3f insideColor(182, 18, 18);

    float step = 0.0125f;
    int max_iter = 1000;
    float targetIso = 0.5f;
    float scale = 0.5f;

    Eigen::Vector3f light = Eigen::Vector3f(0.25f, 0.5f, 1.f);
    light.normalize();

    Eigen::Vector3f albedo = computeColor(0.5f, targetIso);
    float ambient = 0.125f;

    Eigen::AlignedBox2f screenRegion = Eigen::AlignedBox2f(Eigen::Vector2f::Zero(), height * Eigen::Vector2f::Ones());

    int screenWidth = screenRegion.sizes()[0];
    int screenHeight = screenRegion.sizes()[1];

    Eigen::Vector3f lookAt = 0.5f * Eigen::Vector3f::Ones();

    float angleX = -3 * M_PI / 4.f;
    float angleZ = 0.9f * -3 * M_PI / 4.f;

    Eigen::Vector3f cameraDirection =
            - Eigen::Vector3f::UnitY();
    Eigen::Vector3f cameraX =
            Eigen::Vector3f::UnitX();
    Eigen::Vector3f cameraY =
            Eigen::Vector3f::UnitZ();

    Eigen::AngleAxisf rotationX = Eigen::AngleAxisf(angleX, Eigen::Vector3f::UnitX());
    Eigen::AngleAxisf rotationZ = Eigen::AngleAxisf(angleZ, Eigen::Vector3f::UnitZ());

    cameraDirection = rotationX * rotationZ * cameraDirection;
    cameraX = rotationX * rotationZ * cameraX;
    cameraY = rotationX * rotationZ * cameraY;

    /*Eigen::Vector3f cameraDirection = lookAt - cameraPosition;
    Eigen::Vector3f cameraY = Eigen::Vector3f(0, cameraDirection[2], -cameraDirection[1]);
    Eigen::Vector3f cameraX = Eigen::Vector3f(
            - cameraDirection[1] * cameraDirection[1] - cameraDirection[2] * cameraDirection[2],
            - cameraDirection[0] * cameraDirection[1],
            cameraDirection[0] * cameraDirection[2]);*/

    cameraDirection.normalize();
    cameraX.normalize();
    cameraY.normalize();

    cameraX /= scale;
    cameraY /= scale;

    Eigen::Vector3f cameraPosition = lookAt - 1.5f * cameraDirection;

    Eigen::Vector3f color;
    Eigen::Vector3f addColor;
    Eigen::Vector3f normal;
    Eigen::Vector2f screenPosition;
    Eigen::Vector3f spacePosition;
    SDL_Rect pixel;
    pixel.h = accuracy;
    pixel.w = accuracy;

    for(int x = 0; x < screenWidth; x += accuracy) {
        for (int y = 0; y < screenHeight; y += accuracy) {

            float lastIso = 0.f;
            float currentIso = 0.f;

            color = Eigen::Vector3f::Zero();

            screenPosition =
                    ((float)(x) / screenWidth) * Eigen::Vector2f::UnitX() +
                    ((float)(y) / screenHeight) * Eigen::Vector2f::UnitY()
                    - 0.5f * Eigen::Vector2f::Ones();
            screenPosition = Eigen::Rotation2Df(0.9f * M_PI / 4.f) * screenPosition;
            spacePosition =
                    cameraPosition
                    + screenPosition[0] * cameraX
                    + screenPosition[1] * cameraY;

            float tx, ty, tz;

            tx = (0.f - spacePosition[0]) / cameraDirection[0];
            ty = (0.f - spacePosition[1]) / cameraDirection[1];
            tz = (0.f - spacePosition[2]) / cameraDirection[2];

            float t = std::min({tx, ty, tz});

            spacePosition += t * cameraDirection;

            op->evalFloats({spacePosition[0], spacePosition[1], spacePosition[2]}, currentIso);

            color = computeColor(0.5f, currentIso);

            Eigen::Vector3f addColor = Eigen::Vector3f::Zero();

            tx = (1.f - spacePosition[0]) / cameraDirection[0];
            ty = (1.f - spacePosition[1]) / cameraDirection[1];
            tz = (1.f - spacePosition[2]) / cameraDirection[2];

            t = std::max({tx, ty, tz});

            spacePosition += t * cameraDirection;

            op->evalFloats({spacePosition[0], spacePosition[1], spacePosition[2]}, currentIso);

            for(int s = 0; s < max_iter; ++s) {
                lastIso = currentIso;
                spacePosition += 1.f * std::abs(currentIso - targetIso) * cameraDirection;

                if(std::min({spacePosition[0], spacePosition[1], spacePosition[2]}) < 0.f) {

                    tx = (0.f - spacePosition[0]) / cameraDirection[0];
                    ty = (0.f - spacePosition[1]) / cameraDirection[1];
                    tz = (0.f - spacePosition[2]) / cameraDirection[2];

                    t = std::min({tx, ty, tz});

                    spacePosition += t * cameraDirection;

                    float eps = 1e-2;
                    float m;
                    if(
                            spacePosition[2] > std::max(spacePosition[0], spacePosition[1])
                            && (m = std::max({std::abs(spacePosition[0]), std::abs(spacePosition[1])})) < eps
                            && 0.f < spacePosition[2] && spacePosition[2] < 1.f) {
                        addColor = 254.f * ((eps - m) / eps) * Eigen::Vector3f::UnitZ();
                    }
                    if(
                            spacePosition[1] > std::max(spacePosition[0], spacePosition[2])
                            && (m = std::max({std::abs(spacePosition[0]), std::abs(spacePosition[2])})) < eps
                            && 0.f < spacePosition[1] && spacePosition[1] < 1.f) {
                        addColor = 254.f * ((eps - m) / eps) * Eigen::Vector3f::UnitY();
                    }
                    if(
                            spacePosition[0] > std::max(spacePosition[2], spacePosition[1])
                            && (m = std::max({std::abs(spacePosition[1]), std::abs(spacePosition[2])})) < eps
                            && 0.f < spacePosition[0] && spacePosition[0] < 1.f) {
                        addColor = 254.f * ((eps - m) / eps) * Eigen::Vector3f::UnitX();
                    }

                    color = Eigen::Vector3f(
                            std::max(color[0], addColor[0]),
                            std::max(color[1], addColor[1]),
                            std::max(color[2], addColor[2]));

                    break;
                }

                op->evalFloats({spacePosition[0], spacePosition[1], spacePosition[2]}, currentIso);

                /*if(std::max({spacePosition[0], spacePosition[1], spacePosition[2]}) > 1.f) {
                    continue;
                }*/

                if(std::abs(currentIso - targetIso) < 1e-3) {
                //if((lastIso > targetIso && targetIso > currentIso) || (lastIso < targetIso && targetIso < currentIso)) {

                    float fdx, fdy, fdz;
                    float h = 1e-4;
                    op->evalFloats({spacePosition[0] + h, spacePosition[1], spacePosition[2]}, fdx);
                    op->evalFloats({spacePosition[0], spacePosition[1] + h, spacePosition[2]}, fdy);
                    op->evalFloats({spacePosition[0], spacePosition[1], spacePosition[2] + h}, fdz);

                    normal = Eigen::Vector3f(fdx - currentIso, fdy - currentIso, fdz - currentIso);
                    normal.normalize();

                    addColor = albedo * std::max((1.f - ambient) * abs(normal.dot(light)) + ambient, 0.f);

                    float w = 0.95;
                    color = w * addColor + (1 - w) * color;

                    //color = (1.f - ((cameraPosition - spacePosition).norm() / (cameraPosition - lookAt).norm())) * 254.f * Eigen::Vector3f::Ones();

                    break;

                }
            }

            pixel.x = screenRegion.min()[0] + x;
            pixel.y = screenRegion.min()[1] + y;
            SDL_FillRect(screen, &pixel, SDL_MapRGB(screen->format, color[0], color[1], color[2]));
        }
    }

}

}
}
}
}
