// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <SDL/SDL.h>
#include <Implicit/Display/Window.hpp>

namespace Implicit {
namespace Display {
namespace Cores {
namespace SDL1 {

class Window : public Implicit::Display::Window {
public:
    Window(int a_width, int a_height, const std::string& a_caption) :
        Implicit::Display::Window(a_width, a_height, a_caption) {

    }

    /// Interface methods : {

    virtual bool init() override;

    virtual bool quit() override;

    virtual bool wait() override;

    virtual void clear() override;

    virtual void refresh() override;

    virtual void mouseGrab(std::function<void (const Eigen::Vector2f& offset)> mouseOffsetHandler,
                           std::function<void (void)> render) override;

    virtual void displayVector2fImplicitField(const std::shared_ptr<Implicit::Core::Vector2f::ImplicitField>& field,
                                              const Eigen::AlignedBox2f& screenRegion,
                                              const Eigen::AlignedBox2f& spaceRegion,
                                              int accuracy = 1) override;

    virtual void displayOperatorWorkingField(const std::shared_ptr<Implicit::Engine::Display::BinaryOperator>& op,
                                             int accuracy = 1) override;

    virtual void displayOperatorNaryWorkingField(const std::shared_ptr<Implicit::Engine::Display::NaryFloatOneParameter>& op,
                                                 int accuracy = 1) override;

    /// }

private:

    Eigen::Vector3f computeColor(float targetIso, float fieldValue);

    SDL_Surface* screen;
};

}
}
}
}





