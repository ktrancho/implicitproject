// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#include <iostream>

#include <Implicit/Display/Cores/Factory.hpp>
#include <Implicit/Core/Primitives2D/AllFactories.hpp>
#include <Implicit/Engine/Transforms2D/Factory.hpp>
#include <Implicit/Engine/Composite2D/Factory.hpp>
#include <Implicit/Composition/Operators/AllFactories.hpp>
#include <Implicit/Engine/Display/Factory.hpp>
#include <Implicit/Script/Dialogs/Factory.hpp>
#include <Implicit/Engine/Tree/Scene2D/Factory.hpp>
#include <Implicit/Engine/Tree/Utils.hpp>

#include <Implicit/Process/Cluster/PlaneCut.hpp>

int main() {
    // Window init : {

    std::shared_ptr<Implicit::Display::Core> displayCore = Implicit::Display::Cores::Factory::createSDL1();

    std::shared_ptr<Implicit::Display::Window> displayWindow = displayCore->createWindow(800, 600, "Test display window");

    displayWindow->init();

    // }

    // Implicit work : {


    std::vector<Eigen::Vector2f> positions = {
            Eigen::Vector2f(0.8, 1.2),
            Eigen::Vector2f(1, -1),
            Eigen::Vector2f(-0.5, -0.5),
            Eigen::Vector2f(-1.5, 1),
            Eigen::Vector2f(0, 0),
            Eigen::Vector2f(0.4, 0.4),
            Eigen::Vector2f(-0.5, 0.5)
            };

    std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> circle = Implicit::Core::Vector2f::DistanceFields::Factory::createCircle();

    std::vector<std::shared_ptr<Implicit::Core::Vector2f::ImplicitField>> positionFields;
    positionFields.resize(positions.size());

    for(int i = 0; i < positions.size(); ++i) {
        positionFields[i] = Implicit::Engine::Vector2f::Transforms::Factory::translate(
                circle,
                positions[i]
                );
    }

    std::shared_ptr<Implicit::Operator::BinaryFloat> op = Implicit::Operator::Binary::Factory::largeBlending();

    // Binary version : {

    std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> composition =
            Implicit::Engine::Vector2f::Transforms::Factory::translate(
                    circle,
                    positions[0]
                    );

    for(auto it = positionFields.cbegin() + 1; it != positionFields.cend(); ++it) {
        composition =
                Implicit::Engine::Vector2f::Composition::Factory::binaryComposite(
                        composition,
                        *it,
                        op
                        );
    }

    // }

    // N-ary version : {

    std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> parameter =
            Implicit::Core::Vector2f::Parameters::Factory::constantValue(1);

    std::shared_ptr<Implicit::Operator::OneParameter::NaryFloat> nary_operator =
            Implicit::Operator::Nary::OneParameter::Factory::blending();

    std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> nary_composite =
            Implicit::Engine::Vector2f::Composition::Factory::naryOneParameterComposite(
                    positionFields,
                    parameter,
                    nary_operator
                    );


    // }

    // Cluster processing : {

    Implicit::Process::Cluster::PlaneCut planeCut(positions);

    planeCut.process();

    /*std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> smooth_parameter =
            Implicit::Core::Vector2f::Parameters::Factory::constantValue(1);*/

    std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> smooth_parameter =
            Implicit::Engine::Vector2f::Transforms::Factory::cleanBound(
                    Implicit::Core::Vector2f::DistanceFields::Factory::createPlane(
                            Eigen::Vector2f::Ones(),
                            Eigen::Vector2f::Zero(),
                            3.f
                            ),
                    0.f, 1.f
                    );

    std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> width_parameter =
            Implicit::Engine::Vector2f::Transforms::Factory::cleanBound(
                    Implicit::Core::Vector2f::DistanceFields::Factory::createPlane(
                            Eigen::Vector2f::UnitY(),
                            Eigen::Vector2f::Zero(),
                            3.f
                    ),
                    0.f, 1.f
            );

    std::vector<std::shared_ptr<Implicit::Core::Vector2f::ImplicitField>> clusters =
            planeCut.getClusters(
                    smooth_parameter,
                    Implicit::Operator::Nary::OneParameter::Factory::blending()
            );

    for(int i = 0; i < clusters.size(); ++i) {
        clusters[i] =
                Implicit::Engine::Vector2f::Transforms::Factory::fieldScale(
                        clusters[i],
                        width_parameter
                );
    }

    std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> clusterComposite =
            Implicit::Engine::Vector2f::Composition::Factory::naryOneParameterComposite(
                    clusters,
                    Implicit::Core::Vector2f::Parameters::Factory::constantValue(0),
                    Implicit::Operator::Nary::OneParameter::Factory::blending()
            );

    // }

    // Scene visualisation setting : {

    Eigen::AlignedBox2f workingView = Eigen::AlignedBox2f(-2.f * Eigen::Vector2f::Ones(), 2.f * Eigen::Vector2f::Ones());
    workingView.min()[0] *= (8.f / 6.f);
    workingView.max()[0] *= (8.f / 6.f);

    std::shared_ptr<Implicit::Engine::Tree::Vector2f::Scene> scene = Implicit::Engine::Tree::Vector2f::Scenes::Factory::emptyScene(displayWindow, workingView);
    //scene->addImplicit(nary_composite);
    scene->addImplicit(clusterComposite);

    std::shared_ptr<Implicit::Script::Dialog> console = Implicit::Script::Dialogs::Factory::createScene(scene, std::cin, std::cout);
    Implicit::Script::Dialog* currentConsole = console.get();

    // }

    // Rendering : {

    do {
        scene->render();

    } while(currentConsole->use(currentConsole));

    // }


    displayWindow->quit();

    return 0;
}