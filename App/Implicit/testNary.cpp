// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#include <iostream>

#include <Implicit/Display/Cores/Factory.hpp>
#include <Implicit/Core/Primitives2D/AllFactories.hpp>
#include <Implicit/Engine/Transforms2D/Factory.hpp>
#include <Implicit/Engine/Composite2D/Factory.hpp>
#include <Implicit/Composition/Operators/AllFactories.hpp>
#include <Implicit/Engine/Display/Factory.hpp>
#include <Implicit/Script/Dialogs/Factory.hpp>
#include <Implicit/Engine/Tree/Scene2D/Factory.hpp>
#include <Implicit/Engine/Tree/Utils.hpp>

int main() {
    // Window init : {

    std::shared_ptr<Implicit::Display::Core> displayCore =
            Implicit::Display::Cores::Factory::createSDL1();

    std::shared_ptr<Implicit::Display::Window> displayWindow =
            displayCore->createWindow(800, 600, "Test display window");

    displayWindow->init();

    // }

    // Implicit work : {

    int n = 4;

    std::vector<Eigen::Vector2f> points;

    for(int i = 0; i < n; ++i) {
        points.push_back(0.5f * Eigen::Vector2f(std::cos((i * M_PI * 2.) / n), std::sin((i * M_PI * 2.) / n)));
    }

    std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> circle =
            Implicit::Core::Vector2f::DistanceFields::Factory::createCircle();

    std::shared_ptr<Implicit::Operator::BinaryFloat> op =
            Implicit::Operator::Binary::Factory::largeBlending();


    std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> composition =
            Implicit::Engine::Vector2f::Transforms::Factory::translate(
                    circle,
                    points[0]
            );

    for(int i = 1; i < n; ++i) {
        composition =
        Implicit::Engine::Vector2f::Composition::Factory::binaryComposite(
                composition,
                Implicit::Engine::Vector2f::Transforms::Factory::translate(
                        circle,
                        points[i]
                ),
                op
        );
    }

    // N-ary part

    std::vector<std::shared_ptr<Implicit::Core::Vector2f::ImplicitField>> fields;

    for(int i = 0; i < n; ++i) {
        fields.push_back(Implicit::Engine::Vector2f::Transforms::Factory::translate(
                circle,
                points[i]
        ));
    }

    std::shared_ptr<Implicit::Operator::OneParameter::NaryFloat> nary_operator =
            //Implicit::Operator::Nary::OneParameter::Factory::builtBlending();
            Implicit::Operator::Nary::OneParameter::Factory::bulge();

    std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> nary_composition =
            Implicit::Engine::Vector2f::Composition::Factory::naryOneParameterComposite(
                    fields,
                    Implicit::Core::Vector2f::Parameters::Factory::constantValue(0.5f),
                    nary_operator
            );


    // }

    // Scene visualisation setting : {

    Eigen::AlignedBox2f workingView =
            Eigen::AlignedBox2f(-2.f * Eigen::Vector2f::Ones(), 2.f * Eigen::Vector2f::Ones());

    workingView.min()[0] *= (8.f / 6.f);
    workingView.max()[0] *= (8.f / 6.f);

    std::shared_ptr<Implicit::Engine::Tree::Vector2f::Scene> scene =
            Implicit::Engine::Tree::Vector2f::Scenes::Factory::emptyScene(displayWindow, workingView);

    scene->addImplicit(composition);
    scene->addImplicit(nary_composition);

    std::shared_ptr<Implicit::Script::Dialog> console =
            Implicit::Script::Dialogs::Factory::createScene(scene, std::cin, std::cout);

    Implicit::Script::Dialog* currentConsole =
            console.get();

    // }

    // Rendering : {

    do {
        scene->render();

    } while(currentConsole->use(currentConsole));

    // }


    displayWindow->quit();

    return 0;
}