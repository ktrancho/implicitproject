// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#include <iostream>

#include <Implicit/Display/Cores/Factory.hpp>
#include <Implicit/Core/Primitives2D/AllFactories.hpp>
#include <Implicit/Engine/Transforms2D/Factory.hpp>
#include <Implicit/Engine/Composite2D/Factory.hpp>
#include <Implicit/Composition/Operators/AllFactories.hpp>
#include <Implicit/Engine/Display/Factory.hpp>
#include <Implicit/Script/Dialogs/Factory.hpp>
#include <Implicit/Engine/Tree/Scene2D/Factory.hpp>
#include <Implicit/Engine/Tree/Utils.hpp>

int main() {
    // Window init : {

    std::shared_ptr<Implicit::Display::Core> displayCore =
            Implicit::Display::Cores::Factory::createSDL1();

    std::shared_ptr<Implicit::Display::Window> displayWindow =
            displayCore->createWindow(800, 600, "Test display window");

    displayWindow->init();

    // }

    // Implicit work : {

    Eigen::Vector2f position = Eigen::Vector2f(0, -0.7);

    std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> capsule =
            Implicit::Core::Vector2f::DistanceFields::Factory::createCapsule(
                    Eigen::Vector2f(-1, 0),
                    Eigen::Vector2f(1, 0)
                    );

    std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> point =
            Implicit::Core::Vector2f::DistanceFields::Factory::createPoint(position);

    std::shared_ptr<Implicit::Operator::NParameters::BinaryFloat> op =
            Implicit::Operator::Binary::NParameters::Factory::pointSculpt();

    float height;
    capsule->getFloatField(position, height);

    std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> composition =
            Implicit::Engine::Vector2f::Composition::Factory::binaryNParametersComposite(
                    capsule,
                    Implicit::Engine::Vector2f::Transforms::Factory::scale(point, Eigen::Vector2f(0.5f, 1.f)),
                    {
                        Implicit::Core::Vector2f::Parameters::Factory::constantValue(2.f * (0.5f - height)),
                        //Implicit::Core::Vector2f::Parameters::Factory::constantValue(1.f)
                        Implicit::Engine::Vector2f::Transforms::Factory::cleanBound(
                                Implicit::Core::Vector2f::DistanceFields::Factory::createPlane(
                                        Eigen::Vector2f::UnitX(),
                                        Eigen::Vector2f::Zero(),
                                        0.25f
                                        ),
                                0.f,
                                1.f
                                )
                        },
                    op
            );

    composition = Implicit::Engine::Vector2f::Transforms::Factory::scale(composition, 3 * Eigen::Vector2f::Ones());
    composition = Implicit::Engine::Vector2f::Transforms::Factory::translate(composition, 2 * Eigen::Vector2f::UnitY());


    // }

    // Scene visualisation setting : {

    Eigen::AlignedBox2f workingView =
            Eigen::AlignedBox2f(-2.f * Eigen::Vector2f::Ones(), 2.f * Eigen::Vector2f::Ones());

    workingView.min()[0] *= (8.f / 6.f);
    workingView.max()[0] *= (8.f / 6.f);

    std::shared_ptr<Implicit::Engine::Tree::Vector2f::Scene> scene =
            Implicit::Engine::Tree::Vector2f::Scenes::Factory::emptyScene(displayWindow, workingView);

    scene->addImplicit(composition);

    std::shared_ptr<Implicit::Script::Dialog> console =
            Implicit::Script::Dialogs::Factory::createScene(scene, std::cin, std::cout);

    Implicit::Script::Dialog* currentConsole =
            console.get();

    // }

    // Rendering : {

    do {
        scene->render();

    } while(currentConsole->use(currentConsole));

    // }


    displayWindow->quit();

    return 0;
}