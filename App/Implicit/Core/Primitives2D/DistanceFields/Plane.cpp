// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#include "Plane.hpp"

namespace Implicit {
namespace Core {
namespace Vector2f {
namespace DistanceFields {

bool Plane::getFloatField(const Eigen::Vector2f &position, float& value) const {
    if(std::abs(scale) < 1e-6) {
        value = 0.5f + ((position - offset).dot(normal) > 0) ? 0.5f : -0.5f;
        return true;
    }
    value = 0.5f + (position - offset).dot(normal) / scale;
    return true;
}

bool Plane::getGradientField(const Eigen::Vector2f &position, Eigen::Vector2f& gradient) const {
    gradient = normal;
    return true;
}

}
}
}
}
