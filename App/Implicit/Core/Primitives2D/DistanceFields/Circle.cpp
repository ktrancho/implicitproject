// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#include "Circle.hpp"

namespace Implicit {
namespace Core {
namespace Vector2f {
namespace DistanceFields {

bool Circle::getFloatField(const Eigen::Vector2f &position, float& value) const {
    value = (1 - position.norm());
    return true;
}

bool Circle::getGradientField(const Eigen::Vector2f &position, Eigen::Vector2f& gradient) const {
    gradient = -position;
    gradient.normalized();
    return true;
}

bool Circle::hasLimitSupIso(float& limSup) const {
    limSup = 1.f;
    return true;
}

bool Circle::getSurfaceBoundingBox(Eigen::AlignedBox2f& boundingBox) const {
    boundingBox = Eigen::AlignedBox2f(-0.51f * Eigen::Vector2f::Ones(), 0.51f * Eigen::Vector2f::Ones());
    return true;
}

bool Circle::getIsoBoundingBox(float targetIso, Eigen::AlignedBox2f& boundingBox) const {
    float scale = 1 - targetIso;
    scale *= 1.025f;
    boundingBox = Eigen::AlignedBox2f(-scale * Eigen::Vector2f::Ones(), scale * Eigen::Vector2f::Ones());
    return true;
}

}
}
}
}
