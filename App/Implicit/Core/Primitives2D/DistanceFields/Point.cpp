// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#include "Point.hpp"

#include "Segment.hpp"

namespace Implicit {
namespace Core {
namespace Vector2f {
namespace DistanceFields {

bool Point::getFloatField(const Eigen::Vector2f &position, float& value) const {
    value = 0.5 - (position - center).norm();
    return true;
}

bool Point::getGradientField(const Eigen::Vector2f &position, Eigen::Vector2f& gradient) const {
    float n = (position - center).norm();
    if(n < 1e-9) {
        gradient = Eigen::Vector2f::Zero();
        return true;
    }
    gradient = -(position - center) / n;
    return true;
}

bool Point::hasLimitSupIso(float& limSup) const {
    limSup = 1.f;
    return true;
}

bool Point::getSurfaceBoundingBox(Eigen::AlignedBox2f& boundingBox) const {
    boundingBox = Eigen::AlignedBox2f(-0.26f * Eigen::Vector2f::Ones() + center,
                                      0.26f * Eigen::Vector2f::Ones() + center);
    return true;
}

bool Point::getIsoBoundingBox(float targetIso, Eigen::AlignedBox2f& boundingBox) const {
    float scale = 0.5 - targetIso;
    scale *= 1.025f;
    boundingBox = Eigen::AlignedBox2f(-scale * Eigen::Vector2f::Ones() + center,
                                      scale * Eigen::Vector2f::Ones() + center);
    return true;
}

}
}
}
}
