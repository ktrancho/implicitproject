// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <Eigen/Dense>

#include <Implicit/Core/Types/Types2D/ImplicitField.hpp>

namespace Implicit {
namespace Core {
namespace Vector2f {
namespace DistanceFields {

class Plane : public ImplicitField {
public:
    Plane(const Eigen::Vector2f& a_normal = Eigen::Vector2f::UnitX(),
            const Eigen::Vector2f& a_offset = Eigen::Vector2f::Zero(),
            float a_scale = 1.f) :
        normal(a_normal),
        offset(a_offset),
        scale(a_scale) {
        normal.normalized();
    }

    virtual bool getFloatField(const Eigen::Vector2f &position, float& value) const override;

    virtual bool getGradientField(const Eigen::Vector2f &position, Eigen::Vector2f& gradient) const override;

    virtual std::string getName() const override {
        return std::string("Plane");
    }

private:
    Eigen::Vector2f normal;
    Eigen::Vector2f offset;
    float scale;
};

}
}
}
}