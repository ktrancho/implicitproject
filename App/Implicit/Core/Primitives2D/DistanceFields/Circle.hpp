// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <Implicit/Core/Types/Types2D/ImplicitField.hpp>

namespace Implicit {
namespace Core {
namespace Vector2f {
namespace DistanceFields {

class Circle : public ImplicitField {
public:
    Circle() {}

    virtual bool getFloatField(const Eigen::Vector2f &position, float& value) const override;

    virtual bool getGradientField(const Eigen::Vector2f &position, Eigen::Vector2f& gradient) const override;

    virtual bool hasLimitSupIso(float& limSup) const override;

    virtual bool getSurfaceBoundingBox(Eigen::AlignedBox2f& boundingBox) const override;

    virtual bool getIsoBoundingBox(float targetIso, Eigen::AlignedBox2f& boundingBox) const override;

    virtual std::string getName() const override {
        return std::string("Circle");
    }
};

}
}
}
}