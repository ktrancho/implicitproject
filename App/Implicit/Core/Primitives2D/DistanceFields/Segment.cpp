// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#include "Segment.hpp"

namespace Implicit {
namespace Core {
namespace Vector2f {
namespace DistanceFields {

bool Segment::getFloatField(const Eigen::Vector2f &position, float& value) const {
    if((position - first).dot(second - first) < 0) {
        value = 0.5 - (position - first).norm();
        return true;
    } else if((position - second).dot(first - second) < 0) {
        value = 0.5 - (position - second).norm();
        return true;
    }
    Eigen::Vector2f rotated = Eigen::Vector2f(-(second[1] - first[1]), (second[0] - first[0]));
    value = 0.5 - std::abs((position - first).dot(rotated / (second - first).norm()));
    return true;
}

bool Segment::getGradientField(const Eigen::Vector2f &position, Eigen::Vector2f& gradient) const {

    if((position - first).dot(second - first) < 0) {
        gradient = -(position - first) / (position - first).norm();
        return true;
    } else if((position - second).dot(first - second) < 0) {
        gradient = -(position - second) / (position - second).norm();
        return true;
    }
    Eigen::Vector2f rotated = Eigen::Vector2f(-(second[1] - first[1]), (second[0] - first[0]));
    Eigen::Vector2f n = rotated / (second - first).norm();
    if((position - first).dot(n) > 0) {
        gradient = -n;
    } else {
        gradient = n;
    }
    return true;
}

bool Segment::hasLimitSupIso(float& limSup) const {
    limSup = 1.f;
    return true;
}

bool Segment::getSurfaceBoundingBox(Eigen::AlignedBox2f& boundingBox) const {
    boundingBox = Eigen::AlignedBox2f(-0.26f * Eigen::Vector2f::Ones() + Eigen::Vector2f(std::min(float(first[0]), float(second[0])), std::min(float(first[1]), float(second[1]))),
                                      0.26f * Eigen::Vector2f::Ones() + Eigen::Vector2f(std::max(float(first[0]), float(second[0])), std::max(float(first[1]), float(second[1]))));
    return true;
}

bool Segment::getIsoBoundingBox(float targetIso, Eigen::AlignedBox2f& boundingBox) const {
    float scale = 0.5 - targetIso;
    scale *= 1.025f;
    boundingBox = Eigen::AlignedBox2f(-scale * Eigen::Vector2f::Ones() + Eigen::Vector2f(std::min(float(first[0]), float(second[0])), std::min(float(first[1]), float(second[1]))),
                                      scale * Eigen::Vector2f::Ones() + Eigen::Vector2f(std::max(float(first[0]), float(second[0])), std::max(float(first[1]), float(second[1]))));
    return true;
}

}
}
}
}
