// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#include "Factory.hpp"

#include <Implicit/Core/Primitives2D/DistanceFields/Circle.hpp>
#include <Implicit/Core/Primitives2D/DistanceFields/Plane.hpp>
#include <Implicit/Core/Primitives2D/DistanceFields/Capsule.hpp>
#include <Implicit/Core/Primitives2D/DistanceFields/Segment.hpp>
#include <Implicit/Core/Primitives2D/DistanceFields/Point.hpp>

namespace Implicit {
namespace Core {
namespace Vector2f {
namespace DistanceFields {

std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> Factory::createCircle() {
    std::shared_ptr<Implicit::Core::Vector2f::DistanceFields::Circle> circle(
            new Implicit::Core::Vector2f::DistanceFields::Circle());
    return circle;
}

std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> Factory::createCapsule(const Eigen::Vector2f& start,
                                                                               const Eigen::Vector2f& end) {
    std::shared_ptr<Implicit::Core::Vector2f::DistanceFields::Capsule> circle(
            new Implicit::Core::Vector2f::DistanceFields::Capsule(start, end));
    return circle;
}

std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> Factory::createSegment(const Eigen::Vector2f& start,
                                                                                const Eigen::Vector2f& end) {
    std::shared_ptr<Implicit::Core::Vector2f::DistanceFields::Segment> circle(
            new Implicit::Core::Vector2f::DistanceFields::Segment(start, end));
    return circle;
}

std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> Factory::createPoint(const Eigen::Vector2f& center) {
    std::shared_ptr<Implicit::Core::Vector2f::DistanceFields::Point> point(
            new Implicit::Core::Vector2f::DistanceFields::Point(center));
    return point;
}

std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> Factory::createPlane(
        const Eigen::Vector2f& a_normal,
        const Eigen::Vector2f& a_offset,
        float a_scale) {
    std::shared_ptr<Implicit::Core::Vector2f::DistanceFields::Plane> plane(
            new Implicit::Core::Vector2f::DistanceFields::Plane(a_normal, a_offset, a_scale));
    return plane;
}

}
}
}
}