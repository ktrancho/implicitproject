// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <Implicit/Core/Types/Types2D/ImplicitField.hpp>

namespace Implicit {
namespace Core {
namespace Vector2f {
namespace Parameters {

class ConstantValue : public ImplicitField {
public:
    ConstantValue(float a_value) : parameter(a_value) {}

    virtual bool getFloatField(const Eigen::Vector2f &position, float& value) const override {
        value = parameter;
        return true;
    }

    virtual bool getGradientField(const Eigen::Vector2f &position, Eigen::Vector2f& gradient) const override {
        gradient = Eigen::Vector2f::Zero();
        return true;
    }

    virtual bool hasLimitSupIso(float& limSup) const override {
        return false;
    }

    virtual bool getSurfaceBoundingBox(Eigen::AlignedBox2f& boundingBox) const override {
        return false;
    }

    virtual bool getIsoBoundingBox(float targetIso, Eigen::AlignedBox2f& boundingBox) const override {
        return false;
    }

    virtual std::string getName() const override {
        return std::string("ConstantValue");
    }

private:
    float parameter;
};

}
}
}
}