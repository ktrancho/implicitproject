// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#include "Factory.hpp"

#include <Implicit/Core/Primitives2D/Parameters/ConstantValue.hpp>

namespace Implicit {
namespace Core {
namespace Vector2f {
namespace Parameters {

std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> Factory::constantValue(float value) {
    std::shared_ptr<Implicit::Core::Vector2f::Parameters::ConstantValue> field(new Implicit::Core::Vector2f::Parameters::ConstantValue(value));
    return field;
}

}
}
}
}