// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <memory>

#include <Eigen/Dense>

#include <Implicit/Core/Types/Types2D/ImplicitField.hpp>

namespace Implicit {
namespace Core {
namespace Vector2f {
namespace Parameters {

class Factory {
public:
    static std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> constantValue(float value);
};

}
}
}
}
