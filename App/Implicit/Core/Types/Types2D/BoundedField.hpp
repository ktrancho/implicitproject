// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <Eigen/Geometry>

namespace Implicit {
namespace Core {
namespace Vector2f {

class BoundedField {
protected:
    BoundedField() {}

public:
    virtual ~BoundedField() {}

    /// Interface methods to implement : {

    /// Can return false in case the field is not bounded or unknown.
    virtual bool getSpaceBoundingBox(Eigen::AlignedBox2f& boundingBox) const {
        return false;
    }

    /// By default we don't assume field have a maximum.
    virtual bool hasLimitSupIso(float& limSup) const {
        return false;
    }

    /// By default we don't assume field have a minimum.
    virtual bool hasLimitInfIso(float& limInf) const {
        return false;
    }

    /// }
};

}
}
}
