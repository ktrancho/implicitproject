// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <Eigen/Dense>

namespace Implicit {
namespace Core {
namespace Vector2f {

class GradientField {
protected:
    GradientField() {}

public:
    virtual ~GradientField() {}

    /// Interface methods to implement : {

    virtual bool getGradientField(const Eigen::Vector2f &position, Eigen::Vector2f& gradient) const = 0;

    /// }
};

}
}
}
