// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <Eigen/Geometry>

namespace Implicit {
namespace Core {
namespace Vector2f {

class SurfaceField {
protected:
    SurfaceField() {}

public:
    virtual ~SurfaceField() {}

    /// Interface methods to implement : {

    /// Can return false in case the surface is not bounded or bounding box is not known.
    virtual bool getSurfaceBoundingBox(Eigen::AlignedBox2f& boundingBox) const {
        return false;
    }

    /// Allow to look at different isovalues. By default don't know how to find isosurfaces bounding boxes.
    virtual bool getIsoBoundingBox(float targetIso, Eigen::AlignedBox2f& boundingBox) const {
        return false;
    }

    /// Can return false if there is not any surface but if there is one, target must be provided.
    virtual bool hasSurfaceIso(float& targetIsoValue) const {
        return false;
    }

    /// }
};

}
}
}