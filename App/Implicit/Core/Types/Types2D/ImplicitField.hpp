// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <Eigen/Dense>

#include <Implicit/Core/Types/Types2D/Types.hpp>
#include <Implicit/Core/Types/Types2D/FloatField.hpp>
#include <Implicit/Core/Types/Types2D/GradientField.hpp>
#include <Implicit/Core/Types/Types2D/BoundedField.hpp>
#include <Implicit/Core/Types/Types2D/SurfaceField.hpp>
#include <Implicit/Engine/Tree/Component.hpp>

namespace Implicit {
namespace Core {
namespace Vector2f {

class ImplicitField : public Implicit::Engine::Tree::Component, public FloatField, public GradientField, public BoundedField, public SurfaceField {
protected:
    ImplicitField() {}

public:
    virtual ~ImplicitField() {}

    /// Have to redefine methods from :
    /// FloatField
    /// GradientField
    /// BoundedField
    /// SurfaceField

    /// Unbounded field support by default.
    /// If field is bounded surface has to be bounded but bounded surface does not assume bounded field.
    virtual bool getSpaceBoundingBox(Eigen::AlignedBox2f& boundingBox) const override {
        return false;
    }

    /// Unbounded surface by default.
    virtual bool getSurfaceBoundingBox(Eigen::AlignedBox2f& boundingBox) const override {
        return false;
    }

    /// Assume that a default surface should be at iso = 0.5f by convention.
    virtual bool hasSurfaceIso(float& targetIsoValue) const override {
        targetIsoValue = 0.5f;
        return true;
    }

    /// Interface methods with default implementation : {

    virtual bool getImplicitField(const Eigen::Vector2f &position, ImplicitValue& value) const {
        bool check = getFloatField(position, value.field);
        check = getGradientField(position, value.gradient) && check; // allow check in case error has a default value
        return check;
    }

    /// }

    virtual std::string getType() const override {
        return std::string("ImplicitField");
    }

    virtual std::string getName() const override {
        return std::string("ImplicitField");
    }
};

}
}
}