// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#include <iostream>
#include <Implicit/Display/Cores/Factory.hpp>
#include <Implicit/Core/Primitives2D/AllFactories.hpp>
#include <Implicit/Engine/Transforms2D/Factory.hpp>
#include <Implicit/Engine/Composite2D/Factory.hpp>
#include <Implicit/Composition/Operators/AllFactories.hpp>
#include <Implicit/Engine/Display/Factory.hpp>
#include <Implicit/Script/Dialogs/Factory.hpp>
#include <Implicit/Engine/Tree/Scene2D/Factory.hpp>
#include <Implicit/Engine/Tree/Utils.hpp>

int main() {

    std::shared_ptr<Implicit::Display::Core> displayCore = Implicit::Display::Cores::Factory::createSDL1();

    std::shared_ptr<Implicit::Display::Window> displayWindow = displayCore->createWindow(800, 600, "Test display window");

    std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> primitiveCircle = Implicit::Core::Vector2f::DistanceFields::Factory::createCircle();

    std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> firstCircle =
            Implicit::Engine::Vector2f::Transforms::Factory::translate(primitiveCircle, -0.4f * Eigen::Vector2f::UnitX());

    std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> secondCircle =
            Implicit::Engine::Vector2f::Transforms::Factory::translate(primitiveCircle, 0.4f * Eigen::Vector2f::UnitX());

    /*std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> capsule = Implicit::Core::Vector2f::DistanceFields::Factory::createCapsule(
            0.4f * Eigen::Vector2f::UnitX() - 0.4f * Eigen::Vector2f::UnitY(),
            0.4f * Eigen::Vector2f::UnitX() + 0.4f * Eigen::Vector2f::UnitY());*/
    std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> capsule = Implicit::Core::Vector2f::DistanceFields::Factory::createCapsule(
            Eigen::Vector2f(-0.5, -0.25),
            Eigen::Vector2f(-0.25, -0.25));

    std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> otherCapsule = Implicit::Core::Vector2f::DistanceFields::Factory::createCapsule(
            Eigen::Vector2f(0.5, 0.25),
            Eigen::Vector2f(0.25, 0.25));

    std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> firstCapsule =
            Implicit::Engine::Vector2f::Transforms::Factory::translate(capsule, Eigen::Vector2f::Zero());

    std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> secondCapsule =
            Implicit::Engine::Vector2f::Transforms::Factory::translate(otherCapsule, Eigen::Vector2f::Zero());


    //std::shared_ptr<Implicit::Operator::BinaryFloat> op = Implicit::Operator::Binary::Factory::largeBlending();
    std::shared_ptr<Implicit::Operator::BinaryFloat> op = Implicit::Operator::Binary::Factory::maxUnion();
    std::shared_ptr<Implicit::Operator::BinaryFloat> blendOp = Implicit::Operator::Binary::Factory::largeBlending();
    std::shared_ptr<Implicit::Operator::BinaryFloat> bulgeOp = Implicit::Operator::Binary::Factory::fullBulge();
    //std::shared_ptr<Implicit::Operator::BinaryFloat> bulgeOp = Implicit::Operator::Binary::Factory::oneSidedFullBulge();

    //std::shared_ptr<Implicit::Engine::Display::BinaryOperator> operatorDisplay = Implicit::Engine::Display::Factory::binary(op);

    std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> curve = Implicit::Engine::Vector2f::Composition::Factory::binaryComposite(
            Implicit::Engine::Vector2f::Composition::Factory::lineCurve(op, {
                Eigen::Vector2f(-2, -1),
                Eigen::Vector2f(-1.6, -0.4),
                Eigen::Vector2f(-1.2, 0),
                Eigen::Vector2f(-0.8, 0),
                Eigen::Vector2f(0, -0.4),
                Eigen::Vector2f(0.5, -1)
        }),
            Implicit::Engine::Vector2f::Composition::Factory::lineCurve(op, {
                Eigen::Vector2f(1.5, -1),
                Eigen::Vector2f(2, 0)
        }),
    op);

    /*std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> composition = Implicit::Engine::Vector2f::Composition::Factory::binaryComposite(
            firstCircle, capsule, op);*/

    std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> composition = Implicit::Engine::Vector2f::Composition::Factory::binaryComposite(
            firstCapsule, secondCapsule, bulgeOp);

    Eigen::AlignedBox2f workingView = Eigen::AlignedBox2f(-2.f * Eigen::Vector2f::Ones(), 2.f * Eigen::Vector2f::Ones());
    workingView.min()[0] *= (8.f / 6.f);
    workingView.max()[0] *= (8.f / 6.f);

    /*std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> composition = Implicit::Engine::Vector2f::Composition::Factory::binaryComposite(
            firstPlane, secondPlane, Implicit::Operator::Binary::Factory::largeBlending());*/

    displayWindow->init();

    std::shared_ptr<Implicit::Engine::Tree::Vector2f::Scene> scene = Implicit::Engine::Tree::Vector2f::Scenes::Factory::emptyScene(displayWindow, workingView);
    scene->addImplicit(composition);

    //displayWindow->drawField = false;
    //displayWindow->drawExternal = true;
    //displayWindow->drawIso = false;
    //displayWindow->numberIso = 16;
    //displayWindow->drawTarget = false;
    //displayWindow->drawFarField = false;

    std::shared_ptr<Implicit::Script::Dialog> console = Implicit::Script::Dialogs::Factory::createScene(scene, std::cin, std::cout);
    Implicit::Script::Dialog* currentConsole = console.get();

    do {
        scene->render();

    } while(currentConsole->use(currentConsole));

    /*do {

    } while(displayWindow->wait());*/


    displayWindow->quit();

	return 0;
}