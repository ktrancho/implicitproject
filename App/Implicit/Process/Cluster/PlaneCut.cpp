// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#include "PlaneCut.hpp"

#include <Implicit/Core/Primitives2D/Parameters/Factory.hpp>
#include <Implicit/Core/Primitives2D/DistanceFields/Factory.hpp>

namespace Implicit {
namespace Process {
namespace Cluster {

bool PlaneCut::process() {
    if(positions.empty()) {
        std::cerr << "PlaneCut : processing of no positions" << std::endl;
        return false;
    } else if(positions.size() == 1) {
        std::vector<std::shared_ptr<Implicit::Core::Vector2f::ImplicitField>> fields;
        fields.push_back(Implicit::Core::Vector2f::Parameters::Factory::constantValue(1));
        clusters.push_back(std::shared_ptr<Implicit::Process::Cluster::PlaneSet>(new Implicit::Process::Cluster::PlaneSet(fields)));
        return true;
    }
    clusters.resize(positions.size());
    for(int i = 0; i < clusters.size(); ++i) {
        std::vector<std::shared_ptr<Implicit::Core::Vector2f::ImplicitField>> fields;
        for(int k = 0; k < clusters.size(); ++k) {
            if(i == k) {
                continue;
            }
            fields.push_back(Implicit::Core::Vector2f::DistanceFields::Factory::createPlane(
                    (positions[k] - positions[i]).normalized(),
                    (positions[k] + positions[i]) / 2,
                    (positions[k] - positions[i]).norm()
                    ));
        }
        clusters[i] = (std::shared_ptr<Implicit::Process::Cluster::PlaneSet>(new Implicit::Process::Cluster::PlaneSet(fields)));
    }
    return true;
}

}
}
}