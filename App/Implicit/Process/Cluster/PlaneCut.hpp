// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <iostream>
#include <memory>
#include <vector>

#include <Eigen/Dense>

#include <Implicit/Core/Types/All.hpp>
#include <Implicit/Process/Cluster/PlaneSet.hpp>

namespace Implicit {
namespace Process {
namespace Cluster {

class PlaneCut {
public:
    PlaneCut(const std::vector<Eigen::Vector2f>& a_positions) :
        positions(a_positions) {
    }

    bool process();

    std::vector<std::shared_ptr<Implicit::Core::Vector2f::ImplicitField>> getClusters(
            const std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> &a_parameter,
            const std::shared_ptr<Implicit::Operator::OneParameter::NaryFloat> &a_operator) {
        if(clusters.empty()) {
            std::cerr << "PlaneCut : getting empty clusters, please process before doing this" << std::endl;
        }
        std::vector<std::shared_ptr<Implicit::Core::Vector2f::ImplicitField>> planes;
        planes.resize(clusters.size());
        for(int i = 0; i < clusters.size(); ++i) {
            planes[i] = getCluster(i, a_parameter, a_operator);
        }
        return planes;
    }

    std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> getCluster(
            int i,
            const std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> &a_parameter,
            const std::shared_ptr<Implicit::Operator::OneParameter::NaryFloat> &a_operator) {
        return clusters[i]->compose(a_parameter, a_operator);
    }

private:
    std::vector<Eigen::Vector2f> positions;
    std::vector<std::shared_ptr<Implicit::Process::Cluster::PlaneSet>> clusters;
};

}
}
}