// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <memory>
#include <vector>

#include <Eigen/Dense>
#include <Implicit/Core/Types/All.hpp>
#include <Implicit/Composition/Types/All.hpp>
#include <Implicit/Engine/Composite2D/Factory.hpp>
#include <Implicit/Engine/Transforms2D/Factory.hpp>

namespace Implicit {
namespace Process {
namespace Cluster {

class PlaneSet {
public:
    PlaneSet(const std::vector<std::shared_ptr<Implicit::Core::Vector2f::ImplicitField>>& a_planes):
        planes(a_planes) {
    }

    std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> compose(
            const std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> &a_parameter,
            const std::shared_ptr<Implicit::Operator::OneParameter::NaryFloat> &a_operator) {
        std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> composite =
                Implicit::Engine::Vector2f::Composition::Factory::naryOneParameterComposite(
                        planes,
                        a_parameter,
                        a_operator
                );
        return Implicit::Engine::Vector2f::Transforms::Factory::invert(composite);
    }

private:
    std::vector<std::shared_ptr<Implicit::Core::Vector2f::ImplicitField>> planes;
};

}
}
}
