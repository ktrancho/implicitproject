// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#include "Factory.hpp"

#include <Implicit/Composition/Operators/Binary/MaxUnion.hpp>
#include <Implicit/Composition/Operators/Binary/LargeBlending.hpp>
#include <Implicit/Composition/Operators/Binary/Join.hpp>
#include <Implicit/Composition/Operators/Binary/Swap.hpp>
#include <Implicit/Composition/Operators/Binary/OneSidedFullStretching.hpp>
#include <Implicit/Composition/Operators/Binary/OneSidedFullStretchingAgainstPreserve.hpp>
#include <Implicit/Composition/Operators/Binary/Preserve.hpp>
#include <Implicit/Composition/Operators/Binary/Plus.hpp>
#include <Implicit/Composition/Operators/Binary/Intersection.hpp>
#include <Implicit/Composition/Operators/Binary/Difference.hpp>
#include <Implicit/Composition/Operators/Binary/OneSidedContact.hpp>
#include <Implicit/Composition/Operators/Binary/OneSidedFullBulge.hpp>
#include <Implicit/Composition/Operators/Binary/TestDome.hpp>

namespace Implicit {
namespace Operator {
namespace Binary {

std::shared_ptr<Implicit::Operator::BinaryFloat> Factory::maxUnion() {
    std::shared_ptr<Implicit::Operator::Binary::MaxUnion> op(new Implicit::Operator::Binary::MaxUnion());
    return op;
}

std::shared_ptr<Implicit::Operator::BinaryFloat> Factory::largeBlending() {
    std::shared_ptr<Implicit::Operator::Binary::LargeBlending> op(new Implicit::Operator::Binary::LargeBlending());
    return op;
}

std::shared_ptr<Implicit::Operator::BinaryFloat> Factory::oneSidedFullStretching() {
    std::shared_ptr<Implicit::Operator::Binary::OneSidedFullStretching> op(new Implicit::Operator::Binary::OneSidedFullStretching());
    return op;
}

std::shared_ptr<Implicit::Operator::BinaryFloat> Factory::oneSidedFullBulge() {
    std::shared_ptr<Implicit::Operator::Binary::OneSidedFullBulge> op(new Implicit::Operator::Binary::OneSidedFullBulge());
    return op;
}

std::shared_ptr<Implicit::Operator::BinaryFloat> Factory::oneSidedFullStretchingAgainstPreserve() {
    std::shared_ptr<Implicit::Operator::Binary::OneSidedFullStretchingAgainstPreserve> op(new Implicit::Operator::Binary::OneSidedFullStretchingAgainstPreserve());
    return op;
}

std::shared_ptr<Implicit::Operator::BinaryFloat> Factory::preserve() {
    std::shared_ptr<Implicit::Operator::Binary::Preserve> op(new Implicit::Operator::Binary::Preserve());
    return op;
}

std::shared_ptr<Implicit::Operator::BinaryFloat> Factory::oneSidedContact() {
    std::shared_ptr<Implicit::Operator::Binary::OneSidedContact> op(new Implicit::Operator::Binary::OneSidedContact());
    return op;
}

std::shared_ptr<Implicit::Operator::BinaryFloat> Factory::plus() {
    std::shared_ptr<Implicit::Operator::Binary::Plus> op(new Implicit::Operator::Binary::Plus());
    return op;
}

std::shared_ptr<Implicit::Operator::BinaryFloat> Factory::testDome(float pos, float size) {
    std::shared_ptr<Implicit::Operator::Binary::TestDome> op(new Implicit::Operator::Binary::TestDome(pos, size));
    return op;
}

std::shared_ptr<Implicit::Operator::BinaryFloat> Factory::swap(const std::shared_ptr<Implicit::Operator::BinaryFloat>& a_op) {
    std::shared_ptr<Implicit::Operator::Binary::Swap> op(new Implicit::Operator::Binary::Swap(a_op));
    return op;
}

std::shared_ptr<Implicit::Operator::BinaryFloat> Factory::intersection(const std::shared_ptr<Implicit::Operator::BinaryFloat>& a_op) {
    std::shared_ptr<Implicit::Operator::Binary::Intersection> op(new Implicit::Operator::Binary::Intersection(a_op));
    return op;
}

std::shared_ptr<Implicit::Operator::BinaryFloat> Factory::difference(const std::shared_ptr<Implicit::Operator::BinaryFloat>& a_op) {
    std::shared_ptr<Implicit::Operator::Binary::Difference> op(new Implicit::Operator::Binary::Difference(a_op));
    return op;
}

std::shared_ptr<Implicit::Operator::BinaryFloat> Factory::join(const std::shared_ptr<Implicit::Operator::BinaryFloat>& a_op,
                                                               const std::shared_ptr<Implicit::Operator::BinaryFloat>& a_first,
                                                               const std::shared_ptr<Implicit::Operator::BinaryFloat>& a_second) {
    std::shared_ptr<Implicit::Operator::Binary::Join> op(new Implicit::Operator::Binary::Join(a_op, a_first, a_second));
    return op;
}

std::shared_ptr<Implicit::Operator::BinaryFloat> Factory::fullStretching() {
    return join(maxUnion(), oneSidedFullStretching(), swap(oneSidedFullStretching()));
}

std::shared_ptr<Implicit::Operator::BinaryFloat> Factory::fullBulge() {
    return join(maxUnion(), oneSidedFullBulge(), swap(oneSidedFullBulge()));
}

std::shared_ptr<Implicit::Operator::BinaryFloat> Factory::testDomeJoin(const std::shared_ptr<Implicit::Operator::BinaryFloat>& a_op,
        float pos1, float size1, float pos2, float size2) {
    return join(a_op, testDome(pos1, size1), swap(testDome(pos2, size2)));
}

std::shared_ptr<Implicit::Operator::BinaryFloat> Factory::fullStretchingVsPreserve() {
    return join(maxUnion(), oneSidedFullStretchingAgainstPreserve(), swap(preserve()));
}

std::shared_ptr<Implicit::Operator::BinaryFloat> Factory::contact() {
    return join(maxUnion(), oneSidedContact(), swap(oneSidedContact()));
}

bool Factory::byName(const std::string& name, std::shared_ptr<Implicit::Operator::BinaryFloat>& res) {
    // TODO : ça pique les yeux, faire une hashMap ou un truc du genre (laisser possibilité d'enregistrer un nouvel opérateur)
    if(name == "LargeBlending") {
        res = largeBlending();
        return true;
    } else if(name == "MaxUnion") {
        res = maxUnion();
        return true;
    } else if(name == "OneSidedFullStretching") {
        res = oneSidedFullStretching();
        return true;
    } else if(name == "OneSidedFullBulge") {
        res = oneSidedFullBulge();
        return true;
    } else if(name == "OneSidedFullStretchingAgainstPreserve") {
        res = oneSidedFullStretchingAgainstPreserve();
        return true;
    } else if(name == "OneSidedContact") {
        res = oneSidedContact();
        return true;
    } else if(name == "Preserve") {
        res = preserve();
        return true;
    } else if(name == "Plus") {
        res = plus();
        return true;
    } else if(name == "TestDome") {
        res = testDome();
        return true;
    } else if(name == "TestDomeContact") {
        res = testDomeJoin(contact());
        return true;
    } else if(name == "TestDomeContactStretch") {
        res = testDomeJoin(fullStretching());
        return true;
    } else if(name == "TestDomeContactBulge") {
        res = testDomeJoin(fullBulge());
        return true;
    } else if(name == "TestDomeBlending") {
        res = testDomeJoin(largeBlending());
        return true;
    } else if(name == "FullStretching") {
        res = fullStretching();
        return true;
    } else if(name == "FullBulge") {
        res = fullBulge();
        return true;
    } else if(name == "FullStretchingVsPreserve") {
        res = fullStretchingVsPreserve();
        return true;
    } else if(name == "Contact") {
        res = contact();
        return true;
    } else if(name == "MaxIntersection") {
        res = intersection(maxUnion());
        return true;
    } else if(name == "BlendedIntersection") {
        res = intersection(largeBlending());
        return true;
    } else if(name == "StretchedIntersection") {
        res = intersection(fullStretching());
        return true;
    } else if(name == "PlusIntersection") {
        res = intersection(plus());
        return true;
    } else if(name == "MaxDifference") {
        res = difference(maxUnion());
        return true;
    } else if(name == "BlendedDifference") {
        res = difference(largeBlending());
        return true;
    } else if(name == "StretchedDifference") {
        res = difference(fullStretching());
        return true;
    } else if(name == "PlusDifference") {
        res = difference(plus());
        return true;
    }
    return false;
}

std::vector<std::string> Factory::getNames() {
    std::vector<std::string> names;
    names.push_back("LargeBlending");
    names.push_back("MaxUnion");
    names.push_back("OneSidedFullStretching");
    names.push_back("OneSidedFullBulge");
    names.push_back("OneSidedFullStretchingAgainstPreserve");
    names.push_back("OneSidedContact");
    names.push_back("Preserve");
    names.push_back("Plus");
    names.push_back("TestDome");
    names.push_back("TestDomeContact");
    names.push_back("TestDomeContactStretch");
    names.push_back("TestDomeContactBulge");
    names.push_back("TestDomeBlending");
    names.push_back("FullStretching");
    names.push_back("FullBulge");
    names.push_back("FullStretchingVsPreserve");
    names.push_back("Contact");
    names.push_back("MaxIntersection");
    names.push_back("BlendedIntersection");
    names.push_back("StretchedIntersection");
    names.push_back("PlusIntersection");
    names.push_back("MaxDifference");
    names.push_back("BlendedDifference");
    names.push_back("StretchedDifference");
    names.push_back("PlusDifference");
    return names;
}

}
}
}
