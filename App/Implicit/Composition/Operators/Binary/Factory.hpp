// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <memory>
#include <string>
#include <vector>

#include <Implicit/Composition/Types/BinaryFloat.hpp>

namespace Implicit {
namespace Operator {
namespace Binary {

class Factory {
public:
    static std::shared_ptr<Implicit::Operator::BinaryFloat> maxUnion();

    static std::shared_ptr<Implicit::Operator::BinaryFloat> largeBlending();

    static std::shared_ptr<Implicit::Operator::BinaryFloat> oneSidedFullStretching();

    static std::shared_ptr<Implicit::Operator::BinaryFloat> oneSidedFullBulge();

    static std::shared_ptr<Implicit::Operator::BinaryFloat> oneSidedFullStretchingAgainstPreserve();

    static std::shared_ptr<Implicit::Operator::BinaryFloat> preserve();

    static std::shared_ptr<Implicit::Operator::BinaryFloat> oneSidedContact();

    static std::shared_ptr<Implicit::Operator::BinaryFloat> plus();

    static std::shared_ptr<Implicit::Operator::BinaryFloat> testDome(float pos = -0.5f, float size = 0.25f);

    static std::shared_ptr<Implicit::Operator::BinaryFloat> swap(
            const std::shared_ptr<Implicit::Operator::BinaryFloat>& a_op);

    static std::shared_ptr<Implicit::Operator::BinaryFloat> intersection(
            const std::shared_ptr<Implicit::Operator::BinaryFloat>& a_op);

    static std::shared_ptr<Implicit::Operator::BinaryFloat> difference(
            const std::shared_ptr<Implicit::Operator::BinaryFloat>& a_op);

    static std::shared_ptr<Implicit::Operator::BinaryFloat> join(
            const std::shared_ptr<Implicit::Operator::BinaryFloat>& a_op,
            const std::shared_ptr<Implicit::Operator::BinaryFloat>& a_first,
            const std::shared_ptr<Implicit::Operator::BinaryFloat>& a_second);

    static std::shared_ptr<Implicit::Operator::BinaryFloat> fullStretching();

    static std::shared_ptr<Implicit::Operator::BinaryFloat> fullBulge();

    static std::shared_ptr<Implicit::Operator::BinaryFloat> testDomeJoin(
            const std::shared_ptr<Implicit::Operator::BinaryFloat>& a_op,
            float pos1 = 0.35f, float size1 = 0.2f, float pos2 = 0.35f, float size2 = 0.2f);

    static std::shared_ptr<Implicit::Operator::BinaryFloat> fullStretchingVsPreserve();

    static std::shared_ptr<Implicit::Operator::BinaryFloat> contact();

    static bool byName(const std::string& name, std::shared_ptr<Implicit::Operator::BinaryFloat>& res);

    static std::vector<std::string> getNames();
};

}
}
}
