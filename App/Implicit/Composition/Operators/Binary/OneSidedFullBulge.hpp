// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <cmath>

#include <Implicit/Composition/Operators/Binary/DistanceFieldParts.hpp>

namespace Implicit {
namespace Operator {
namespace Binary {

class OneSidedFullBulge : public Implicit::Operator::Binary::DistanceFieldParts {
public:
    OneSidedFullBulge() {
        registerParts();
    }

    virtual void registerParts() {
        registerRay(0.5f * Eigen::Vector2f::Ones(), Eigen::Vector2f::Ones());

        Eigen::Vector2f C = (1.f / 4.f) * Eigen::Vector2f(1, 3);
        float rC = 1.f / (std::sqrt(2.f) * 2.f);
        registerArc(C, rC, Eigen::Vector2f::UnitX(), -Eigen::Vector2f::Ones());

        Eigen::Vector2f A = 0.5f * Eigen::Vector2f::UnitY();
        Eigen::Vector2f B = C - rC * Eigen::Vector2f::UnitY();
        Eigen::Vector2f rotAB = Eigen::Vector2f(-(B[1] - A[1]), (B[0] - A[0]));
        Eigen::Vector2f center3AB = 0.25f * (3.f * A + B);
        Eigen::Vector2f center3BA = 0.25f * (3.f * B + A);
        float t = -center3AB[0] / rotAB[0];
        Eigen::Vector2f botC = center3AB + t * rotAB;
        Eigen::Vector2f topC = center3BA - t * rotAB;
        float rS = (0.5f - botC[1]);
        Eigen::Vector2f splitN(-(topC[1] - botC[1]), (topC[0] - botC[0]));

        registerArc(topC, rS, -splitN, -Eigen::Vector2f::UnitX());
        registerArc(botC, -rS, Eigen::Vector2f::UnitX(), splitN);

        registerRay(0.5f * Eigen::Vector2f::UnitY(), -Eigen::Vector2f::UnitX(), false);
    }

    virtual std::string getName() const override {
        return std::string("BinaryFloatOperatorOneSidedFullBulge");
    }
};

}
}
}
