// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <memory>

#include <Implicit/Composition/Types/BinaryFloat.hpp>

namespace Implicit {
namespace Operator {
namespace Binary {

class Swap : public Implicit::Operator::BinaryFloat {
public:
    Swap(const std::shared_ptr<Implicit::Operator::BinaryFloat>& a_op) :
        op(a_op) {
        registerChild(op);
    }

    virtual bool evalFloat(float f1, float f2, float& value) const {
        return op->evalFloat(f2, f1, value);
    }

    virtual bool evalGradient(float f1, float f2, Eigen::Vector2f& gradient) const {
        if(! op->evalGradient(f2, f1, gradient)) {
            return false;
        }
        gradient = Eigen::Vector2f(gradient[1], gradient[0]);
        return true;
    }

    virtual bool evalFieldValue(float f1, float f2, float& value, Eigen::Vector2f& gradient) const {
        if(! op->evalFieldValue(f2, f1, value, gradient)) {
            return false;
        }
        gradient = Eigen::Vector2f(gradient[1], gradient[0]);
        return true;
    }

    virtual std::string getName() const override {
        return std::string("BinaryFloatOperatorSwap");
    }
private:
    std::shared_ptr<Implicit::Operator::BinaryFloat> op;
};

}
}
}
