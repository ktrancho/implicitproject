// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#include "LargeBlending.hpp"

#include <cmath>

namespace Implicit {
namespace Operator {
namespace Binary {

bool LargeBlending::evalFloat(float f1, float f2, float &value) const {
    if(f1 < 0 || f2 < 0) {
        value = std::max(f1, f2);
        return true;
    }
    value = Eigen::Vector2f(f1, f2).norm();
    return true;
}

bool LargeBlending::evalGradient(float f1, float f2, Eigen::Vector2f &gradient) const {
    if(f1 < 0 || f2 < 0) {
        if(f1 > f2) {
            gradient = Eigen::Vector2f::UnitX();
        } else {
            gradient = Eigen::Vector2f::UnitY();
        }
        return true;
    }
    gradient = Eigen::Vector2f(f1, f2);
    gradient.normalize();
    return true;
}

}
}
}