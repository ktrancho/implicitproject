// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#include "MultiParts.hpp"

namespace Implicit {
namespace Operator {
namespace Binary {

bool MultiParts::evalFieldValue(float f1, float f2, float& value, Eigen::Vector2f& gradient) const {
    float currentIsoDistance = -1;

    float workingIsoDistance = -1;
    float workingField;
    Eigen::Vector2f workingGradient;

    for(auto f : parts) {
        if(! f(f1, f2, workingField, workingGradient)) {
            continue;
        }
        workingIsoDistance = std::abs(workingField - 0.5f);
        if((currentIsoDistance < 0) || (workingIsoDistance < currentIsoDistance)) {
            currentIsoDistance = workingIsoDistance;
            value = workingField;
            gradient = workingGradient;
        }
    }

    return currentIsoDistance >= 0;
}

}
}
}
