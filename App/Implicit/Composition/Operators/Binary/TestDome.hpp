// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <cmath>

#include <Implicit/Composition/Operators/Binary/DistanceFieldParts.hpp>

namespace Implicit {
namespace Operator {
namespace Binary {

class TestDome : public Implicit::Operator::Binary::DistanceFieldParts {
public:
    TestDome(float pos = 0, float size = 0.5) {
        registerParts(pos, size);
    }

    virtual void registerParts(float pos, float size) {
        registerRay((pos - size) * Eigen::Vector2f::UnitX() + 0.5f * Eigen::Vector2f::UnitY(), -Eigen::Vector2f::UnitX(), false);
        registerArc((pos - size) * Eigen::Vector2f::UnitX() + (0.5f - 0.5f * size) * Eigen::Vector2f::UnitY(), -0.5f * size,
                Eigen::Vector2f::UnitX(), Eigen::Vector2f::UnitY());
        registerArc((pos) * Eigen::Vector2f::UnitX() + (0.5f - 0.5f * size) * Eigen::Vector2f::UnitY(), 0.5f * size,
                    -Eigen::Vector2f::UnitY(), -Eigen::Vector2f::UnitY());
        registerArc((pos + size) * Eigen::Vector2f::UnitX() + (0.5f - 0.5f * size) * Eigen::Vector2f::UnitY(), -0.5f * size,
                    -Eigen::Vector2f::UnitX(), Eigen::Vector2f::UnitY());
        registerRay((pos + size) * Eigen::Vector2f::UnitX() + 0.5f * Eigen::Vector2f::UnitY(), Eigen::Vector2f::UnitX());
    }

    virtual std::string getName() const override {
        return std::string("BinaryFloatOperatorOneSidedFullStretching");
    }
};

}
}
}
