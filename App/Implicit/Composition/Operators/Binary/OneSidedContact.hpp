// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <cmath>

#include <Implicit/Composition/Operators/Binary/DistanceFieldParts.hpp>

namespace Implicit {
namespace Operator {
namespace Binary {

class OneSidedContact : public Implicit::Operator::Binary::DistanceFieldParts {
public:
    OneSidedContact() {
        registerParts();
    }

    virtual void registerParts() {
        registerRay(0.5f * Eigen::Vector2f::Ones(), -Eigen::Vector2f::UnitX(), false);
        registerArc(0.5f * Eigen::Vector2f::Ones(), 1e-9, Eigen::Vector2f::UnitX(), -Eigen::Vector2f::Ones());
        registerRay(0.5f * Eigen::Vector2f::Ones(), Eigen::Vector2f::Ones());
    }

    virtual std::string getName() const override {
        return std::string("BinaryFloatOperatorOneSidedContact");
    }
};

}
}
}
