// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <Eigen/Dense>
#include <Implicit/Composition/Operators/Binary/MultiParts.hpp>

namespace Implicit {
namespace Operator {
namespace Binary {

class DistanceFieldParts : public Implicit::Operator::Binary::MultiParts {
protected:
    DistanceFieldParts() {}

    /// Assume normal towards positive field is (sign)*(first, second).rotate(PI / 2).
    void registerLine(const Eigen::Vector2f& a_first, const Eigen::Vector2f& a_second, float positiveSign = true) {
        float sign = (positiveSign) ? 1.f : -1.f;
        Eigen::Vector2f first = a_first;
        Eigen::Vector2f second = a_second;

        parts.push_back([=](float f1, float f2, float& value, Eigen::Vector2f& gradient) -> bool {

            Eigen::Vector2f pos(f1, f2);
            if(((pos - first).dot(second - first) < 0)
            || ((pos - second).dot(first - second) < 0)) { // outside line range
                return false;
            }
            gradient = sign * Eigen::Vector2f(-(second[1] - first[1]), (second[0] - first[0]));
            gradient.normalize();

            value = 0.5f + (pos - first).dot(gradient);
            return true;
        });
    }

    /// Assume normal towards positive field is (sign)*(direction).rotate(PI / 2).
    void registerRay(const Eigen::Vector2f& a_first, const Eigen::Vector2f& a_direction, float positiveSign = true) {
        float sign = (positiveSign) ? 1.f : -1.f;
        Eigen::Vector2f first = a_first;
        Eigen::Vector2f direction = a_direction;

        parts.push_back([=](float f1, float f2, float& value, Eigen::Vector2f& gradient) -> bool {

            Eigen::Vector2f pos(f1, f2);
            if((pos - first).dot(direction) < 0) { // before ray
                return false;
            }
            gradient = sign * Eigen::Vector2f(-direction[1], direction[0]);
            gradient.normalize();

            value = 0.5f + (pos - first).dot(gradient);
            return true;
        });
    }

    /// Positive radius create bulge and negative a crease.
    void registerArc(const Eigen::Vector2f& a_center, float radius, const Eigen::Vector2f& a_firstNormal,
            const Eigen::Vector2f& a_secondNormal) {
        Eigen::Vector2f center = a_center;
        Eigen::Vector2f firstNormal = a_firstNormal;
        Eigen::Vector2f secondNormal = a_secondNormal;

        parts.push_back([=](float f1, float f2, float& value, Eigen::Vector2f& gradient) -> bool {
            Eigen::Vector2f pos(f1, f2);
            if(((pos - center).dot(firstNormal) < 0)
            || ((pos - center).dot(secondNormal) < 0)) {
                return false;
            }
            if(radius > 0) {
                gradient = (center - pos);
                value = 0.5f - (gradient.norm() - radius);
            } else {
                gradient = (pos - center);
                value = 0.5f + (gradient.norm() + radius);
            }
            gradient.normalize();
            return true;
        });
    }

public:
    virtual std::string getName() const override {
        return std::string("BinaryFloatOperatorDistanceFieldParts");
    }
};

}
}
}
