// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <cmath>

#include <Implicit/Composition/Operators/Binary/DistanceFieldParts.hpp>

namespace Implicit {
namespace Operator {
namespace Binary {

class OneSidedFullStretchingAgainstPreserve : public Implicit::Operator::Binary::DistanceFieldParts {
public:
    OneSidedFullStretchingAgainstPreserve() {
        registerParts();
    }

    virtual void registerParts() {
        registerRay(0.5f * Eigen::Vector2f::UnitY(), -Eigen::Vector2f::UnitX(), false);
        registerArc(Eigen::Vector2f::UnitY(), 0.5f, Eigen::Vector2f::UnitX(), -Eigen::Vector2f::UnitY());
        registerRay(Eigen::Vector2f::UnitY() + 0.5f * Eigen::Vector2f::UnitX(), Eigen::Vector2f::UnitY());
    }

    virtual std::string getName() const override {
        return std::string("BinaryFloatOperatorOneSidedFullStretchingAgainstPreserve");
    }
};

}
}
}
