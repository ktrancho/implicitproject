// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <memory>

#include <Implicit/Composition/Types/BinaryFloat.hpp>

namespace Implicit {
namespace Operator {
namespace Binary {

class Join : public Implicit::Operator::BinaryFloat {
public:
    Join(const std::shared_ptr<Implicit::Operator::BinaryFloat>& a_op,
            const std::shared_ptr<Implicit::Operator::BinaryFloat>& a_first,
            const std::shared_ptr<Implicit::Operator::BinaryFloat>& a_second) :
            op(a_op),
            first(a_first),
            second(a_second) {
        registerChild(op);
        registerChild(first);
        registerChild(second);
    }

    virtual bool evalFloat(float f1, float f2, float& value) const override {
        Eigen::Vector2f _;
        return evalFieldValue(f1, f2, value, _);
    }

    virtual bool evalGradient(float f1, float f2, Eigen::Vector2f& gradient) const override {
        float _;
        return evalFieldValue(f1, f2, _, gradient);
    }

    virtual bool evalFieldValue(float f1, float f2, float& value, Eigen::Vector2f& gradient) const override {
        float firstValue;
        float secondValue;
        Eigen::Vector2f firstGradient;
        Eigen::Vector2f secondGradient;

        if(! first->evalFieldValue(f1, f2, firstValue, firstGradient)) {
            return false;
        }
        if(! second->evalFieldValue(f1, f2, secondValue, secondGradient)) {
            return false;
        }
        if(! op->evalFieldValue(firstValue, secondValue, value, gradient)) {
            return false;
        }
        gradient = gradient[0] * firstGradient + gradient[1] * secondGradient;
        return true;
    }

    virtual std::string getName() const override {
        return std::string("BinaryFloatOperatorJoin");
    }

private:
    std::shared_ptr<Implicit::Operator::BinaryFloat> op;
    std::shared_ptr<Implicit::Operator::BinaryFloat> first;
    std::shared_ptr<Implicit::Operator::BinaryFloat> second;
};

}
}
}



