// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <Eigen/Dense>
#include <Implicit/Composition/Operators/BinaryNParameters/MultiParts.hpp>

namespace Implicit {
namespace Operator {
namespace Binary {
namespace NParameters {

class DistanceFieldParts : public Implicit::Operator::Binary::NParameters::MultiParts {
protected:
    DistanceFieldParts() {}

    /// Assume normal towards positive field is (sign)*(first, second).rotate(PI / 2).
    void registerLine(
            std::function<Eigen::Vector2f (const std::vector<float>& params)> first_generator,
            std::function<Eigen::Vector2f (const std::vector<float>& params)> second_generator,
            std::function<bool (const std::vector<float>& params)> positiveSign_generator = [](const std::vector<float>& params){return true;},
            std::function<bool (const std::vector<float>& params)> enable_generator = [](const std::vector<float>& params){return true;}
            ) {

        parts.push_back([=](float f1, float f2, const std::vector<float>& params, float &value, Eigen::Vector2f &gradient) -> bool {
            if(! enable_generator(params)) {
                return false;
            }

            float sign = (positiveSign_generator(params)) ? 1.f : -1.f;
            Eigen::Vector2f first = first_generator(params);
            Eigen::Vector2f second = second_generator(params);

            Eigen::Vector2f pos(f1, f2);
            if (((pos - first).dot(second - first) < 0)
                || ((pos - second).dot(first - second) < 0)) { // outside line range
                return false;
            }
            gradient = sign * Eigen::Vector2f(-(second[1] - first[1]), (second[0] - first[0]));
            gradient.normalize();

            value = 0.5f + (pos - first).dot(gradient);
            return true;
        });
    }

    void registerLineBulk(
            std::function<void (
                    const std::vector<float>& params,
                    Eigen::Vector2f& first,
                    Eigen::Vector2f& second,
                    bool& positiveSign,
                    bool& enable)> generator
            ) {

        parts.push_back([=](float f1, float f2, const std::vector<float>& params, float &value, Eigen::Vector2f &gradient) -> bool {
            Eigen::Vector2f first;
            Eigen::Vector2f second;
            bool positiveSign;
            bool enable;

            generator(params, first, second, positiveSign, enable);

            if(! enable) {
                return false;
            }

            float sign = (positiveSign) ? 1.f : -1.f;

            Eigen::Vector2f pos(f1, f2);
            if (((pos - first).dot(second - first) < 0)
                || ((pos - second).dot(first - second) < 0)) { // outside line range
                return false;
            }
            gradient = sign * Eigen::Vector2f(-(second[1] - first[1]), (second[0] - first[0]));
            gradient.normalize();

            value = 0.5f + (pos - first).dot(gradient);
            return true;
        });
    }

    void registerLineContactBulk(
            std::function<void (
                    const std::vector<float>& params,
                    Eigen::Vector2f& first,
                    Eigen::Vector2f& second,
                    bool& enable)> generator
    ) {

        parts.push_back([=](float f1, float f2, const std::vector<float>& params, float &value, Eigen::Vector2f &gradient) -> bool {
            Eigen::Vector2f first;
            Eigen::Vector2f second;
            bool enable;

            generator(params, first, second, enable);

            if(! enable) {
                return false;
            }

            Eigen::Vector2f pos(f1, f2);
            if (((pos - first).dot(second - first) < 0)
                || ((pos - second).dot(first - second) < 0)) { // outside line range
                return false;
            }
            gradient = Eigen::Vector2f(-(second[1] - first[1]), (second[0] - first[0]));
            gradient.normalize();

            value = (pos - first).dot(gradient);
            if(value < 0) {
                gradient *= -1;
                value *= -1;
            }
            value += 0.5f;
            return true;
        });
    }

    /// Assume normal towards positive field is (sign)*(direction).rotate(PI / 2).
    void registerRay(
            std::function<Eigen::Vector2f (const std::vector<float>& params)> first_generator,
            std::function<Eigen::Vector2f (const std::vector<float>& params)> direction_generator,
            std::function<bool (const std::vector<float>& params)> positiveSign_generator = [](const std::vector<float>& params){return true;},
            std::function<bool (const std::vector<float>& params)> enable_generator = [](const std::vector<float>& params){return true;}
            ) {

        parts.push_back([=](float f1, float f2, const std::vector<float>& params, float &value, Eigen::Vector2f &gradient) -> bool {

            if(! enable_generator(params)) {
                return false;
            }

            float sign = (positiveSign_generator(params)) ? 1.f : -1.f;
            Eigen::Vector2f first = first_generator(params);
            Eigen::Vector2f direction = direction_generator(params);

            Eigen::Vector2f pos(f1, f2);
            if ((pos - first).dot(direction) < 0) { // before ray
                return false;
            }
            gradient = sign * Eigen::Vector2f(-direction[1], direction[0]);
            gradient.normalize();

            value = 0.5f + (pos - first).dot(gradient);
            return true;
        });
    }

    void registerRayBulk(
            std::function<void (
                    const std::vector<float>& params,
                    Eigen::Vector2f& first,
                    Eigen::Vector2f& direction,
                    bool& positiveSign,
                    bool& enable)> generator
            ) {

        parts.push_back([=](float f1, float f2, const std::vector<float>& params, float &value, Eigen::Vector2f &gradient) -> bool {

            Eigen::Vector2f first;
            Eigen::Vector2f direction;
            bool positiveSign;
            bool enable;

            generator(params, first, direction, positiveSign, enable);

            if(! enable) {
                return false;
            }

            float sign = (positiveSign) ? 1.f : -1.f;

            Eigen::Vector2f pos(f1, f2);
            if ((pos - first).dot(direction) < 0) { // before ray
                return false;
            }
            gradient = sign * Eigen::Vector2f(-direction[1], direction[0]);
            gradient.normalize();

            value = 0.5f + (pos - first).dot(gradient);
            return true;
        });
    }

    void registerRayContactBulk(
            std::function<void (
                    const std::vector<float>& params,
                    Eigen::Vector2f& first,
                    Eigen::Vector2f& direction,
                    bool& enable)> generator
    ) {

        parts.push_back([=](float f1, float f2, const std::vector<float>& params, float &value, Eigen::Vector2f &gradient) -> bool {

            Eigen::Vector2f first;
            Eigen::Vector2f direction;
            bool enable;

            generator(params, first, direction, enable);

            if(! enable) {
                return false;
            }

            Eigen::Vector2f pos(f1, f2);
            if ((pos - first).dot(direction) < 0) { // before ray
                return false;
            }
            gradient = Eigen::Vector2f(-direction[1], direction[0]);
            gradient.normalize();

            value = (pos - first).dot(gradient);
            if(value < 0) {
                gradient *= -1;
                value *= -1;
            }
            value += 0.5f;
            return true;
        });
    }

    /// Positive radius create bulge and negative a crease.
    void registerArc(
            std::function<Eigen::Vector2f (const std::vector<float>& params)> center_generator,
            std::function<float (const std::vector<float>& params)> radius_generator,
            std::function<Eigen::Vector2f (const std::vector<float>& params)> firstNormal_generator,
            std::function<Eigen::Vector2f (const std::vector<float>& params)> secondNormal_generator,
            std::function<bool (const std::vector<float>& params)> enable_generator = [](const std::vector<float>& params){return true;}
            ) {


        parts.push_back([=](float f1, float f2, const std::vector<float>& params, float &value, Eigen::Vector2f &gradient) -> bool {

            if(! enable_generator(params)) {
                return false;
            }

            float radius = radius_generator(params);
            Eigen::Vector2f center = center_generator(params);
            Eigen::Vector2f firstNormal = firstNormal_generator(params);
            Eigen::Vector2f secondNormal = secondNormal_generator(params);

            Eigen::Vector2f pos(f1, f2);
            if (((pos - center).dot(firstNormal) < 0)
                || ((pos - center).dot(secondNormal) < 0)) {
                return false;
            }
            if (radius > 0) {
                gradient = (center - pos);
                value = 0.5f - (gradient.norm() - radius);
            } else {
                gradient = (pos - center);
                value = 0.5f + (gradient.norm() + radius);
            }
            gradient.normalize();
            return true;
        });
    }

    void registerArcBulk(
            std::function<void (
                    const std::vector<float>& params,
                    Eigen::Vector2f& center,
                    float& radius,
                    Eigen::Vector2f& firstNormal,
                    Eigen::Vector2f& secondNormal,
                    bool& enable)> generator
            ) {

        parts.push_back([=](float f1, float f2, const std::vector<float>& params, float &value, Eigen::Vector2f &gradient) -> bool {

            Eigen::Vector2f center;
            float radius;
            Eigen::Vector2f firstNormal;
            Eigen::Vector2f secondNormal;
            bool enable;

            generator(params, center, radius, firstNormal, secondNormal, enable);

            if(! enable) {
                return false;
            }

            Eigen::Vector2f pos(f1, f2);
            if (((pos - center).dot(firstNormal) < 0)
                || ((pos - center).dot(secondNormal) < 0)) {
                return false;
            }
            if (radius > 0) {
                gradient = (center - pos);
                value = 0.5f - (gradient.norm() - radius);
            } else {
                gradient = (pos - center);
                value = 0.5f + (gradient.norm() + radius);
            }
            gradient.normalize();
            return true;
        });
    }

public:
    virtual std::string getName() const override {
        return std::string("BinaryFloatNParametersOperatorDistanceFieldParts");
    }
};

}
}
}
}
