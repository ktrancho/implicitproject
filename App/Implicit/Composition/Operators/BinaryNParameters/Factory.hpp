// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <memory>
#include <string>
#include <vector>

#include <Implicit/Composition/Types/BinaryFloatNParameters.hpp>

namespace Implicit {
namespace Operator {
namespace Binary {
namespace NParameters {

class Factory {
public:
    static std::shared_ptr<Implicit::Operator::NParameters::BinaryFloat> blending();

    static std::shared_ptr<Implicit::Operator::NParameters::BinaryFloat> blendingByParts();

    static std::shared_ptr<Implicit::Operator::NParameters::BinaryFloat> pointSculpt();

    static std::shared_ptr<Implicit::Operator::NParameters::BinaryFloat> pointSculptLine();

    static std::shared_ptr<Implicit::Operator::NParameters::BinaryFloat> wallContact();

    static bool byName(const std::string &name, std::shared_ptr<Implicit::Operator::NParameters::BinaryFloat> &res);

    static std::vector<std::string> getNames();
};

}
}
}
}
