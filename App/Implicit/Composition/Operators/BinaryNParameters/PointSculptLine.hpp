// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#include <Implicit/Composition/Operators/BinaryNParameters/DistanceFieldParts.hpp>

namespace Implicit {
namespace Operator {
namespace Binary {
namespace NParameters {

class PointSculptLine : public Implicit::Operator::Binary::NParameters::DistanceFieldParts {
public:
    PointSculptLine() {
        registerParts();
    }

    virtual void registerParts() {

        registerRayBulk([&](
                const std::vector<float>& params,
                Eigen::Vector2f& first,
                Eigen::Vector2f& direction,
                bool& positiveSign,
                bool& enable
        ) {

            first = 0.5f * Eigen::Vector2f::Ones() - 0.5f * getHeight(params) * Eigen::Vector2f::UnitX();
            direction = Eigen::Vector2f::UnitY();
            positiveSign = false;
            enable = true;
        });

        registerArcBulk([&](
                const std::vector<float>& params,
                Eigen::Vector2f& center,
                float& radius,
                Eigen::Vector2f& firstNormal,
                Eigen::Vector2f& secondNormal,
                bool& enable
        ) {

            center = 0.5f * Eigen::Vector2f::Ones() - 0.5f * getHeight(params) * Eigen::Vector2f::UnitX();
            radius = 1e-6;
            firstNormal = (0.5f * Eigen::Vector2f::Ones() - 0.5f * getHeight(params) * Eigen::Vector2f::UnitX() - 0.5f * Eigen::Vector2f::UnitX());
            secondNormal = -Eigen::Vector2f::UnitY();
            enable = true;
        });

        registerLineBulk([&](
                const std::vector<float>& params,
                Eigen::Vector2f& first,
                Eigen::Vector2f& second,
                bool& positiveSign,
                bool& enable
        ) {

            first = 0.5f * Eigen::Vector2f::Ones() - 0.5f * getHeight(params) * Eigen::Vector2f::UnitX();
            second = 0.5f * Eigen::Vector2f::UnitX();
            positiveSign = true;
            enable = true;
        });

        registerArcBulk([&](
                const std::vector<float>& params,
                Eigen::Vector2f& center,
                float& radius,
                Eigen::Vector2f& firstNormal,
                Eigen::Vector2f& secondNormal,
                bool& enable
        ) {

            center = 0.5f * Eigen::Vector2f::UnitX();
            radius = -1e-6;
            firstNormal = -(0.5f * Eigen::Vector2f::Ones() - 0.5f * getHeight(params) * Eigen::Vector2f::UnitX() - 0.5f * Eigen::Vector2f::UnitX());
            secondNormal = Eigen::Vector2f::UnitY();
            enable = true;
        });

        registerRayBulk([&](
                const std::vector<float>& params,
                Eigen::Vector2f& first,
                Eigen::Vector2f& direction,
                bool& positiveSign,
                bool& enable
        ) {

            first = 0.5f * Eigen::Vector2f::UnitX();
            direction = -Eigen::Vector2f::UnitY();
            positiveSign = true;
            enable = true;
        });
    }

    virtual std::string getName() const override {
        return std::string("BinaryFloatNParametersOperatorPointSculptLine");
    }

private:

    float getHeight(const std::vector<float>& params) {
        return (params.empty() ? 1.f : params[0]);
    }
};

}
}
}
}
