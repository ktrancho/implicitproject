// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#include "Factory.hpp"

#include <Implicit/Composition/Operators/BinaryNParameters/Blending.hpp>
#include <Implicit/Composition/Operators/BinaryNParameters/BlendingByParts.hpp>
#include <Implicit/Composition/Operators/BinaryNParameters/PointSculpt.hpp>
#include <Implicit/Composition/Operators/BinaryNParameters/PointSculptLine.hpp>
#include <Implicit/Composition/Operators/BinaryNParameters/WallContact.hpp>

namespace Implicit {
namespace Operator {
namespace Binary {
namespace NParameters {

std::shared_ptr<Implicit::Operator::NParameters::BinaryFloat> Factory::blending() {
    std::shared_ptr<Implicit::Operator::Binary::NParameters::Blending> op(new Implicit::Operator::Binary::NParameters::Blending());
    return op;
}

std::shared_ptr<Implicit::Operator::NParameters::BinaryFloat> Factory::blendingByParts() {
    std::shared_ptr<Implicit::Operator::Binary::NParameters::BlendingByParts> op(new Implicit::Operator::Binary::NParameters::BlendingByParts());
    return op;
}

std::shared_ptr<Implicit::Operator::NParameters::BinaryFloat> Factory::pointSculpt() {
    std::shared_ptr<Implicit::Operator::Binary::NParameters::PointSculpt> op(new Implicit::Operator::Binary::NParameters::PointSculpt());
    return op;
}

std::shared_ptr<Implicit::Operator::NParameters::BinaryFloat> Factory::pointSculptLine() {
    std::shared_ptr<Implicit::Operator::Binary::NParameters::PointSculptLine> op(new Implicit::Operator::Binary::NParameters::PointSculptLine());
    return op;
}

std::shared_ptr<Implicit::Operator::NParameters::BinaryFloat> Factory::wallContact() {
    std::shared_ptr<Implicit::Operator::Binary::NParameters::WallContact> op(new Implicit::Operator::Binary::NParameters::WallContact());
    return op;
}

bool Factory::byName(const std::string &name, std::shared_ptr<Implicit::Operator::NParameters::BinaryFloat> &res) {
    if (name == "Blending") {
        res = blending();
        return true;
    } else if (name == "BlendingByParts") {
        res = blending();
        return true;
    } else if (name == "PointSculpt") {
        res = blending();
        return true;
    } else if (name == "PointSculptLine") {
        res = blending();
        return true;
    }
    return false;
}

std::vector<std::string> Factory::getNames() {
    std::vector<std::string> names;
    names.push_back("Blending");
    names.push_back("BlendingByParts");
    names.push_back("PointSculpt");
    names.push_back("PointSculptLine");
    return names;
}

}
}
}
}
