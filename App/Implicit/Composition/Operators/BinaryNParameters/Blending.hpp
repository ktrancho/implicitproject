// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.


#include <Implicit/Composition/Types/BinaryFloatNParameters.hpp>

namespace Implicit {
namespace Operator {
namespace Binary {
namespace NParameters {

class Blending : public Implicit::Operator::NParameters::BinaryFloat {
public:
    Blending() {}

    virtual bool evalFloat(float f1, float f2, const std::vector<float>& params, float &value) const override {
        float p = ajustParameter(params.empty() ? 0.f : params[0]);
        if(f1 < p || f2 < p) {
            value = std::max(f1, f2);
            return true;
        }
        value = p + Eigen::Vector2f(f1 - p, f2 - p).norm();
        return true;
    }

    virtual bool evalGradient(float f1, float f2, const std::vector<float>& params, Eigen::Vector2f &gradient) const override {
        float p = ajustParameter(params.empty() ? 0.f : params[0]);
        if(f1 < p || f2 < p) {
            if(f1 > f2) {
                gradient = Eigen::Vector2f::UnitX();
            } else {
                gradient = Eigen::Vector2f::UnitY();
            }
        }
        gradient = Eigen::Vector2f(f1 - p, f2 - p);
        gradient.normalize();
        return true;
    }

    virtual std::string getName() const override {
        return std::string("BinaryFloatOperatorNParametersBlending");
    }

private:
    float ajustParameter(float p) const {
        return (1 - p) / 2;
    }
};

}
}
}
}
