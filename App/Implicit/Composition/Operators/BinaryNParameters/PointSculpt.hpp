// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#include <Implicit/Composition/Operators/BinaryNParameters/DistanceFieldParts.hpp>

namespace Implicit {
namespace Operator {
namespace Binary {
namespace NParameters {

class PointSculpt : public Implicit::Operator::Binary::NParameters::DistanceFieldParts {
public:
    PointSculpt() {
        registerParts();
    }

    virtual void registerParts() {

        registerRayBulk([&](
                const std::vector<float>& params,
                Eigen::Vector2f& first,
                Eigen::Vector2f& direction,
                bool& positiveSign,
                bool& enable
                ) {

            first = 0.5f * Eigen::Vector2f::Ones() - 0.5f * getHeight(params) * Eigen::Vector2f::UnitX();
            direction = Eigen::Vector2f::UnitY();
            positiveSign = false;
            enable = true;
        });

        registerArcBulk([&](
                const std::vector<float>& params,
                Eigen::Vector2f& center,
                float& radius,
                Eigen::Vector2f& firstNormal,
                Eigen::Vector2f& secondNormal,
                bool& enable
                ) {
            Eigen::Vector2f roundnessControl;
            float roundnessRadius;
            Eigen::Vector2f sharpControl;
            float sharpRadius;
            Eigen::Vector2f topBotNormal;

            computeControls(params, roundnessControl, roundnessRadius, sharpControl, sharpRadius, topBotNormal);

            center = roundnessControl;
            radius = roundnessRadius;
            firstNormal = -Eigen::Vector2f::UnitY();
            secondNormal = -topBotNormal;
            enable = true;
        });

        registerArcBulk([&](
                const std::vector<float>& params,
                Eigen::Vector2f& center,
                float& radius,
                Eigen::Vector2f& firstNormal,
                Eigen::Vector2f& secondNormal,
                bool& enable
        ) {
            Eigen::Vector2f roundnessControl;
            float roundnessRadius;
            Eigen::Vector2f sharpControl;
            float sharpRadius;
            Eigen::Vector2f topBotNormal;

            computeControls(params, roundnessControl, roundnessRadius, sharpControl, sharpRadius, topBotNormal);

            center = sharpControl;
            radius = -sharpRadius;
            firstNormal = Eigen::Vector2f::UnitY();
            secondNormal = topBotNormal;
            enable = true;
        });

        registerRayBulk([&](
                const std::vector<float>& params,
                Eigen::Vector2f& first,
                Eigen::Vector2f& direction,
                bool& positiveSign,
                bool& enable
        ) {

            first = 0.5f * Eigen::Vector2f::Ones() - 0.5 * getScale(params) * Eigen::Vector2f::UnitY();
            direction = -Eigen::Vector2f::UnitY();
            positiveSign = true;
            enable = true;
        });
    }

    virtual std::string getName() const override {
        return std::string("BinaryFloatNParametersOperatorPointSculpt");
    }

private:

    float getHeight(const std::vector<float>& params) {
        return (params.empty() ? 1.f : params[0]);
    }

    float getScale(const std::vector<float>& params) {
        return 1.f;
        //return ((params.size() < 2) ? 1.f : params[1]);
    }

    float getRoundness(const std::vector<float>& params) {
        return ((params.size() < 2) ? 0.f : params[1]);
    }

    void computeControls(
            const std::vector<float>& params,
            Eigen::Vector2f& roundnessControl,
            float& roundnessRadius,
            Eigen::Vector2f& sharpControl,
            float& sharpRadius,
            Eigen::Vector2f& topBotNormal) {
        float height = getHeight(params) * 0.5f;
        float scale = getScale(params) * 0.5f;
        float roundness = getRoundness(params);

        Eigen::Vector2f top = 0.5 * Eigen::Vector2f::Ones() - height * Eigen::Vector2f::UnitX();
        Eigen::Vector2f bot = 0.5 * Eigen::Vector2f::Ones() - scale * Eigen::Vector2f::UnitY();

        Eigen::Vector2f mid = (1 - roundness) * top + roundness * bot;
        Eigen::Vector2f normal = Eigen::Vector2f(top[1] - bot[1], bot[0] - top[0]);
        normal.normalize();
        Eigen::Vector2f topMedPos = (top + mid) / 2;
        Eigen::Vector2f botMedPos = (bot + mid) / 2;

        float topOffset = (0.5f - topMedPos[1]) / normal[1];
        roundnessControl = topMedPos + topOffset * normal;

        float botOffset = (0.f - botMedPos[1]) / normal[1];
        sharpControl = botMedPos + botOffset * normal;

        roundnessRadius = std::abs(roundnessControl[0] - top[0]) + 1e-6;
        sharpRadius = std::abs(sharpControl[0] - bot[0]) + 1e-6;

        topBotNormal = Eigen::Vector2f(roundnessControl[1] - sharpControl[1], sharpControl[0] - roundnessControl[0]);
    }
};

}
}
}
}
