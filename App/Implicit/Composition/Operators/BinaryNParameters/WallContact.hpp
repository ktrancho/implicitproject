// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#include <Implicit/Composition/Operators/BinaryNParameters/DistanceFieldParts.hpp>

namespace Implicit {
namespace Operator {
namespace Binary {
namespace NParameters {

class WallContact : public Implicit::Operator::Binary::NParameters::DistanceFieldParts {
public:
    WallContact() {
        registerParts();
    }

    virtual void registerParts() {

        registerRayBulk([&](
                const std::vector<float>& params,
                Eigen::Vector2f& first,
                Eigen::Vector2f& direction,
                bool& positiveSign,
                bool& enable
        ) {

            first = 0.5f * Eigen::Vector2f::UnitY() - getOffset(params) * Eigen::Vector2f::UnitX();
            direction = -Eigen::Vector2f::UnitX();
            positiveSign = false;
            enable = true;
        });

        registerArcBulk([&](
                const std::vector<float>& params,
                Eigen::Vector2f& center,
                float& radius,
                Eigen::Vector2f& firstNormal,
                Eigen::Vector2f& secondNormal,
                bool& enable
        ) {

            center = 0.5f * Eigen::Vector2f::Ones()
                    + 0.5f * (getStretch(params) - getBulge(params)) * Eigen::Vector2f::UnitY()
                    - 0.5f * getStretch(params) * Eigen::Vector2f::UnitX();
            radius = 0.5f * getStretch(params);
            firstNormal = Eigen::Vector2f::UnitX();
            secondNormal = -Eigen::Vector2f::UnitY();
            enable = true;
        });

        registerRayBulk([&](
                const std::vector<float>& params,
                Eigen::Vector2f& first,
                Eigen::Vector2f& direction,
                bool& positiveSign,
                bool& enable
        ) {

            first = 0.5f * Eigen::Vector2f::Ones() + 0.5f * (getStretch(params) - getBulge(params)) * Eigen::Vector2f::UnitY();
            direction = -Eigen::Vector2f::UnitY();
            positiveSign = true;
            enable = true;
        });

        registerRayContactBulk([&](
                const std::vector<float>& params,
                Eigen::Vector2f& first,
                Eigen::Vector2f& direction,
                bool& enable
        ) {

            first = 0.5f * Eigen::Vector2f::Ones() + 0.5f * (getStretch(params) - getBulge(params)) * Eigen::Vector2f::UnitY();
            direction = Eigen::Vector2f::UnitY();
            enable = true;
        });

        registerLineBulk([&](
                const std::vector<float>& params,
                Eigen::Vector2f& first,
                Eigen::Vector2f& second,
                bool& positiveSign,
                bool& enable
        ) {

            first = 0.5f * Eigen::Vector2f::UnitY() - getOffset(params) * Eigen::Vector2f::UnitX();
            second = 0.5f * Eigen::Vector2f::Ones() - 0.5f * getStretch(params) * Eigen::Vector2f::UnitX();
            positiveSign = true;
            enable = std::abs(getBulge(params)) < 1e-6;
        });

        registerArcBulk([&](
                const std::vector<float>& params,
                Eigen::Vector2f& center,
                float& radius,
                Eigen::Vector2f& firstNormal,
                Eigen::Vector2f& secondNormal,
                bool& enable
        ) {

            Eigen::Vector2f topControl;
            Eigen::Vector2f botControl;

            computeControls(params, topControl, botControl, radius);

            center = topControl;
            radius *= -1;

            firstNormal = Eigen::Vector2f::UnitX();
            secondNormal = Eigen::Vector2f(topControl[1] - botControl[1], botControl[0] - topControl[0]);
            enable = std::abs(getBulge(params)) >= 1e-6;
        });

        registerArcBulk([&](
                const std::vector<float>& params,
                Eigen::Vector2f& center,
                float& radius,
                Eigen::Vector2f& firstNormal,
                Eigen::Vector2f& secondNormal,
                bool& enable
        ) {

            Eigen::Vector2f topControl;
            Eigen::Vector2f botControl;

            computeControls(params, topControl, botControl, radius);

            center = botControl;

            firstNormal = -Eigen::Vector2f::UnitX();
            secondNormal = -Eigen::Vector2f(topControl[1] - botControl[1], botControl[0] - topControl[0]);
            enable = std::abs(getBulge(params)) >= 1e-6;
        });
    }

    virtual std::string getName() const override {
        return std::string("BinaryFloatNParametersOperatorPointSculptLine");
    }

private:

    float getStretch(const std::vector<float>& params) {
        return (params.empty() ? 0.5f : params[0]);
    }

    float getBulge(const std::vector<float>& params) {
        return ((params.size() < 2) ? 0.f : params[1]) * 0.5f;
    }

    float getOffset(const std::vector<float>& params) {
        float offset = ((params.size() < 3) ? 0.f : params[2]) * 0.5f;
        return -std::min(-offset, 0.5f * (1.f - getStretch(params)));
    }

    void computeControls(
            const std::vector<float>& params,
            Eigen::Vector2f& topControl,
            Eigen::Vector2f& botControl,
            float& radius) {

        Eigen::Vector2f bot =
                0.5f * Eigen::Vector2f::Ones()
                 - 0.5f * getBulge(params) * Eigen::Vector2f::UnitY()
                 - 0.5f * getStretch(params) * Eigen::Vector2f::UnitX();
        Eigen::Vector2f top = 0.5f * Eigen::Vector2f::UnitY() - getOffset(params) * Eigen::Vector2f::UnitX();

        Eigen::Vector2f midBotTop = (bot + top) / 2;
        Eigen::Vector2f normal = Eigen::Vector2f(bot[1] - top[1], top[0] - bot[0]);

        float botOffset = (bot[0] - (midBotTop[0] + bot[0]) / 2.f) / normal[0];
        botControl = (midBotTop + bot) / 2.f + botOffset * normal;

        float topOffset = (top[0] - (midBotTop[0] + top[0]) / 2.f) / normal[0];
        topControl = (midBotTop + top) / 2.f + topOffset * normal;

        radius = std::abs(botControl[1] - bot[1]);
    }
};

}
}
}
}
