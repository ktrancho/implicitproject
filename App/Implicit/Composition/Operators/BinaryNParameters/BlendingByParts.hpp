// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#include <Implicit/Composition/Operators/BinaryNParameters/DistanceFieldParts.hpp>

namespace Implicit {
namespace Operator {
namespace Binary {
namespace NParameters {

class BlendingByParts : public Implicit::Operator::Binary::NParameters::DistanceFieldParts {
public:
    BlendingByParts() {
        registerParts();
    }

    virtual void registerParts() {

        /*registerRay(
                [&](const std::vector<float>& params) {
                    return getBlending(params) * Eigen::Vector2f::UnitX() + 0.5 * Eigen::Vector2f::UnitY();
                },
                [&](const std::vector<float>& params) {
                    return -Eigen::Vector2f::UnitX();
                },
                [&](const std::vector<float>& params) {
                    return false;
                }
                );

        registerRay(
                [&](const std::vector<float>& params) {
                    return getBlending(params) * Eigen::Vector2f::UnitY() + 0.5f * Eigen::Vector2f::UnitX();
                },
                [&](const std::vector<float>& params) {
                    return -Eigen::Vector2f::UnitY();
                },
                [&](const std::vector<float>& params) {
                    return true;
                }
                );

        registerArc(
                [&](const std::vector<float>& params) {
                    return getBlending(params) * Eigen::Vector2f::Ones();
                },
                [&](const std::vector<float>& params) {
                    return getBlending(params) - 0.5f;
                },
                [&](const std::vector<float>& params) {
                    return Eigen::Vector2f::UnitX();
                },
                [&](const std::vector<float>& params) {
                    return Eigen::Vector2f::UnitY();
                }
                );*/

        registerRayBulk([&](
                const std::vector<float>& params,
                Eigen::Vector2f& first,
                Eigen::Vector2f& second,
                bool& positiveSign,
                bool& enable
                ) {
            first = getBlending(params) * Eigen::Vector2f::UnitX() + 0.5 * Eigen::Vector2f::UnitY();
            second = -Eigen::Vector2f::UnitX();
            positiveSign = false;
            enable = true;
        });

        registerRayBulk([&](
                const std::vector<float>& params,
                Eigen::Vector2f& first,
                Eigen::Vector2f& second,
                bool& positiveSign,
                bool& enable
                ) {
            first = getBlending(params) * Eigen::Vector2f::UnitY() + 0.5f * Eigen::Vector2f::UnitX();
            second = -Eigen::Vector2f::UnitY();
            positiveSign = true;
            enable = true;
        });

        registerArcBulk([&](
                const std::vector<float>& params,
                Eigen::Vector2f& center,
                float& radius,
                Eigen::Vector2f& firstNormal,
                Eigen::Vector2f& secondNormal,
                bool& enable) {
            center = getBlending(params) * Eigen::Vector2f::Ones();
            radius = getBlending(params) - 0.5f;
            firstNormal = Eigen::Vector2f::UnitX();
            secondNormal = Eigen::Vector2f::UnitY();
            enable = true;
        });
    }

    virtual std::string getName() const override {
        return std::string("BinaryFloatNParametersOperatorBlendingByParts");
    }

private:

    float getBlending(const std::vector<float>& params) {
        return ajustParameter(params.empty() ? 1.f : params[0]);
    }

    float ajustParameter(float p) const {
        return (1 - p) / 2;
    }
};

}
}
}
}
