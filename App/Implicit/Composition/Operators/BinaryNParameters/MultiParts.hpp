// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <Eigen/Dense>
#include <vector>
#include <functional>

#include <Implicit/Composition/Types/BinaryFloatNParameters.hpp>

namespace Implicit {
namespace Operator {
namespace Binary {
namespace NParameters {

class MultiParts : public Implicit::Operator::NParameters::BinaryFloat {
protected:
    MultiParts() {}

public:

    virtual bool evalFloat(float f1, float f2, const std::vector<float>& params, float &value) const override {
        Eigen::Vector2f _;
        return evalFieldValue(f1, f2, params, value, _);
    }

    virtual bool evalGradient(float f1, float f2, const std::vector<float>& params, Eigen::Vector2f &gradient) const override {
        float _;
        return evalFieldValue(f1, f2, params, _, gradient);
    }

    virtual bool evalFieldValue(float f1, float f2, const std::vector<float>& params, float &value, Eigen::Vector2f &gradient) const override;

    virtual std::string getName() const override {
        return std::string("BinaryFloatNParametersOperatorMultiParts");
    }

protected:
    std::vector<std::function<bool(float f1, float f2, std::vector<float> p, float &value, Eigen::Vector2f &gradient)>> parts;
};

}
}
}
}

