// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#include "Factory.hpp"

#include <Implicit/Composition/Operators/NaryOneParameter/Blending.hpp>
#include <Implicit/Composition/Operators/NaryOneParameter/FromBinaryBuilder.hpp>
#include <Implicit/Composition/Operators/NaryOneParameter/FromBinaryBuilderBlending.hpp>
#include <Implicit/Composition/Operators/Binary/Factory.hpp>

namespace Implicit {
namespace Operator {
namespace Nary {
namespace OneParameter {

std::shared_ptr<Implicit::Operator::OneParameter::NaryFloat> Factory::blending() {
    std::shared_ptr<Implicit::Operator::Nary::OneParameter::Blending> op(new Implicit::Operator::Nary::OneParameter::Blending());
    return op;
}

std::shared_ptr<Implicit::Operator::OneParameter::NaryFloat> Factory::contact() {
    std::shared_ptr<Implicit::Operator::Nary::OneParameter::FromBinaryBuilder> op(
            new Implicit::Operator::Nary::OneParameter::FromBinaryBuilder(
                    Implicit::Operator::Binary::Factory::oneSidedContact()
                    )
            );
    return op;
}

std::shared_ptr<Implicit::Operator::OneParameter::NaryFloat> Factory::bulge() {
    std::shared_ptr<Implicit::Operator::Nary::OneParameter::FromBinaryBuilder> op(
            new Implicit::Operator::Nary::OneParameter::FromBinaryBuilder(
                    Implicit::Operator::Binary::Factory::oneSidedFullBulge()
            )
    );
    return op;
}

std::shared_ptr<Implicit::Operator::OneParameter::NaryFloat> Factory::stretch() {
    std::shared_ptr<Implicit::Operator::Nary::OneParameter::FromBinaryBuilder> op(
            new Implicit::Operator::Nary::OneParameter::FromBinaryBuilder(
                    Implicit::Operator::Binary::Factory::oneSidedFullStretching()
            )
    );
    return op;
}

std::shared_ptr<Implicit::Operator::OneParameter::NaryFloat> Factory::builtBlending() {
    std::shared_ptr<Implicit::Operator::Nary::OneParameter::FromBinaryBuilderBlending> op(
            new Implicit::Operator::Nary::OneParameter::FromBinaryBuilderBlending(
                    Implicit::Operator::Binary::Factory::largeBlending()
            )
    );
    return op;
}

bool Factory::byName(const std::string &name, std::shared_ptr<Implicit::Operator::OneParameter::NaryFloat> &res) {
    if (name == "Blending") {
        res = blending();
        return true;
    } else if (name == "Contact") {
        res = contact();
        return true;
    } else if (name == "Bulge") {
        res = contact();
        return true;
    } else if (name == "Stretch") {
        res = contact();
        return true;
    } else if (name == "BuiltBlending") {
        res = contact();
        return true;
    }
    return false;
}

std::vector<std::string> Factory::getNames() {
    std::vector<std::string> names;
    names.push_back("Blending");
    names.push_back("Contact");
    names.push_back("Bulge");
    names.push_back("Stretch");
    names.push_back("BuiltBlending");
    return names;
}

}
}
}
}
