// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <memory>
#include <string>
#include <vector>

#include <Implicit/Composition/Types/NaryFloatOneParameter.hpp>

namespace Implicit {
namespace Operator {
namespace Nary {
namespace OneParameter {

class Factory {
public:
    static std::shared_ptr<Implicit::Operator::OneParameter::NaryFloat> blending();

    static std::shared_ptr<Implicit::Operator::OneParameter::NaryFloat> contact();

    static std::shared_ptr<Implicit::Operator::OneParameter::NaryFloat> bulge();

    static std::shared_ptr<Implicit::Operator::OneParameter::NaryFloat> stretch();

    static std::shared_ptr<Implicit::Operator::OneParameter::NaryFloat> builtBlending();

    static bool byName(const std::string &name, std::shared_ptr<Implicit::Operator::OneParameter::NaryFloat> &res);

    static std::vector<std::string> getNames();
};

}
}
}
}
