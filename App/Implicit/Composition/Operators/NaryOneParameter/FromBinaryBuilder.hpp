// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.


#include <Implicit/Composition/Types/NaryFloatOneParameter.hpp>
#include <Implicit/Composition/Types/BinaryFloat.hpp>

namespace Implicit {
namespace Operator {
namespace Nary {
namespace OneParameter {

class FromBinaryBuilder : public Implicit::Operator::OneParameter::NaryFloat {
public:
    FromBinaryBuilder(const std::shared_ptr<Implicit::Operator::BinaryFloat>& a_oneSided) :
        oneSided(a_oneSided) {
        registerChild(oneSided);
    }

    virtual bool evalFloat(const std::vector<float> &f, float p, float &value) const override {

        for(int i = 0; i < f.size(); ++i) {
            float sum = 0;
            float maxValue = -1e9;
            float minValue = 1e9;

            float offset = p;

            offset = ajustParameter(offset);

            for(int j = 0; j < f.size(); ++j) {
                if(i == j) {
                    continue;
                }
                sum += std::pow(std::max(f[j] - offset, 0.f), 2);
                maxValue = std::max(maxValue, f[j]);
                minValue = std::min(minValue, f[j]);
            }

            if(maxValue < offset) {
                sum = maxValue;
            } else {
                sum = offset + std::sqrt(sum);
            }

            float res;
            oneSided->evalFloat(sum, f[i], res);

            if(i == 0) {
                value = res;
            } else {
                if(value <= 0.5f) {
                    value = std::max(value, res);
                } else if((value >= 0.5f && res >= 0.5f) && (abs(res - 0.5f) < abs(value - 0.5f))) {
                    value = res;
                }
            }
        }

        return true;
    }

    virtual bool evalGradient(const std::vector<float> &f, float, Eigen::VectorXf &gradient) const override {
        //TODO

        return false;
    }

    virtual std::string getName() const override {
        return std::string("NaryFloatOperatorOneParameterFromBinaryBuilder");
    }

private:
    float ajustParameter(float p) const {
        return (1 - p) / 2;
    }

private:
    std::shared_ptr<Implicit::Operator::BinaryFloat> oneSided;
};

}
}
}
}
