// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.


#include <Implicit/Composition/Types/NaryFloatOneParameter.hpp>

namespace Implicit {
namespace Operator {
namespace Nary {
namespace OneParameter {

class Blending : public Implicit::Operator::OneParameter::NaryFloat {
public:
    Blending() {}

    virtual bool evalFloat(const std::vector<float> &f, float p, float &value) const override {

        p = ajustParameter(p);

        float squaredPositiveNorm = 0;
        float maxValue = -1e9;

        for(float v : f) {
            squaredPositiveNorm += std::pow(std::max(v - p, 0.f), 2);
            maxValue = std::max(maxValue, v);
        }

        if(maxValue < p) {
            value = maxValue;
            return true;
        }

        value = p + std::sqrt(squaredPositiveNorm);
        return true;
    }

    virtual bool evalGradient(const std::vector<float> &f, float p, Eigen::VectorXf &gradient) const override {

        p = ajustParameter(p);

        gradient = Eigen::VectorXf(f.size());
        float maxValue = std::numeric_limits<float>::min();
        int maxId = -1;

        for(long unsigned int i = 0; i < f.size(); ++i) {
            float v = f[i];
            gradient[i] = v - p;
            if(v > maxValue) {
                maxId = i;
                maxValue = v;
            }
        }

        if(maxValue < p) {
            gradient.fill(0.f);
            gradient[maxId] = 1.f;
            return true;
        }

        gradient.normalize();
        return true;
    }

    virtual std::string getName() const override {
        return std::string("NaryFloatOperatorOneParameterBlending");
    }

private:
    float ajustParameter(float p) const {
        return (1 - p) / 2;
    }
};

}
}
}
}
