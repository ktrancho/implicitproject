// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <vector>

#include <Eigen/Dense>

#include <Implicit/Composition/Types/Operator.hpp>

namespace Implicit {
namespace Operator {

class NaryFloat : public Operator {
protected:
    NaryFloat() {}

public:
    virtual ~NaryFloat() {}

    /// Field value of the composition.
    virtual bool evalFloat(const std::vector<float>& f, float& value) const = 0;

    /// Gradient of the operator for the given values.
    virtual bool evalGradient(const std::vector<float>& f, Eigen::VectorXf& gradient) const = 0;

    /// Bulk operation.
    virtual bool evalFieldValue(const std::vector<float>& f, float& value, Eigen::VectorXf& gradient) const {
        bool check = evalFloat(f, value);
        return evalGradient(f, gradient) && check;
    }

    virtual std::string getOperatorType() const override {
        return "NaryFloat";
    }

    virtual std::string getName() const override {
        return std::string("NaryFloatOperator");
    }
};

}
}