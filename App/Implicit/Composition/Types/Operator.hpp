// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <string>

#include <Eigen/Dense>

#include <Implicit/Engine/Tree/Component.hpp>

namespace Implicit {
namespace Operator {

///TODO : know original surface isosurfaces operator preserves : boundary if exists.

class Operator : public Implicit::Engine::Tree::Component {
protected:
    Operator() {}

public:
    virtual ~Operator() {}

    virtual std::string getOperatorType() const {
        return "Operator";
    }

    virtual std::string getType() const {
        return std::string("Operator");
    }

    virtual std::string getName() const override {
        return std::string("Operator");
    }
};

}
}
