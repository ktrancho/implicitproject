// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <Eigen/Dense>

#include <Implicit/Composition/Types/Operator.hpp>

namespace Implicit {
namespace Operator {
namespace NParameters {

class BinaryFloat : public Operator {
protected:
    BinaryFloat() {}

public:
    virtual ~BinaryFloat() {}

    /// Field value of the composition.
    virtual bool evalFloat(float f1, float f2, const std::vector<float>& p, float &value) const = 0;

    /// Gradient of the operator for the given values.
    virtual bool evalGradient(float f1, float f2, const std::vector<float>& p, Eigen::Vector2f &gradient) const = 0;

    /// Bulk operation.
    virtual bool evalFieldValue(float f1, float f2, const std::vector<float>& p, float &value, Eigen::Vector2f &gradient) const {
        bool check = evalFloat(f1, f2, p, value);
        return evalGradient(f1, f2, p, gradient) && check;
    }

    virtual std::string getOperatorType() const override {
        return "BinaryFloatNParameters";
    }

    virtual std::string getName() const override {
        return std::string("BinaryFloatOperatorNParameters");
    }
};

}
}
}