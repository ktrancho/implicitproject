// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <Eigen/Dense>

#include <Implicit/Composition/Types/Operator.hpp>
#include <Implicit/Core/Types/Types2D/Types.hpp>

namespace Implicit {
namespace Operator {

class ImplicitValue2DBinary : public Operator {
protected:
    ImplicitValue2DBinary() {}

public:
    virtual ~ImplicitValue2DBinary() {}

    /// Field value of the composition.
    virtual bool evalFloat(const Implicit::Core::Vector2f::ImplicitValue& f1,
            const Implicit::Core::Vector2f::ImplicitValue& f2, float& value) const = 0;

    /// Gradient of the operator for the given values.
    virtual bool evalGradient(const Implicit::Core::Vector2f::ImplicitValue& f1,
            const Implicit::Core::Vector2f::ImplicitValue& f2, Eigen::Vector2f& gradient) const = 0;

    /// Bulk operation.
    virtual bool evalFieldValue(const Implicit::Core::Vector2f::ImplicitValue& f1,
            const Implicit::Core::Vector2f::ImplicitValue& f2, float& value, Eigen::Vector2f& gradient) const {
        bool check = evalFloat(f1, f2, value);
        return evalGradient(f1, f2, gradient) && check;
    }

    virtual std::string getOperatorType() const override {
        return "BinaryImplicitValue";
    }

    virtual std::string getName() const override {
        return std::string("BinaryImplicitValueOperator");
    }
};

}
}