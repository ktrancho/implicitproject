// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#include "NaryOneParameterComposite.hpp"

namespace Implicit {
namespace Engine {
namespace Vector2f {
namespace Composition {
namespace OneParameter {

bool NaryComposite::getFloatField(const Eigen::Vector2f &position, float &value) const {
    std::vector<float> v;
    bool check = true;

    v.resize(fields.size());

    for (long unsigned int i = 0; i < fields.size(); ++i) {
        check = fields[i]->getFloatField(position, v[i]) && check;
    }
    float p = 0;
    parameter->getFloatField(position, p);

    return op->evalFloat(v, p, value) && check;
}

bool NaryComposite::getGradientField(const Eigen::Vector2f &position, Eigen::Vector2f &gradient) const {
    Implicit::Core::Vector2f::ImplicitValue res;
    bool check = getImplicitField(position, res);
    gradient = res.gradient;
    return check;
}

bool NaryComposite::getImplicitField(const Eigen::Vector2f &position, Implicit::Core::Vector2f::ImplicitValue &value) const {

    std::vector<float> v;
    std::vector<Implicit::Core::Vector2f::ImplicitValue> iv;
    Eigen::VectorXf gradient;
    float opValue = 0;

    bool check;

    v.resize(fields.size());
    iv.resize(fields.size());

    for (long unsigned int i = 0; i < fields.size(); ++i) {
        check = fields[i]->getImplicitField(position, iv[i]);
        v[i] = iv[i].field;
    }
    float p = 0;
    parameter->getFloatField(position, p); // TODO : parameter should be taken into account in gradient computation

    check = op->evalFieldValue(v, p, opValue, gradient) && check;

    value.field = opValue;
    value.gradient = Eigen::Vector2f::Zero();

    for (int i = 0; i < fields.size(); ++i) {
        value.gradient += gradient[i] * iv[i].gradient;
    }

    return check;
}

bool NaryComposite::getSpaceBoundingBox(Eigen::AlignedBox2f &boundingBox) const {
    Eigen::AlignedBox2f res(Eigen::Vector2f::Zero(), Eigen::Vector2f::Zero());

    for (auto f : fields) {
        Eigen::AlignedBox2f b;
        if (!f->getSpaceBoundingBox(b)) {
            continue;
        }
        res.extend(b);
    }

    boundingBox.extend(res);
    return true;
}

bool NaryComposite::getSurfaceBoundingBox(Eigen::AlignedBox2f &boundingBox) const {
    /// TODO : know more about operator : for example bulge would not be captured by simple extension.

    Eigen::AlignedBox2f res(Eigen::Vector2f::Zero(), Eigen::Vector2f::Zero());

    for (auto f : fields) {
        Eigen::AlignedBox2f b;
        if (!f->getSurfaceBoundingBox(b)) {
            continue;
        }
        res.extend(b);
    }

    boundingBox.extend(res);
    return true;
}

bool NaryComposite::getIsoBoundingBox(float targetIso, Eigen::AlignedBox2f &boundingBox) const {

    Eigen::AlignedBox2f res(Eigen::Vector2f::Zero(), Eigen::Vector2f::Zero());

    for (auto f : fields) {
        Eigen::AlignedBox2f b;
        if (!f->getIsoBoundingBox(targetIso, b)) {
            continue;
        }
        res.extend(b);
    }

    boundingBox.extend(res);
    return true;
}

}
}
}
}
}
