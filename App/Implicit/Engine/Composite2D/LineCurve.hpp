// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <vector>
#include <iostream>
#include <memory>

#include <Implicit/Core/Types/Types2D/ImplicitField.hpp>
#include <Implicit/Composition/Types/BinaryFloat.hpp>

#include <Implicit/Core/Primitives2D/AllFactories.hpp>
#include <Implicit/Engine/Transforms2D/Factory.hpp>
#include <Implicit/Engine/Composite2D/BinaryComposite.hpp>

namespace Implicit {
namespace Engine {
namespace Vector2f {
namespace Composition {

class LineCurve : public Implicit::Core::Vector2f::ImplicitField {
public:
    LineCurve(const std::shared_ptr<Implicit::Operator::BinaryFloat>& a_operator) :
        op(a_operator) {
    }

    virtual bool getFloatField(const Eigen::Vector2f &position, float &value) const override {
        return root->getFloatField(position, value);
    }

    virtual bool getGradientField(const Eigen::Vector2f &position, Eigen::Vector2f &gradient) const override {
        return root->getGradientField(position, gradient);
    }

    virtual bool getImplicitField(const Eigen::Vector2f &position, Implicit::Core::Vector2f::ImplicitValue &value) const override {
        return root->getImplicitField(position, value);
    }

    virtual bool getSpaceBoundingBox(Eigen::AlignedBox2f& boundingBox) const override {
        return root->getSpaceBoundingBox(boundingBox);
    }

    virtual bool getSurfaceBoundingBox(Eigen::AlignedBox2f& boundingBox) const override {
        return root->getSurfaceBoundingBox(boundingBox);
    }

    virtual bool getIsoBoundingBox(float targetIso, Eigen::AlignedBox2f& boundingBox) const override {
        return root->getIsoBoundingBox(targetIso, boundingBox);
    }

    virtual std::string getType() const override {
        return std::string("LineCurve");
    }

    virtual std::string getName() const override {
        return std::string("LineCurve");
    }

    void changeOperator(const std::shared_ptr<Implicit::Operator::BinaryFloat>& a_operator) {
        op = a_operator;
        buildCurve();
    }

    void addPosition(const Eigen::Vector2f& position) {
        positions.push_back(position);
    }

    void buildCurve() {
        clearChildren();

        if(positions.size() <= 0) {
            std::cerr << "Building void curve" << std::endl;
        } else if(positions.size() <= 1) {
            root = Implicit::Engine::Vector2f::Transforms::Factory::translate(Implicit::Core::Vector2f::DistanceFields::Factory::createCircle(), positions[0]);
        } else {
            root = Implicit::Core::Vector2f::DistanceFields::Factory::createSegment(positions[0], positions[1]);
        }

        for(long unsigned int i = 2; i < positions.size(); ++i) {
            std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> segment =
                    Implicit::Core::Vector2f::DistanceFields::Factory::createSegment(positions[i - 1], positions[i]);
            root = std::shared_ptr<Implicit::Engine::Vector2f::Composition::BinaryComposite>(
                    new Implicit::Engine::Vector2f::Composition::BinaryComposite(root, segment, op));
        }

        registerChild(root);
        registerChild(op);
    }
private:
    std::vector<Eigen::Vector2f> positions;
    std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> root;
public:
    std::shared_ptr<Implicit::Operator::BinaryFloat> op;
};

}
}
}
}



