// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#include "Factory.hpp"

#include <Implicit/Engine/Composite2D/BinaryComposite.hpp>
#include <Implicit/Engine/Composite2D/NaryOneParameterComposite.hpp>
#include <Implicit/Engine/Composite2D/LineCurve.hpp>
#include <Implicit/Engine/Composite2D/BinaryNParametersComposite.hpp>

namespace Implicit {
namespace Engine {
namespace Vector2f {
namespace Composition {

std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> Factory::binaryComposite(
            const std::shared_ptr<Implicit::Core::Vector2f::ImplicitField>& a_first,
            const std::shared_ptr<Implicit::Core::Vector2f::ImplicitField>& a_second,
            const std::shared_ptr<Implicit::Operator::BinaryFloat>& a_operator) {
    std::shared_ptr<Implicit::Engine::Vector2f::Composition::BinaryComposite> composite(
            new Implicit::Engine::Vector2f::Composition::BinaryComposite(
                    a_first, a_second, a_operator));
    return composite;
};

std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> Factory::lineCurve(
        const std::shared_ptr<Implicit::Operator::BinaryFloat>& a_operator,
        const std::vector<Eigen::Vector2f> points) {
    std::shared_ptr<Implicit::Engine::Vector2f::Composition::LineCurve> composite(
            new Implicit::Engine::Vector2f::Composition::LineCurve(
                    a_operator));
    for(auto point : points) {
        composite->addPosition(point);
    }
    composite->buildCurve();
    return composite;
};

std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> Factory::naryOneParameterComposite(
        const std::vector<std::shared_ptr<Implicit::Core::Vector2f::ImplicitField>>& a_fields,
        const std::shared_ptr<Implicit::Core::Vector2f::ImplicitField>& a_parameter,
        const std::shared_ptr<Implicit::Operator::OneParameter::NaryFloat>& a_operator) {
    std::shared_ptr<Implicit::Engine::Vector2f::Composition::OneParameter::NaryComposite> composite(
            new Implicit::Engine::Vector2f::Composition::OneParameter::NaryComposite(
                a_fields, a_parameter, a_operator));
    return composite;
}

std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> Factory::binaryNParametersComposite(
        const std::shared_ptr<Implicit::Core::Vector2f::ImplicitField>& a_first,
        const std::shared_ptr<Implicit::Core::Vector2f::ImplicitField>& a_second,
        const std::vector<std::shared_ptr<Implicit::Core::Vector2f::ImplicitField>>& a_parameters,
        const std::shared_ptr<Implicit::Operator::NParameters::BinaryFloat>& a_operator) {
    std::shared_ptr<Implicit::Engine::Vector2f::Composition::NParameters::BinaryComposite> composite(
            new Implicit::Engine::Vector2f::Composition::NParameters::BinaryComposite(
                            a_first,
                            a_second,
                            a_parameters,
                            a_operator
                    )
            );
    return composite;
}

}
}
}
}
