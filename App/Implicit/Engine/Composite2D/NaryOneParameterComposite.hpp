// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <memory>
#include <vector>

#include <Implicit/Core/Types/Types2D/ImplicitField.hpp>
#include <Implicit/Composition/Types/NaryFloatOneParameter.hpp>

namespace Implicit {
namespace Engine {
namespace Vector2f {
namespace Composition {
namespace OneParameter {

class NaryComposite : public Implicit::Core::Vector2f::ImplicitField {
public:
    NaryComposite(const std::vector<std::shared_ptr<Implicit::Core::Vector2f::ImplicitField>> &a_fields,
                  const std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> &a_parameter,
                  const std::shared_ptr<Implicit::Operator::OneParameter::NaryFloat> &a_operator) :
            fields(a_fields),
            parameter(a_parameter),
            op(a_operator) {

        for (auto f : fields) {
            registerChild(f);
        }
        registerChild(op);
        registerChild(parameter);
    }

    virtual bool getFloatField(const Eigen::Vector2f &position, float &value) const override;

    virtual bool getGradientField(const Eigen::Vector2f &position, Eigen::Vector2f &gradient) const override;

    virtual bool
    getImplicitField(const Eigen::Vector2f &position, Implicit::Core::Vector2f::ImplicitValue &value) const override;

    virtual bool getSpaceBoundingBox(Eigen::AlignedBox2f &boundingBox) const override;

    virtual bool getSurfaceBoundingBox(Eigen::AlignedBox2f &boundingBox) const override;

    virtual bool getIsoBoundingBox(float targetIso, Eigen::AlignedBox2f &boundingBox) const override;

    virtual std::string getType() const override {
        return std::string("NaryOneParameterComposite");
    }

    virtual std::string getName() const override {
        return std::string("NaryOneParameterComposite");
    }

    void changeOperator(const std::shared_ptr<Implicit::Operator::OneParameter::NaryFloat> &a_operator) {
        op = a_operator;
        clearChildren();
        for (auto f : fields) {
            registerChild(f);
        }
        registerChild(op);
        registerChild(parameter);
    }

    int size() {
        return fields.size();
    }

private:
    std::vector<std::shared_ptr<Implicit::Core::Vector2f::ImplicitField>> fields;
    std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> parameter;
public:
    std::shared_ptr<Implicit::Operator::OneParameter::NaryFloat> op;
};

}
}
}
}
}