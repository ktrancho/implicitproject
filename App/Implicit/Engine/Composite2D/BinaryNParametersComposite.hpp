// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <memory>

#include <Implicit/Core/Types/Types2D/ImplicitField.hpp>
#include <Implicit/Composition/Types/BinaryFloatNParameters.hpp>

namespace Implicit {
namespace Engine {
namespace Vector2f {
namespace Composition {
namespace NParameters {

class BinaryComposite : public Implicit::Core::Vector2f::ImplicitField {
public:
    BinaryComposite(const std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> &a_first,
                    const std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> &a_second,
                    const std::vector<std::shared_ptr<Implicit::Core::Vector2f::ImplicitField>>& a_parameters,
                    const std::shared_ptr<Implicit::Operator::NParameters::BinaryFloat> &a_operator) :
            first(a_first),
            second(a_second),
            parameters(a_parameters),
            op(a_operator) {
        registerChild(first);
        registerChild(second);
        for(auto p : parameters) {
            registerChild(p);
        }
        registerChild(op);
    }

    virtual bool getFloatField(const Eigen::Vector2f &position, float &value) const override;

    virtual bool getGradientField(const Eigen::Vector2f &position, Eigen::Vector2f &gradient) const override;

    virtual bool getImplicitField(const Eigen::Vector2f &position,
            Implicit::Core::Vector2f::ImplicitValue &value) const override;

    virtual bool getSpaceBoundingBox(Eigen::AlignedBox2f &boundingBox) const override;

    virtual bool getSurfaceBoundingBox(Eigen::AlignedBox2f &boundingBox) const override;

    virtual bool getIsoBoundingBox(float targetIso, Eigen::AlignedBox2f &boundingBox) const override;

    virtual std::string getType() const override {
        return std::string("BinaryNParametersComposite");
    }

    virtual std::string getName() const override {
        return std::string("BinaryNParametersComposite");
    }

    void changeOperator(const std::shared_ptr<Implicit::Operator::NParameters::BinaryFloat> &a_operator) {
        op = a_operator;
        clearChildren();
        registerChild(first);
        registerChild(second);
        for(auto p : parameters) {
            registerChild(p);
        }
        registerChild(op);
    }

    int size() {
        return parameters.size();
    }

private:
    std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> first;
    std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> second;
    std::vector<std::shared_ptr<Implicit::Core::Vector2f::ImplicitField>> parameters;
public:
    std::shared_ptr<Implicit::Operator::NParameters::BinaryFloat> op;
};

}
}
}
}
}
