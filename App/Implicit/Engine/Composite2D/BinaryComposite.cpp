// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#include "BinaryComposite.hpp"

namespace Implicit {
namespace Engine {
namespace Vector2f {
namespace Composition {

bool BinaryComposite::getFloatField(const Eigen::Vector2f &position, float &value) const {
    float v1;
    float v2;
    bool check = first->getFloatField(position, v1);
    check = second->getFloatField(position, v2) && check;
    return op->evalFloat(v1, v2, value) && check;
}

bool BinaryComposite::getGradientField(const Eigen::Vector2f &position, Eigen::Vector2f &gradient) const {
    Implicit::Core::Vector2f::ImplicitValue v1;
    Implicit::Core::Vector2f::ImplicitValue v2;
    bool check = first->getImplicitField(position, v1);
    check = second->getImplicitField(position, v2) && check;
    Eigen::Vector2f opGrad;
    check = op->evalGradient(v1.field, v2.field, opGrad) && check;
    gradient = opGrad[0] * v1.gradient + opGrad[1] * v2.gradient;
    return check;
}

bool BinaryComposite::getImplicitField(const Eigen::Vector2f &position, Implicit::Core::Vector2f::ImplicitValue &value) const {
    Implicit::Core::Vector2f::ImplicitValue v1;
    Implicit::Core::Vector2f::ImplicitValue v2;
    bool check = first->getImplicitField(position, v1);
    check = second->getImplicitField(position, v2) && check;
    Eigen::Vector2f opGrad;
    check = op->evalFieldValue(v1.field, v2.field, value.field, opGrad) && check;
    value.gradient = opGrad[0] * v1.gradient + opGrad[1] * v2.gradient;
    return check;
}

bool BinaryComposite::getSpaceBoundingBox(Eigen::AlignedBox2f& boundingBox) const {
    Eigen::AlignedBox2f res;
    if(!(first->getSpaceBoundingBox(boundingBox))) {
        return second->getSpaceBoundingBox(boundingBox);
    }
    if(!(second->getSpaceBoundingBox(res))) {
        return true;
    }
    boundingBox.extend(res);
    return true;
}

bool BinaryComposite::getSurfaceBoundingBox(Eigen::AlignedBox2f& boundingBox) const {
    /// TODO : know more about operator : for example bulge would not be captured by simple extension.
    Eigen::AlignedBox2f res;
    if(!(first->getSurfaceBoundingBox(boundingBox))) {
        return second->getSurfaceBoundingBox(boundingBox);
    }
    if(!(second->getSurfaceBoundingBox(res))) {
        return true;
    }
    boundingBox.extend(res);
    return true;
}

bool BinaryComposite::getIsoBoundingBox(float targetIso, Eigen::AlignedBox2f& boundingBox) const {
    Eigen::AlignedBox2f res;
    if(!(first->getIsoBoundingBox(targetIso, boundingBox))) {
        return second->getIsoBoundingBox(targetIso, boundingBox);
    }
    if(!(second->getIsoBoundingBox(targetIso, res))) {
        return true;
    }
    boundingBox.extend(res);
    return true;
}

}
}
}
}