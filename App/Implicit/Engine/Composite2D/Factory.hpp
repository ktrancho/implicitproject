// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <memory>
#include <vector>
#include <Eigen/Dense>

#include <Implicit/Core/Types/Types2D/ImplicitField.hpp>
#include <Implicit/Composition/Types/All.hpp>

namespace Implicit {
namespace Engine {
namespace Vector2f {
namespace Composition {

class Factory {
public:
    static std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> binaryComposite(
            const std::shared_ptr<Implicit::Core::Vector2f::ImplicitField>& a_first,
            const std::shared_ptr<Implicit::Core::Vector2f::ImplicitField>& a_second,
            const std::shared_ptr<Implicit::Operator::BinaryFloat>& a_operator);

    static std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> lineCurve(
            const std::shared_ptr<Implicit::Operator::BinaryFloat>& a_operator,
            const std::vector<Eigen::Vector2f> points);

    static std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> naryOneParameterComposite(
            const std::vector<std::shared_ptr<Implicit::Core::Vector2f::ImplicitField>>& a_fields,
            const std::shared_ptr<Implicit::Core::Vector2f::ImplicitField>& a_parameter,
            const std::shared_ptr<Implicit::Operator::OneParameter::NaryFloat>& a_operator);

    static std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> binaryNParametersComposite(
            const std::shared_ptr<Implicit::Core::Vector2f::ImplicitField>& a_first,
            const std::shared_ptr<Implicit::Core::Vector2f::ImplicitField>& a_second,
            const std::vector<std::shared_ptr<Implicit::Core::Vector2f::ImplicitField>>& a_parameters,
            const std::shared_ptr<Implicit::Operator::NParameters::BinaryFloat>& a_operator);
};

}
}
}
}
