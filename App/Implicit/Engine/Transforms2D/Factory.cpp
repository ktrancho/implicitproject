// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#include "Factory.hpp"

#include <Implicit/Engine/Transforms2D/Translation.hpp>
#include <Implicit/Engine/Transforms2D/Scale.hpp>
#include <Implicit/Engine/Transforms2D/Invert.hpp>
#include <Implicit/Engine/Transforms2D/Bound.hpp>
#include <Implicit/Engine/Transforms2D/CleanBound.hpp>
#include <Implicit/Engine/Transforms2D/FieldScale.hpp>

namespace Implicit {
namespace Engine {
namespace Vector2f {
namespace Transforms {

std::shared_ptr<Core::Vector2f::ImplicitField> Factory::translate(
        const std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> &field, const Eigen::Vector2f &translation) {
    std::shared_ptr<Implicit::Engine::Vector2f::Transforms::Translation> moved(
            new Implicit::Engine::Vector2f::Transforms::Translation(field, translation));
    return moved;
}

std::shared_ptr<Core::Vector2f::ImplicitField> Factory::scale(
        const std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> &field, const Eigen::Vector2f& scale) {
    std::shared_ptr<Implicit::Engine::Vector2f::Transforms::Scale> scaled(
            new Implicit::Engine::Vector2f::Transforms::Scale(field, scale));
    return scaled;
}

std::shared_ptr<Core::Vector2f::ImplicitField> Factory::invert(
        const std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> &field) {
    std::shared_ptr<Implicit::Engine::Vector2f::Transforms::Invert> res(
            new Implicit::Engine::Vector2f::Transforms::Invert(field));
    return res;
}

std::shared_ptr<Core::Vector2f::ImplicitField> Factory::bound(
        const std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> &field,
        float inf, float sup) {
    std::shared_ptr<Implicit::Engine::Vector2f::Transforms::Bound> res(
            new Implicit::Engine::Vector2f::Transforms::Bound(field, inf, sup));
    return res;
}

std::shared_ptr<Core::Vector2f::ImplicitField> Factory::cleanBound(
        const std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> &field,
        float inf, float sup) {
    std::shared_ptr<Implicit::Engine::Vector2f::Transforms::CleanBound> res(
            new Implicit::Engine::Vector2f::Transforms::CleanBound(field, inf, sup));
    return res;
}

std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> Factory::fieldScale(
        const std::shared_ptr<Implicit::Core::Vector2f::ImplicitField>& field,
        const std::shared_ptr<Implicit::Core::Vector2f::ImplicitField>& a_scale) {
    std::shared_ptr<Implicit::Engine::Vector2f::Transforms::FieldScale> res(
            new Implicit::Engine::Vector2f::Transforms::FieldScale(field, a_scale));
    return res;
}

}
}
}
}