// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#include "Translation.hpp"

namespace Implicit {
namespace Engine {
namespace Vector2f {
namespace Transforms {

bool Translation::getFloatField(const Eigen::Vector2f &position, float &value) const {
    return field->getFloatField(position - translation, value);
}

bool Translation::getGradientField(const Eigen::Vector2f &position, Eigen::Vector2f &gradient) const {
    return field->getGradientField(position - translation, gradient);
}

bool Translation::getImplicitField(const Eigen::Vector2f &position, Implicit::Core::Vector2f::ImplicitValue &value) const {
    return field->getImplicitField(position - translation, value);
}

bool Translation::getSpaceBoundingBox(Eigen::AlignedBox2f& boundingBox) const {
    if(! field->getSpaceBoundingBox(boundingBox)) {
        return false;
    }
    boundingBox = Eigen::AlignedBox2f(boundingBox.min() + translation, boundingBox.max() + translation);
    return true;
}

bool Translation::getSurfaceBoundingBox(Eigen::AlignedBox2f& boundingBox) const {
    if(! field->getSurfaceBoundingBox(boundingBox)) {
        return false;
    }
    boundingBox = Eigen::AlignedBox2f(boundingBox.min() + translation, boundingBox.max() + translation);
    return true;
}

bool Translation::getIsoBoundingBox(float targetIso, Eigen::AlignedBox2f& boundingBox) const {
    if(! field->getIsoBoundingBox(targetIso, boundingBox)) {
        return false;
    }
    boundingBox = Eigen::AlignedBox2f(boundingBox.min() + translation, boundingBox.max() + translation);
    return true;
}

}
}
}
}
