// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#include "FieldScale.hpp"

namespace Implicit {
namespace Engine {
namespace Vector2f {
namespace Transforms {

bool FieldScale::getFloatField(const Eigen::Vector2f &position, float &value) const {
    bool check = field->getFloatField(position, value);
    float s = 1.f;
    scale->getFloatField(position, s);
    value = (value - 0.5f) / s + 0.5f;
    return check;
}

bool FieldScale::getGradientField(const Eigen::Vector2f &position, Eigen::Vector2f &gradient) const {
    bool check = field->getGradientField(position, gradient);
    return check;
}

bool FieldScale::getImplicitField(const Eigen::Vector2f &position, Implicit::Core::Vector2f::ImplicitValue &value) const {
    bool check = field->getImplicitField(position, value);
    float s = 1.f;
    scale->getFloatField(position, s);
    value.field = (value.field - 0.5f) / s + 0.5f;
    return check;
}

bool FieldScale::getSpaceBoundingBox(Eigen::AlignedBox2f& boundingBox) const {
    return field->getSpaceBoundingBox(boundingBox);
}

bool FieldScale::getSurfaceBoundingBox(Eigen::AlignedBox2f& boundingBox) const {
    return field->getSurfaceBoundingBox(boundingBox);
}

bool FieldScale::getIsoBoundingBox(float targetIso, Eigen::AlignedBox2f& boundingBox) const {
    //TODO : determine real scale, but field makes this difficult if unbounded
    return field->getIsoBoundingBox(targetIso, boundingBox);
}

}
}
}
}

