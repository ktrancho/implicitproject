// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#include "CleanBound.hpp"

namespace Implicit {
namespace Engine {
namespace Vector2f {
namespace Transforms {

bool CleanBound::getFloatField(const Eigen::Vector2f &position, float &value) const {
    bool check = field->getFloatField(position, value);
    if(value < inf || value > sup) {
        value = std::max(std::min(value, sup), inf);
    } else {
        value = inf + (sup - inf) * (std::sin((((value - inf) / (sup - inf)) * 2 - 1) * M_PI * 0.5f) + 1) * 0.5f;
    }

    return check;
}

bool CleanBound::getGradientField(const Eigen::Vector2f &position, Eigen::Vector2f &gradient) const {
    bool check = field->getGradientField(position, gradient);
    return check;
}

bool CleanBound::getImplicitField(const Eigen::Vector2f &position, Implicit::Core::Vector2f::ImplicitValue &value) const {
    bool check = field->getImplicitField(position, value);
    if(value.field < inf || value.field > sup) {
        value.field = std::max(std::min(value.field, sup), inf);
    } else {
        value.field = inf + (sup - inf) * (std::sin((((value.field - inf) / (sup - inf)) * 2 - 1) * M_PI * 0.5f) + 1) * 0.5f;
    }
    return check;
}

bool CleanBound::getSpaceBoundingBox(Eigen::AlignedBox2f& boundingBox) const {
    return field->getSpaceBoundingBox(boundingBox);
}

bool CleanBound::getSurfaceBoundingBox(Eigen::AlignedBox2f& boundingBox) const {
    return field->getSurfaceBoundingBox(boundingBox);
}

bool CleanBound::getIsoBoundingBox(float targetIso, Eigen::AlignedBox2f& boundingBox) const {
    return field->getIsoBoundingBox(targetIso, boundingBox);
}

}
}
}
}

