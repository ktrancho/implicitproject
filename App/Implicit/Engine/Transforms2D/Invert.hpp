// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <memory>

#include <Eigen/Dense>

#include <Implicit/Core/Types/Types2D/ImplicitField.hpp>

namespace Implicit {
namespace Engine {
namespace Vector2f {
namespace Transforms {

class Invert : public Implicit::Core::Vector2f::ImplicitField {
public:
    Invert(const std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> &a_field) :
            field(a_field) {
        registerChild(field);
    }

    virtual bool getFloatField(const Eigen::Vector2f &position, float &value) const override;

    virtual bool getGradientField(const Eigen::Vector2f &position, Eigen::Vector2f &gradient) const override;

    virtual bool getImplicitField(const Eigen::Vector2f &position, Implicit::Core::Vector2f::ImplicitValue &value) const override;

    virtual bool getSpaceBoundingBox(Eigen::AlignedBox2f& boundingBox) const override;

    virtual bool getSurfaceBoundingBox(Eigen::AlignedBox2f& boundingBox) const override;

    virtual bool getIsoBoundingBox(float targetIso, Eigen::AlignedBox2f& boundingBox) const override;

    virtual std::string getType() const override {
        return std::string("ImplicitField");
    }

    virtual std::string getName() const override {
        return std::string("Invert");
    }

private:
    std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> field;
};

}
}
}
}
