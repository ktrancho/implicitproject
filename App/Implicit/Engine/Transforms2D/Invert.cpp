// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#include "Invert.hpp"

namespace Implicit {
namespace Engine {
namespace Vector2f {
namespace Transforms {

bool Invert::getFloatField(const Eigen::Vector2f &position, float &value) const {
    bool check = field->getFloatField(position, value);
    value = 1 - value;
    return check;
}

bool Invert::getGradientField(const Eigen::Vector2f &position, Eigen::Vector2f &gradient) const {
    bool check = field->getGradientField(position, gradient);
    gradient *= -1;
    return check;
}

bool Invert::getImplicitField(const Eigen::Vector2f &position, Implicit::Core::Vector2f::ImplicitValue &value) const {
    bool check = field->getImplicitField(position, value);
    value.field = 1 - value.field;
    value.gradient *= -1;
    return check;
}

bool Invert::getSpaceBoundingBox(Eigen::AlignedBox2f& boundingBox) const {
    return field->getSpaceBoundingBox(boundingBox);
}

bool Invert::getSurfaceBoundingBox(Eigen::AlignedBox2f& boundingBox) const {
    return field->getSurfaceBoundingBox(boundingBox);
}

bool Invert::getIsoBoundingBox(float targetIso, Eigen::AlignedBox2f& boundingBox) const {
    return field->getIsoBoundingBox(1 - targetIso, boundingBox);
}

}
}
}
}

