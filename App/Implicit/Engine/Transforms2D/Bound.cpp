// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#include "Bound.hpp"

namespace Implicit {
namespace Engine {
namespace Vector2f {
namespace Transforms {

bool Bound::getFloatField(const Eigen::Vector2f &position, float &value) const {
    bool check = field->getFloatField(position, value);
    value = std::max(std::min(value, sup), inf);
    return check;
}

bool Bound::getGradientField(const Eigen::Vector2f &position, Eigen::Vector2f &gradient) const {
    bool check = field->getGradientField(position, gradient);
    return check;
}

bool Bound::getImplicitField(const Eigen::Vector2f &position, Implicit::Core::Vector2f::ImplicitValue &value) const {
    bool check = field->getImplicitField(position, value);
    value.field = std::max(std::min(value.field, sup), inf);
    return check;
}

bool Bound::getSpaceBoundingBox(Eigen::AlignedBox2f& boundingBox) const {
    return field->getSpaceBoundingBox(boundingBox);
}

bool Bound::getSurfaceBoundingBox(Eigen::AlignedBox2f& boundingBox) const {
    return field->getSurfaceBoundingBox(boundingBox);
}

bool Bound::getIsoBoundingBox(float targetIso, Eigen::AlignedBox2f& boundingBox) const {
    return field->getIsoBoundingBox(targetIso, boundingBox);
}

}
}
}
}

