// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <memory>

#include <Eigen/Dense>

#include <Implicit/Core/Types/Types2D/ImplicitField.hpp>

namespace Implicit {
namespace Engine {
namespace Vector2f {
namespace Transforms {

class Factory {
public:
    static std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> translate(
            const std::shared_ptr<Implicit::Core::Vector2f::ImplicitField>& field,
            const Eigen::Vector2f& translation = Eigen::Vector2f::Zero());

    static std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> scale(
            const std::shared_ptr<Implicit::Core::Vector2f::ImplicitField>& field,
            const Eigen::Vector2f& scale = Eigen::Vector2f::Ones());

    static std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> invert(
            const std::shared_ptr<Implicit::Core::Vector2f::ImplicitField>& field);

    static std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> bound(
            const std::shared_ptr<Implicit::Core::Vector2f::ImplicitField>& field,
            float inf = 0.f, float sup = 1.f);

    static std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> cleanBound(
            const std::shared_ptr<Implicit::Core::Vector2f::ImplicitField>& field,
            float inf = 0.f, float sup = 1.f);

    static std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> fieldScale(
            const std::shared_ptr<Implicit::Core::Vector2f::ImplicitField>& field,
            const std::shared_ptr<Implicit::Core::Vector2f::ImplicitField>& a_scale);
};

}
}
}
}