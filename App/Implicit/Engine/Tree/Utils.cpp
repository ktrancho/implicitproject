// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#include "Utils.hpp"

namespace Implicit {
namespace Engine {
namespace Tree {

///TODO : generalize find process.

bool Utils::findComponentByType(const std::shared_ptr<Implicit::Engine::Tree::Component>& root, const std::string& type,
        std::shared_ptr<Implicit::Engine::Tree::Component>& res) {
    if(root->getType() == type) {
        res = root;
        return true;
    }
    auto children = root->getChildren();
    if(children.empty()) {
        return false;
    }
    for(auto child : children) {
        if(child->getType() == type) {
            res = child;
            return true;
        }
    }
    for(auto child : children) {
        if(findComponentByType(child, type, res)) {
            return true;
        }
    }
    return false;
}

bool Utils::findComponentById(const std::shared_ptr<Implicit::Engine::Tree::Component>& root,
                       int id, std::shared_ptr<Implicit::Engine::Tree::Component>& res) {
    if(root->getId() == id) {
        res = root;
        return true;
    }
    auto children = root->getChildren();
    if(children.empty()) {
        return false;
    }
    for(auto child : children) {
        if(findComponentById(child, id, res)) {
            return true;
        }
    }
    return false;
}

void Utils::treeDisplay(const std::shared_ptr<Implicit::Engine::Tree::Component>& root, std::ostream& stream) {
    std::vector<std::shared_ptr<Implicit::Engine::Tree::Component>> stackComponents;
    std::vector<int> treeHeights;

    stackComponents.push_back(root);
    treeHeights.push_back(0);

    while(! stackComponents.empty()) {
        std::shared_ptr<Implicit::Engine::Tree::Component> current = stackComponents[stackComponents.size() - 1];
        int currentHeight = treeHeights[stackComponents.size() - 1];

        stackComponents.pop_back();
        treeHeights.pop_back();

        int prevMinHeight = (treeHeights.empty()) ? (currentHeight) : treeHeights[0];

        for(int i = 0; i < prevMinHeight; ++i) {
            stream << "    ";
        }
        for(int i = prevMinHeight; i < currentHeight; ++i) {
            stream << "|   ";
        }

        stream << "+- (#" << current->getId() << ") : " << current->getType() << " | " << current->getName() << std::endl;

        for(auto child : current->getChildren()) {
            stackComponents.push_back(child);
            treeHeights.push_back(currentHeight + 1);
        }
    }
}

}
}
}
