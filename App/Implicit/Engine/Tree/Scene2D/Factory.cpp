// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#include "Factory.hpp"

namespace Implicit {
namespace Engine {
namespace Tree {
namespace Vector2f {
namespace Scenes {

std::shared_ptr<Implicit::Engine::Tree::Vector2f::Scene> Factory::emptyScene(
        const std::shared_ptr<Implicit::Display::Window>& a_window,
        const Eigen::AlignedBox2f& a_sceneSpace) {
    std::shared_ptr<Implicit::Engine::Tree::Vector2f::Scene> scene(new Implicit::Engine::Tree::Vector2f::Scene(a_window, a_sceneSpace));
    return scene;
}

}
}
}
}
}