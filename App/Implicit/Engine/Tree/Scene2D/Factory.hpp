// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <memory>

#include <Eigen/Dense>

#include <Implicit/Engine/Tree/Scene2D/Scene.hpp>

namespace Implicit {
namespace Engine {
namespace Tree {
namespace Vector2f {
namespace Scenes {

class Factory {
public:
    static std::shared_ptr<Implicit::Engine::Tree::Vector2f::Scene> emptyScene(
            const std::shared_ptr<Implicit::Display::Window>& a_window,
            const Eigen::AlignedBox2f& a_sceneSpace);
};

}
}
}
}
}
