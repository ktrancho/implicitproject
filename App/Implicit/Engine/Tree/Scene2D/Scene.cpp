// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#include "Scene.hpp"

#include <Implicit/Engine/Display/Factory.hpp>

#include <Implicit/Engine/Tree/Utils.hpp>

namespace Implicit {
namespace Engine {
namespace Tree {
namespace Vector2f {

void Scene::addImplicit(const std::shared_ptr<Implicit::Core::Vector2f::ImplicitField>& field) {
    implicits.push_back(field);
    currentImplicit = field;

    firstOperatorFoundLookAt();
}

void Scene::firstOperatorFoundLookAt() {
    std::shared_ptr<Implicit::Engine::Tree::Component> component;
    if(Implicit::Engine::Tree::Utils::findComponentByType(currentImplicit, "Operator", component)) {
        // given component is expected to be an operator, assuming searching function is fine, we can cast the component into operator.
        lookAtOperator((std::shared_ptr<Implicit::Operator::Operator>&)component);
    }
}

void Scene::lookAtOperator(const std::shared_ptr<Implicit::Operator::Operator>& op) {
    visualizeOperator(Implicit::Engine::Display::Factory::binary(op));
}

void Scene::visualizeOperator(const std::shared_ptr<Implicit::Engine::Display::BinaryOperator>& op) {
    currentOperator = op;
    current3DOperator = nullptr;
}

void Scene::visualizeOperator3D(const std::shared_ptr<Implicit::Engine::Display::NaryFloatOneParameter>& op) {
    current3DOperator = op;
    currentOperator = nullptr;
}

}
}
}
}