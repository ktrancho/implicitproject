// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <memory>

#include <Eigen/Dense>

#include <Implicit/Core/Types/Types2D/ImplicitField.hpp>
#include <Implicit/Engine/Display/BinaryOperator.hpp>
#include <Implicit/Engine/Display/NaryFloatOneParameter.hpp>
#include <Implicit/Display/Window.hpp>

namespace Implicit {
namespace Engine {
namespace Tree {
namespace Vector2f {

class Scene {
public:
    Scene(const std::shared_ptr<Implicit::Display::Window>& a_window,
            const Eigen::AlignedBox2f& a_sceneSpace) :
        window(a_window),
        sceneSpace(a_sceneSpace) {

    }

    virtual void addImplicit(const std::shared_ptr<Implicit::Core::Vector2f::ImplicitField>& field);

    void firstOperatorFoundLookAt();

    void lookAtOperator(const std::shared_ptr<Implicit::Operator::Operator>& op);

    void visualizeOperator(const std::shared_ptr<Implicit::Engine::Display::BinaryOperator>& op);

    void visualizeOperator3D(const std::shared_ptr<Implicit::Engine::Display::NaryFloatOneParameter>& op);

    void render(int accuracy = 1) {
        window->clear();

        window->displayVector2fWorkingField(currentImplicit, sceneSpace, accuracy);
        if(currentOperator) {
            window->displayOperatorWorkingField(currentOperator, accuracy);
        }
        if(current3DOperator) {
            window->displayOperatorNaryWorkingField(current3DOperator, accuracy);
        }


        window->refresh();
    }

    void positionFromSceneToScreen(const Eigen::Vector2f& fromScene, Eigen::Vector2f& toScreen) {
        Eigen::AlignedBox2f screen = window->getScreenRegion();
        Eigen::Vector2f screenOrigin = screen.min();
        Eigen::Vector2f sceneOrigin = sceneSpace.min();
        Eigen::Vector2f screenSize = screen.sizes();
        Eigen::Vector2f sceneSize = sceneSpace.sizes();

        Eigen::Vector2f coords = fromScene - sceneOrigin;
        coords[0] /= sceneSize[0];
        coords[1] /= sceneSize[1];

        toScreen = screenOrigin
                   + coords[0] * screenSize[0] * Eigen::Vector2f::UnitX()
                   + coords[1] * screenSize[1] * Eigen::Vector2f::UnitY();
    }

    void positionFromScreenToScene(const Eigen::Vector2f& fromScreen, Eigen::Vector2f& toScene) {
        Eigen::AlignedBox2f screen = window->getScreenRegion();
        Eigen::Vector2f screenOrigin = screen.min();
        Eigen::Vector2f sceneOrigin = sceneSpace.min();
        Eigen::Vector2f screenSize = screen.sizes();
        Eigen::Vector2f sceneSize = sceneSpace.sizes();

        Eigen::Vector2f coords = fromScreen - screenOrigin;
        coords[0] /= screenSize[0];
        coords[1] /= screenSize[1];

        toScene = sceneOrigin
                  + coords[0] * sceneSize[0] * Eigen::Vector2f::UnitX()
                  + coords[1] * sceneSize[1] * Eigen::Vector2f::UnitY();
    }

    void vectorFromSceneToScreen(const Eigen::Vector2f& fromScene, Eigen::Vector2f& toScreen) {
        Eigen::AlignedBox2f screen = window->getScreenRegion();
        Eigen::Vector2f screenOrigin = screen.min();
        Eigen::Vector2f sceneOrigin = sceneSpace.min();
        Eigen::Vector2f screenSize = screen.sizes();
        Eigen::Vector2f sceneSize = sceneSpace.sizes();

        Eigen::Vector2f coords = fromScene - sceneOrigin;
        coords[0] /= sceneSize[0];
        coords[1] /= sceneSize[1];

        toScreen = screenOrigin
                + coords[0] * screenSize[0] * Eigen::Vector2f::UnitX()
                + coords[1] * screenSize[1] * Eigen::Vector2f::UnitY();
    }

    void vectorFromScreenToScene(const Eigen::Vector2f& fromScreen, Eigen::Vector2f& toScene) {
        Eigen::AlignedBox2f screen = window->getScreenRegion();
        Eigen::Vector2f screenSize = screen.sizes();
        Eigen::Vector2f sceneSize = sceneSpace.sizes();

        Eigen::Vector2f coords = fromScreen;
        coords[0] /= screenSize[0];
        coords[1] /= screenSize[1];

        toScene = coords[0] * sceneSize[0] * Eigen::Vector2f::UnitX()
                   + coords[1] * sceneSize[1] * Eigen::Vector2f::UnitY();
    }

public:
    std::shared_ptr<Implicit::Display::Window> window;
    Eigen::AlignedBox2f sceneSpace;
    std::vector<std::shared_ptr<Implicit::Core::Vector2f::ImplicitField>> implicits;
    std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> currentImplicit;
    std::shared_ptr<Implicit::Engine::Display::BinaryOperator> currentOperator;
    std::shared_ptr<Implicit::Engine::Display::NaryFloatOneParameter> current3DOperator;
};

}
}
}
}
