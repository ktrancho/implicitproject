// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <vector>
#include <memory>

namespace Implicit {
namespace Engine {
namespace Tree {

///TODO: think about better way to automatically register components classes w.r.t type and name.

class Component {
protected:
    Component() :
        id(lastId++) {

    }

    void registerChild(const std::shared_ptr<Component>& component) {
        children.push_back(component);
    }

    void clearChildren() {
        children.clear();
    }

    ///TODO: add copy function to avoid conflicts on Id

public:
    int getId() const {
        return id;
    }

    const std::vector<std::shared_ptr<Component>> getChildren() const {
        return children;
    }

    virtual std::string getType() const {
        return std::string("Component");
    }

    virtual std::string getName() const {
        return std::string("Component");
    }

private:
    static int lastId;

private:
    int id;
    std::vector<std::shared_ptr<Component>> children;
};

}
}
}
