// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#include "Factory.hpp"

#include <iostream>

#include <Implicit/Engine/Display/BinaryFloatSimple.hpp>
#include <Implicit/Engine/Display/NaryFloatOneParameter.hpp>
#include <Implicit/Engine/Display/BinaryNParameters.hpp>

namespace Implicit {
namespace Engine {
namespace Display {

std::shared_ptr<Implicit::Engine::Display::BinaryOperator> Factory::binaryFloat(
        const std::shared_ptr<Implicit::Operator::BinaryFloat>& op) {
    std::shared_ptr<Implicit::Engine::Display::BinaryFloatSimple> display(new Implicit::Engine::Display::BinaryFloatSimple(op));
    return display;
}

std::shared_ptr<Implicit::Engine::Display::BinaryOperator>Factory:: naryOneParamFloat(
        const std::shared_ptr<Implicit::Operator::OneParameter::NaryFloat>& op,
        int a_size,
        float a_parameter) {
    std::shared_ptr<Implicit::Engine::Display::NaryFloatOneParameter> display(
            new Implicit::Engine::Display::NaryFloatOneParameter(op, a_size, a_parameter)
            );
    return display;
}

std::shared_ptr<Implicit::Engine::Display::BinaryOperator> Factory::binaryNParamsFloat(
        const std::shared_ptr<Implicit::Operator::NParameters::BinaryFloat>& op,
        int a_size) {
    std::shared_ptr<Implicit::Engine::Display::BinaryNParameters> display(
            new Implicit::Engine::Display::BinaryNParameters(op, a_size)
            );
    return display;
}

std::shared_ptr<Implicit::Engine::Display::BinaryOperator> Factory::binary(const std::shared_ptr<Implicit::Operator::Operator>& op) {
    /// TODO : better registration to Factory ?

    if(op->getOperatorType() == "BinaryFloat") {
        return binaryFloat((const std::shared_ptr<Implicit::Operator::BinaryFloat>&)op);
    }

    std::cerr << "No automatic operator type detection for type \"" << op->getOperatorType() << "\"" << std::endl;
    return nullptr;
}

}
}
}