// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <memory>

#include <Implicit/Engine/Display/BinaryOperator.hpp>
#include <Implicit/Composition/Types/BinaryFloat.hpp>
#include <Implicit/Composition/Types/NaryFloatOneParameter.hpp>
#include <Implicit/Composition/Types/BinaryFloatNParameters.hpp>

namespace Implicit {
namespace Engine {
namespace Display {

class Factory {
public:
    static std::shared_ptr<Implicit::Engine::Display::BinaryOperator> binaryFloat(
            const std::shared_ptr<Implicit::Operator::BinaryFloat>& op);

    static std::shared_ptr<Implicit::Engine::Display::BinaryOperator> naryOneParamFloat(
            const std::shared_ptr<Implicit::Operator::OneParameter::NaryFloat>& op,
            int a_size,
            float a_parameter);

    static std::shared_ptr<Implicit::Engine::Display::BinaryOperator> binaryNParamsFloat(
            const std::shared_ptr<Implicit::Operator::NParameters::BinaryFloat>& op,
            int a_size);

    static std::shared_ptr<Implicit::Engine::Display::BinaryOperator> binary(
            const std::shared_ptr<Implicit::Operator::Operator>& op);
};

}
}
}
