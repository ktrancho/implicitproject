// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#include "BinaryNParameters.hpp"

namespace Implicit {
namespace Engine {
namespace Display {

bool BinaryNParameters::evalFloat(float f1, float f2, float& value) const {
    return op->evalFloat(f1, f2, parameters, value);
}

bool BinaryNParameters::evalGradient(float f1, float f2, Eigen::Vector2f& gradient) const {
    return op->evalGradient(f1, f2, parameters, gradient);
}

}
}
}
