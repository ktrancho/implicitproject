// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#include "NaryFloatOneParameter.hpp"

namespace Implicit {
namespace Engine {
namespace Display {

bool NaryFloatOneParameter::evalFloat(float f1, float f2, float& value) const {
    std::vector<float> v = values;
    v[firstId] = f1;
    v[secondId] = f2;
    return op->evalFloat(v, parameter, value);
}

bool NaryFloatOneParameter::evalGradient(float f1, float f2, Eigen::Vector2f& gradient) const {
    // TODO : gradient visualisation
    return false;
}

bool NaryFloatOneParameter::evalFloats(const std::vector<float> &f, float &value) const {
    std::vector<float> v = values;
    for(int i = 0; i < std::min(v.size(), f.size()); ++i) {
        v[i] = f[i];
    }
    return op->evalFloat(v, parameter, value);
}

}
}
}