// This file is part of ImplicitProject, a prototype to try implicit surfaces' stuff.
//
// Copyright (C) 2020 Kevin Trancho
//
// This Source Code Form is subject to the terms of the Creative Commons 
// Attribution-NonCommercial-ShareAlike 4.0 International Public License.
// If a copy of the License was not distributed with this file, You can obtain one at 
// https://creativecommons.org/licenses/by-nc-sa/4.0/.

#pragma once

#include <cmath>

#include <Implicit/Core/Types/Types2D/ImplicitField.hpp>
#include <Implicit/Core/Primitives2D/AllFactories.hpp>
#include <Implicit/Composition/Types/BinaryFloat.hpp>

namespace Implicit {
namespace Engine {
namespace Display {

class BinaryOperator : public Implicit::Core::Vector2f::ImplicitField {
protected:
    BinaryOperator() :
            firstPlane(Implicit::Core::Vector2f::DistanceFields::Factory::createPlane(Eigen::Vector2f::UnitX())),
            secondPlane(Implicit::Core::Vector2f::DistanceFields::Factory::createPlane(-Eigen::Vector2f::UnitY())) {
    }

    /// Have to redefine BinaryFloat like methods.
    /// Idea is to use an operator for these and embed possible parameters.

    /// Field value of the composition.
    virtual bool evalFloat(float f1, float f2, float& value) const = 0;

    /// Gradient of the operator for the given values.
    virtual bool evalGradient(float f1, float f2, Eigen::Vector2f& gradient) const = 0;

public:
    virtual bool getFloatField(const Eigen::Vector2f &position, float& value) const {
        float v1;
        float v2;
        bool check = firstPlane->getFloatField(position, v1);
        check = secondPlane->getFloatField(position, v2) && check;
        check = evalFloat(v1, v2, value) && check;

        return check;
    }

    virtual bool getGradientField(const Eigen::Vector2f &position, Eigen::Vector2f& gradient) const {
        float v1;
        float v2;
        bool check = firstPlane->getFloatField(position, v1);
        check = secondPlane->getFloatField(position, v2) && check;
        check = evalGradient(v1, v2, gradient) && check;
        return check;
    }

    virtual std::string getType() const override {
        return std::string("DisplayBinaryOperator");
    }

    virtual std::string getName() const override {
        return std::string("DisplayBinaryOperator");
    }

private:
    std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> firstPlane;
    std::shared_ptr<Implicit::Core::Vector2f::ImplicitField> secondPlane;

};

}
}
}
