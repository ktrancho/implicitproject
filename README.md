  Implicit Project
============

### things to try

Aller dans le menu `tree` et visualiser l'arbre avec `display`.

```ASCII
>>> tree
tree >>> display
+- (#4) : BinaryComposite | BinaryComposite
    +- (#3) : Operator | BinaryFloatOperatorLargeBlending
    +- (#2) : Translation | Translation
    |   +- (#0) : ImplicitField | Circle
    +- (#1) : Translation | Translation
        +- (#0) : ImplicitField | Circle
```

Pour changer l'opérateur de composition, il suffit de selectionner le noeud 
`BinaryComposite` puis entrer le nom de l'opérateur avec la commande `operator-change`.
Pour obtenir la liste des opérateurs proposés pour ce noeud ceci se fait en demandant de
l'aide pour cette commande `? operator-change`.

```ASCII
tree >>> select 4
#4 | BinaryComposite >>> help
Commands : 
 - operator-change [OP] : change the operator of the current composite.
 - select [COMPONENT ID] : select a component.
 - help : ask for this page.
 - view : display current component on the scene.
 - exit : exit the console.
 - back : go to previous stage.
 - ? [COMMAND] : ask for the help of a given command.
 - display : display current implicit tree.
#4 | BinaryComposite >>> ? operator-change
For given command : operator-change [OP] : change the operator of the current composite.
Available operators are the following :
- LargeBlending
- MaxUnion
- OneSidedFullStretching
- FullStretching
#4 | BinaryComposite >>> operator-change FullStretching
```

Il est aussi possible d'éditer les translations à la souris, pour ceci, on selectionne
la translation, puis on lance la commande `mouse`. Il faut presser un bouton de la 
souris pour déplacer relativement la surface. Lorsqu'on veut arrêter la manipulation on
appuie sur la touche Échap.

```ASCII
#4 | BinaryComposite >>> select 2
#2 | Translation >>> mouse
 - Edit translation with mouse grab on screen.
 - Type key Esc. when finished.
 ...(window manipulation) 
#2 | Translation >>> state
Translation : ( 0.586667 -0.353333)
```


### TODO list

```ASCII
-More operators (contact operators to play with)
--bulging versus hard
--Analytic contact (compare to operations builded)
--Analytic contact one-sided (compare to part builded)

-Composition time computation

-Scaling
-Rotate

-Copy for components
-Implicit tree handling :
--Add/remove surfaces/compositions/transformations

-Gradient vector field visualization

-Gradient-based operators
--control shape
--binary operator with parameter integration
--gradient-based composite

-Anisotropic operators
--have to be compatible with binary with operator used for gradient-based

-Physical operator : bulging, stretching, hardness
--implicit with embeded parameters

-test N-ary interaction from physical operator

-User real-time operators building (keyframes)

-Parameter space for user real-time operators

-Hermite Radial Basis Functions

-Deformable distance field with embeded parameters and self-composition

-Do 3D :
--operators should not move
--only space related stuff : surface, composite, deformable surface should need coding
```

### Modules principaux

```ASCII
Implicit
|
+---Core
|   +---Types
|   +---Primitives2D
|   +---Primitives3D
|
+---Composition
|   +---Types
|   +---Operators
|   +---Controlers
|
+---Engine
|   +---Types
|   +---Composites2D
|   +---Composites3D
|
+---Display
    +---Cores
```